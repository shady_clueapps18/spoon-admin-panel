/*
  NOTE:
  ------
  PLACE HERE YOUR OWN JAVASCRIPT CODE IF NEEDED
  WE WILL RELEASE FUTURE UPDATES SO IN ORDER TO NOT OVERWRITE YOUR JAVASCRIPT CODE PLEASE CONSIDER WRITING YOUR SCRIPT HERE.  */

$(document).ready(function() {
  const addButton = (elm, href, title) => {
    $(elm).prepend(`<a href=${href} class="btn btn-primary">${title}</a>`);
  };

  addButton(
    '#branches-table_length',
    // TODO: ROUTE
    `../../spoon/branches/create-branch.html?id=${
      document.location.search.split('id=')[1]
    }`,
    'Create Branch'
  );

  addButton(
    '#notification-table_length',
    // TODO: ROUTE
    `../../spoon/branches/create-branch.html?id=${
      document.location.search.split('id=')[1]
    }`,
    'Create Notification'
  );

  addButton(
    '#providers-table_length',
    `providersOverView?id=${document.location.search.split('id=')[1]}`,
    'Create Provider'
  );

  addButton(
    '#provider-menu-section_filter',
    `createMenuSection?id=${document.location.search.split('id=')[1]}`,
    'Create Menu Section'
  );

  addButton(
    '#areas-table_length',
    `createArea?id=${document.location.search.split('id=')[1]}`,
    'Add Area'
  );

  addButton('#categories-table_length', `createCategory`, 'Add Category');

  addButton(
    '#cities-table_length',
    `createCity?id=${document.location.search.split('id=')[1]}`,
    'Add City'
  );

  // TODO: TO BE ADDED IN THE SUCCESS OF THE TABLE
  addButton(
    '#countries-table_length',
    `createCountry?id=${document.location.search.split('id=')[1]}`,
    'Add Country'
  );

  addButton(
    '#cuisines-table_length',
    `createCuisine?id=${document.location.search.split('id=')[1]}`,
    'Create Cuisine'
  );

  addButton(
    '#events-table_length',
    `createEvent?id=${document.location.search.split('id=')[1]}`,
    'Create Event'
  );

  addButton(
    '#features-table_length',
    `createFeature?id=${document.location.search.split('id=')[1]}`,
    'Add Feature'
  );

  addButton(
    '#promo-table_length',
    `createPromoCode?id=${document.location.search.split('id=')[1]}`,
    'Add Promo code'
  );
  addButton();
  addButton();
  /**
|--------------------------------------------------
| CREATE PROVIDER SCRIPTS
|--------------------------------------------------
*/
  /**
   * Create Provider Validation
   */

  // MaxChars function
  const limitChars = (input, maxChars) => {
    $(input).on('keyup', function() {
      $(this).val(
        $(this)
          .val()
          .substring(0, maxChars)
      );
    });
  };

  limitChars('#provider-description', 180);
  limitChars('#provider-name', 80);

  // End of MaxChars function

  /**
   * End of Create Provider Validation
   */

  /**
  |--------------------------------------------------
  | DATATBLES FUNCTIONS
  |--------------------------------------------------
  */

  var lengthParent = $('.dataTables_length').parent();
  $('.dataTables_filter')
    .parent()
    .prependTo($(lengthParent).parent());

  /**
   * ADD SORTING FUNCTION
   */
  // const addSorting = (elm, ...options) => {
  //   const markup = `<div class="sort-by-wrapper d-flex align-items-center w-50">
  //     <label ckass="w-30"> Sort By: </label>
  //     <select class="sort-by form-control ml-1">
  //       <option value="">Choose Option</option>
  //       ${options.forEach(option =>
  //         $('.sort-by').append(`<option value="${option}">${option}</option>`)
  //       )}
  //     </select>
  //   </div>`;
  //   $(elm).append(markup);
  //   console.log(options);
  // };

  // // FIXME: The only first functions gets executed!
  // addSorting('#branches-table_filter', 'Area', 'Trending', 'Latest', 'Popular');
  // addSorting('#providers-table_filter', 'Popular', 'Latest', 'Top Rated');

  /**
   * END of ADD SORTING FUNCTION
   */

  /**
  |--------------------------------------------------
  | END of DATATBLES FUNCTIONS
  |--------------------------------------------------
  */

  /**
   * Dropzone
   */

  const createDropZone = (
    element,
    maxFiles,
    msg = 'Upload up to 5 images for your branch',
    url
  ) => {
    $(element).addClass('dropzone');
    $(element).dropzone({
      url: url,
      autoProcessQueue: false,
      dictDefaultMessage: msg,
      uploadMultiple: true,
      maxFilesize: 2,
      maxFiles: maxFiles,
      acceptedFiles: 'image/*',
      addRemoveLinks: true,
      headers: {
        Authorization:
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
      }
    });
  };

  createDropZone('#provider-branch-images', 5);
  createDropZone(
    '#branch-images',
    5,
    'Upload up to 5 images for your branch',
    'http://spoon.api.myspoon.me/api/v1/branches'
  );
  createDropZone('#product-img', 1, 'Upload the Product Image');
  createDropZone('#category-img', 1, 'Upload the Category Image');
  createDropZone(
    '#event-img',
    1,
    'Upload the Event Image',
    'http://spoon.api.myspoon.me/api/v1/events'
  );
  createDropZone(
    '#cuisine-img',
    1,
    'Upload the Cuisine Image',
    'http://spoon.api.myspoon.me/api/v1/cuisines'
  );
  createDropZone(
    '#country-flag',
    1,
    'Upload the Country Flag',
    'http://spoon.api.myspoon.me/api/v1/countries'
  );

  createDropZone(
    '#notification-img',
    1,
    'Upload Notification Image',
    'http://spoon.api.myspoon.me/api/admin/v1/notifications'
  );

  // On Editing Section Image

  const changeSectionImage = url => {
    $('.edit-image').on('click', function() {
      $(this)
        .closest('.col-12.col-md-6')
        .removeClass('d-flex');

      $(this)
        .parents('.edit-img-wrapper')
        .hide();

      createDropZone('#edit-img-dropzone', 1, 'Upload New Image', url);

      $('#edit-img-dropzone').trigger('click');
    });

    $('.delete-image').on('click', function() {
      $(this)
        .closest('.col-12.col-md-6')
        .removeClass('d-flex');

      $(this)
        .parents('.edit-img-wrapper')
        .hide();

      createDropZone('#edit-img-dropzone', 1, 'Upload New Image', url);
    });
  };

  // OK, I found that the URL has no impact. It's just to initiate Dropzone :/
  changeSectionImage(`https://spoon.api.myspoon.me/api/admin/v1/countries`);

  /**
   * End of Dropzone
   */

  /**
   * Add Product Functions
   */

  // Add Option
  $('#add-option').on('click', function() {
    $(this).parent().append(`
      <div class="row addition-option">
        <div class="col-8">
          <input type="text" class="form-control option-input" id="option-name" placeholder="Option Name">
        </div>
        <div class="col-4">
          <input type="text" class="form-control option-input" id="option-price" placeholder="Option Price">
        </div>
      </div>
    `);
  });

  // Add Additions
  const addProduct = () => {
    const product = {};
    const additions = [];
    let options = [];
    let option = {};
    let addition = {};
    let isEditing = false;
    let editingIndex = null;

    // Add Product Validation
    (function addProductValidation() {
      // Handle Product Price Input
      $('#product-price').on('change keyup', function() {
        if (isNaN($(this).val())) {
          if ($(this).siblings('p').length === 0) {
            $(this)
              .parent()
              .append(`<p class="danger">Please Enter a Valid Number</p>`);
          } else {
            $(this)
              .siblings('p')
              .replaceWith(`<p class="danger">Please Enter a Valid Number</p>`);
          }
        } else {
          $(this)
            .siblings('p')
            .remove();
        }
      });

      // Handle Product Description Input
      limitChars('#product-description', 180);

      // Check whether the Product Price is Basic
    })();

    $('#submit-product').on('click', function(e) {
      e.preventDefault();
      product.name = $('#product-name').val();
      product.price = $('#product-price').val();
      product.description = $('#product-description').val();
      product.additions = additions;

      $.ajax({
        method: 'POST',
        url: `http://spoon.mtech-ins.com/api/admin/v1/addProductMenuSection/${id}`,
        headers: {
          Authorization:
            'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5tdGVjaC1pbnMuY29tXC9hcGlcL2FkbWluXC92MVwvdXNlcnNMb2dpbiIsImlhdCI6MTUyOTU5NzU3NywiZXhwIjoxNTM3MzczNTc3LCJuYmYiOjE1Mjk1OTc1NzcsImp0aSI6IkY1azBEQkk1MEhrdm5saHEiLCJzdWIiOjE0OCwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.XKNfm6cxBQnfKaEDMxsVK0vTETzIp6uisA0UwB0Q2-Q'
        },
        data: {
          name: product.name,
          description: product.description,
          image: window.btoa('hello, World!'),
          price: parseInt(product.price),
          basic_price: 1,
          num_favorite: 0,
          product_addition: additions
        },
        success: data => console.log(data),
        error: err => console.log(err)
      });

      console.log(product);

      if ($('#basic-price-yes').prop('checked') === true) {
        if ($(this).siblings('p').length === 0) {
          $(this)
            .parent()
            .append(
              `<p class="danger">You should add an Addititon in case of Basic Price Products</p>`
            );
        } else {
          $(this)
            .siblings('p')
            .replaceWith(
              `<p class="danger">You should add an Addititon in case of Basic Price Products</p>`
            );
        }
      }
    });

    // Handle Product Data and view
    const handleProduct = (isNew = true) => {
      let additionTitle = $('#addition-title').val();
      const additionOption = $('.addition-option');
      const additionRequired =
        $('#addition-required').prop('checked') === true ? 1 : 0;
      const additionMultipleOptions =
        $('#multiple-prod-opt').prop('checked') === true ? 1 : 0;

      if (additionOption.length > 0) {
        $(additionOption).each(function() {
          option.name = $(this)
            .find('#option-name')
            .val();
          option.price = $(this)
            .find('#option-price')
            .val();
          options.push(option);
          option = {};
        });
      }

      addition = {
        name: additionTitle,
        required: additionRequired,
        multiple: additionMultipleOptions,
        option: options
      };
      if (isNew) {
        additions.push(addition);
      } else {
        additions.splice(editingIndex, 1, addition);
      }
      addition = {};
      options = [];
    };

    // Clear Modal Function
    const clearModal = () => {
      // Clearing Title Input
      $('#addition-title').val('');

      // Unchecking Checkboxes
      $('#addition-required').prop('checked', false);
      $('#multiple-prod-opt').prop('checked', false);

      // Removing Options
      $('#add-option')
        .parent()
        .find('.row')
        .each(function() {
          $(this).remove();
        });
    };

    // Addition Card Markup!
    const additionCard = `
    <div class="col-12 col-sm-6 col-md-4">
    <div class="added-addition" data-index="">
      <div class="card">
        <div class="card-header">
          <div class="badge badge-info" id="edit-option" data-toggle="modal" data-target="#addition-modal">Edit</div>
          <div class="badge badge-danger" id="delete-option">Delete</div>
        </div>
        <div class="card-content">
          <div class="card-body">
            <div class="media d-flex">
              <div class="align-self-center">
                <i class="la la-star-o yellow font-large-2 float-left"></i>
              </div>
              <div class="media-body text-right">
                <h3>156</h3>
                <span>New Comments</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    `;

    $(document).on('click', '#save-additions', function() {
      // If the User is Editing current addition
      if (isEditing) {
        handleProduct(false);
        $('.added-addition')
          .eq(editingIndex)
          .find('h3')
          .text($('#addition-title').val());
        clearModal();
        isEditing = false;

        // If the User is Adding Another Addition
      } else {
        handleProduct();
        $('.added-options-wrapper').append(additionCard);
        additions.forEach((addition, index) => {
          $('.added-addition')
            .eq(index)
            .attr('data-index', index)
            .find('h3')
            .text(addition.title);
        });
        clearModal();
      }
    });

    // On Clicking Add Another Addition
    $('#another-addition').on('click', function() {
      handleProduct();
      $('.added-options-wrapper').append(additionCard);
      additions.forEach((addition, index) => {
        $('.added-addition')
          .eq(index)
          .attr('data-index', index)
          .find('h3')
          .text(addition.title);
      });
      clearModal();
    });

    // On Clicking Cancel
    $('#modal-cancel').on('click', function() {
      clearModal();
    });

    // Editing Addition
    $(document).on('click', '#edit-option', function() {
      isEditing = true;
      editingIndex = parseInt(
        $(this)
          .parents('.added-addition')
          .attr('data-index')
      );

      // The Item selected to be editing (from Array)
      const editingAddition = additions[editingIndex];
      console.log(editingAddition);

      // Modal title
      $('#addition-title').val(editingAddition.title);

      // Modal required
      editingAddition.required === true
        ? $('#addition-required').prop('checked', true)
        : $('#addition-required').prop('checked', false);

      // Modal Allow Multiple
      editingAddition.multipleOptions === true
        ? $('#multiple-prod-opt').prop('checked', true)
        : $('#multiple-prod-opt').prop('checked', false);

      // Add Options to the Modal from the Array

      editingAddition.options.forEach(option =>
        $('#add-option').parent().append(`
          <div class="row addition-option">
            <div class="col-8">
              <input type="text" class="form-control option-input" id="option-name" placeholder="Option Name" value=${
                option.name
              }>
            </div>
            <div class="col-4">
              <input type="text" class="form-control option-input" id="option-price" placeholder="Option Price" value=${
                option.price
              }>
            </div>
          </div>
        `)
      );
    });

    // Deleting Addition
    $(document).on('click', '#delete-option', function() {
      // Index of the Addition in the Array
      const index = parseInt(
        $(this)
          .parents('.added-addition')
          .attr('data-index')
      );

      // Remove the Addition From the Array
      additions.splice(index);

      // Removing the View
      $(this)
        .parents('.added-addition')
        .parent()
        .remove();
    });
  };
  addProduct();

  /**
   * End of Add Product Functions
   */
});

$('.table')
  .parent()
  .addClass('overflow-auto');
