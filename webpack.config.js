const path = require('path');

module.exports = {
  entry: {
    vendors: './src/vendors.js',
    main: './src/index.js',

    // Provider Entries
    ajaxProviderTable: './src/provider/ajaxProviderTable.js',
    ajaxProviderProfile: './src/provider/ajaxProviderProfile.js',
    ajaxProducts: './src/provider/ajaxProducts.js',
    ajaxEditProvider: './src/provider/ajaxEditProvider.js',

    // Branch Entries
    ajaxBranchTable: './src/branches/ajaxBranchTable.js',
    ajaxBranchProfile: './src/branches/ajaxBranchProfile.js',

    // Orders Entries
    ajaxOrderTable: './src/orders/ajaxOrderTable.js',
    ajaxOrderView: './src/orders/ajaxOrderView.js',
    ajaxOrderEdit: './src/orders/ajaxOrderEdit.js',

    // Notification Entries
    ajaxNotification: './src/notifications/ajaxNotification.js',

    // System Variables Entries
    ajaxAreas: './src/system-variables/ajaxAreas.js',
    ajaxEditArea: './src/system-variables/ajaxEditArea.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  }
};
