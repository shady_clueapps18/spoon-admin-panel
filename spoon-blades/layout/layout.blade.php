@include('partials.header')
@include('partials.top-nav')
@include('partials.sidebar')
 
@yield('content')

@include('partials.footer')

@yield('scripts')

</body>
</html>