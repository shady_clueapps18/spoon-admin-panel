<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

      <!-- Dashboard Nav Item -->
      <li class=" nav-item">
        <a href="index.html">
          <i class="la la-dashboard"></i>
          <span class="menu-title" data-i18n="nav.dash.main">Overview</span>
          <!-- <span class="badge badge badge-info badge-pill float-right mr-2">3</span> -->
        </a>
      </li>
      <!-- End of Dashboard Nav Item -->
      <!-- Users Nav Item -->
      <li class=" nav-item">
        <a href="#">
          <i class="la la-users"></i>
          <span class="menu-title" data-i18n="nav.templates.main">Users</span>
        </a>
        <ul class="menu-content">
          <li>
            <a class="menu-item" href="../vertical-menu-template" data-i18n="nav.templates.vert.classic_menu">
              <i class="la la-user"></i>
              <span>Users Overview</span>
            </a>
          </li>
          <li>
            <a class="menu-item" href="users" data-i18n="nav.templates.vert.classic_menu">
              <i class="la la-user"></i>
              <span>All Users</span>
            </a>
          </li>
          <li>
            <a class="menu-item" href="cars" data-i18n="nav.templates.vert.compact_menu">
              <i class="la la-car"></i>
              <span>Cars</span>
            </a>
          </li>
          <li>
            <a class="menu-item" href="devices" data-i18n="nav.templates.vert.content_menu">
              <i class="la la-mobile"></i>
              <span>Devices</span>
            </a>
          </li>
          <li>
            <a class="menu-item" href="payments" data-i18n="nav.templates.vert.overlay_menu">
              <i class="la la-usd"></i>
              <span>Payments</span>
            </a>
          </li>
          <li>
            <a class="menu-item" href="referrals" data-i18n="nav.templates.vert.overlay_menu">
              <i class="la la-home"></i>
              <span>Referrals</span>
            </a>
          </li>
          <li>
            <a class="menu-item" href="contactUs" data-i18n="nav.templates.vert.overlay_menu">
              <i class="la la-home"></i>
              <span>Messages</span>
            </a>
          </li>
        </ul>
      </li>
      <!-- End of Users Nav Item -->

      <!-- Providers Nav Item -->
      <li class=" nav-item">
        <a href="#">
          <i class="la la-rocket"></i>
          <span class="menu-title" data-i18n="nav.page_layouts.main">Providers</span>
          <!-- <span class="badge badge badge-pill badge-danger float-right mr-2">New</span> -->
        </a>
        <ul class="menu-content">
          <li>
            <a class="menu-item" href="../vertical-content-menu-template" data-i18n="nav.templates.vert.content_menu">
              <i class="la la-home"></i>
              <span>Providers Overview</span>
            </a>
          </li>
          <li>
            <a class="menu-item" href="providersOverView" data-i18n="nav.templates.vert.content_menu">
              <i class="la la-home"></i>
              <span>All Providers</span>
            </a>
          </li>
        </ul>
      </li>
      <!-- End of Providers Nav Item -->

      <!-- Orders Nav Item -->
      <li class=" nav-item">
        <a href="#">
          <i class="la la-shopping-cart"></i>
          <span class="menu-title" data-i18n="nav.navbars.main">Orders</span>
        </a>
        <ul class="menu-content">
          <li>
            <a class="menu-item" href="#" data-i18n="nav.templates.vert.classic_menu">
              <i class="la la-home"></i>
              <span>Orders Overview</span>
            </a>
          </li>
          <li>
            <a class="menu-item" href="orders" data-i18n="nav.templates.vert.classic_menu">
              <i class="la la-home"></i>
              <span>All Orders</span>
            </a>
          </li>
          <li>
            <a class="menu-item" href="../vertical-menu-template" data-i18n="nav.templates.vert.classic_menu">
              <i class="la la-cart-arrow-down"></i>
              <span>Orders Bookings</span>
            </a>
          </li>
          <li>
            <a class="menu-item" href="../vertical-menu-template" data-i18n="nav.templates.vert.classic_menu">
              <i class="la la-cog"></i>
              <span>Reports</span>
            </a>
          </li>
        </ul>
      </li>
      <!-- End of Orders Nav Item -->

      <!-- System Variables -->
      <li class=" nav-item">
        <a href="#">
          <i class="la la-shopping-cart"></i>
          <span class="menu-title" data-i18n="nav.navbars.main">System Variables</span>
        </a>
        <ul class="menu-content">
          <li class=" menu-item">
            <a href="#">
              <i class="la la-download"></i>
              <span class="menu-title" data-i18n="nav.footers.main">General</span>
            </a>
          </li>

          <li class=" menu-item">
            <a href="countries">
              <i class="la la-arrows-h"></i>
              <span class="menu-title" data-i18n="nav.horz_nav.main">Countries</span>
            </a>
          </li>

          <li class=" menu-item">
            <a href="categories">
              <i class="la la-header"></i>
              <span class="menu-title" data-i18n="nav.page_headers.main">Categories</span>
            </a>
          </li>

          <li class=" menu-item">
            <a href="cuisines">
              <i class="la la-download"></i>
              <span class="menu-title" data-i18n="nav.footers.main">Cuisines</span>
            </a>
          </li>

          <li class=" menu-item">
            <a href="features">
              <i class="la la-download"></i>
              <span class="menu-title" data-i18n="nav.footers.main">Features</span>
            </a>
          </li>

          <li class=" menu-item">
            <a href="providersEvents">
              <i class="la la-download"></i>
              <span class="menu-title" data-i18n="nav.footers.main">Events</span>
            </a>
          </li>
          <li class=" menu-item">
            <a href="promoCode">
              <i class="la la-download"></i>
              <span class="menu-title" data-i18n="nav.footers.main">PromoCodes</span>
            </a>
          </li>
        </ul>
      </li>
      <!-- End of System Variables -->

      <!-- Notification Center  -->

      <li class=" nav-item">
        <a href="#">
          <i class="la la-shopping-cart"></i>
          <span class="menu-title" data-i18n="nav.navbars.main">Notifications</span>
        </a>
        <ul class="menu-content">
          <li class=" menu-item">
            <a href="notifications">
              <i class="la la-arrows-h"></i>
              <span class="menu-title" data-i18n="nav.horz_nav.main">New Notifications</span>
            </a>
          </li>
        </ul>
      </li>
      <!-- End of Notification Center -->

      <!-- Settings Nav Item -->
      <li class=" nav-item">
        <a class="menu-item" href="../vertical-menu-template" data-i18n="nav.templates.vert.classic_menu">
          <i class="la la-envelope"></i>
          <span>Settings</span>
        </a>
      </li>
      <!-- End of Messages Nav Item -->

    </ul>
  </div>
</div>