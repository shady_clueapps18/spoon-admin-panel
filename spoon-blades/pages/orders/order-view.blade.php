@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Provider Profile</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
      <div class="content-header-right col-md-6 col-12 mb2">

      </div>
    </div>
    <div class="content-body">
      <!-- CONTENT GOES HERE -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h1>Provider Info</h1>
              <a class="heading-elements-toggle">
                <i class="la la-ellipsis-v font-medium-3"></i>
              </a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li>
                    <a data-action="collapse">
                      <i class="ft-minus"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-content collapse show">
              <div class="card-body">
                <div class="row">
                  <div class="col-12 col-md-6">
                    <div class="order-main-info d-flex">
                      <div class="order-id-type">
                        <div class="order-id text-bold-700 h2">
                          ID :
                          <span class="text-bold-400"></span>
                        </div>
                        <div class="order-type text-bold-700 h2">Type :
                          <span class="text-bold-400"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="order-sec-info">
                      <div class="order-user text-bold-700 h5"> User Name :
                        <a href="" class="text-bold-400"></a>
                      </div>
                      <div class="order-provider text-bold-700 h5"> Provider :
                        <a href="" class="text-bold-400"></a>
                      </div>
                      <div class="order-created-at text-bold-700 h5"> Created At :
                        <span class="text-bold-400"></span>
                      </div>
                      <div class="order-finished-at text-bold-700 h5"> Finished At :
                        <span class="text-bold-400"></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 col-md-6">
                    <div class="order-cash">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <ul>
                              <li class="order-sub-total text-bold-700 h5 no-bullets"> Subtotal Price :
                                <span class="text-bold-400"></span>
                              </li>
                              <li class="order-promo text-bold-700 h5"> Promo Code :
                                <span class="text-bold-400"></span>
                              </li>
                              <li class="order-wallet-discount text-bold-700 h5"> Wallet Discount :
                                <span class="text-bold-400"></span>
                              </li>
                              <li class="order-discounted-sub text-bold-700 h5"> Discounted Subtotal :
                                <span class="text-bold-400"></span>
                              </li>
                              <li class="order-total text-bold-700 h5"> Total Price :
                                <span class="text-bold-400"></span>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="order-status">
                      <h2>Order Status</h2>
                      <div></div>
                    </div>
                    <div class="order-rate-review">
                      <div class="order-rate">
                        <span class="d-inline-block mb-1 h2">Rate</span>
                        <div class="rate-wrapper">
                        </div>
                      </div>
                      <hr>
                      <div class="order-review">
                        <h2>Review</h2>
                        <p class="review-text"></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h1>Order Products</h1>
              <a class="heading-elements-toggle">
                <i class="la la-ellipsis-v font-medium-3"></i>
              </a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li>
                    <a data-action="collapse">
                      <i class="ft-minus"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-content collapse show">
              <div class="card-body">
                <table class="table table-striped table-bordered" id="order-products-table">
                  <thead>
                    <tr>
                      <th>id</th>
                      <th>Name</th>
                      <th>Offer</th>
                      <th>Additions</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script defer>
  const id = window.location.search.split('id=')[1]
  let $id = $('.order-id > span');
  let type = $('.order-type > span');
  let user = $('.order-user > a');
  let provider = $('.order-provider > a');
  let createdAt = $('.order-created-at > span');
  let finishedAt = $('.order-finished-at > span');
  let subTotal = $('.order-sub-total > span');
  let promo = $('.order-promo > span');
  let walletDiscount = $('.order-wallet-discount > span');
  let discountedSubtotal = $('.order-discounted-sub > span')
  let total = $('.order-total > span')
  let $status = $('.order-status > div')
  let $rate = $('.order-rate > .rate-wrapper')
  let review = $('.order-review > review-text');

  let readStatus = (data) => {
    switch (data.status) {
      case "PENDING":
        return $($status).append(`<div class="badge badge-danger">Pending</div>`)
        break;

      case "IN_PROGRESS":
        return $($status).append(`<div class="badge badge-warning">In Progress</div>`)
        break;

      case "APPROVED":
        return $($status).append(`<div class="badge badge-danger">Approved</div>`)
        break;

      case "FINISHED":
        return $($status).append(`<div class="badge badge-danger">Finished</div>`)
        break;

      case "READY":
        return $($status).append(`<div class="badge badge-danger">Ready</div>`)
        break;

      case "CANCELED_BY_CLIENT":
        return $($status).append(`<div class="badge badge-danger">Canceled By Client</div>`)
        break;

      case "CANCELED_BY_ADMIN":
        return $($status).append(`<div class="badge badge-danger">Canceled By Restaurant</div>`)
        break;

      default:
        break;
    }
  }
  let readType = (data) => {
    switch (data.type) {
      case "RESTAURANT_PICKUP":
        return $(type).text(`Restaurant + Restaurant Pickup`)
        break;

      case "CAR_PICKUP":
        return $(type).text(`Restaurant + Car Pickup`)
        break;

      case "BOOKING":
        return $(type).text(`Booking`)
        break;

      case "DINE_IN":
        return $(type).text(`Restaurant + Dine-in`)
        break;

      case "PICKUP":
        return $(type).text(`Grocery Pickup`)
        break;

      default:
        break;
    }
  }

  $.ajax({
    type: 'GET',
    url: `http://spoon.api.myspoon.me/api/admin/v1/orders/${id}`,
    success: ({ data }) => {
      console.log(data)
      $($id).text(data.id);
      $(user).text(data.user_name).attr('href', `userProfile?id=${data.user_id}`)
      $(provider).text(data.provider.name).attr('href', `providerProfile?id=${data.provider.id}`)
      $(createdAt).text(data.created_at)
      $(finishedAt).text(data.finished_at)
      $(subTotal).text(`${data.subtotal_price} EGP`)
      //TODO: Fill the right data for "PromoCode"
      $(promo).text(data.taxes)
      $(walletDiscount).text(`${data.discount_amount} EGP`)
      $(discountedSubtotal).text(`${data.subtotal_price - data.discount_amount} EGP`)
      $(total).text(`${data.total_price} EGP`)
      $(review).text(data.review)

      // Display Rating
      $($rate).raty({
        path: '../../app-assets/images/raty',
        halfShow: true,
        readOnly: true,
        score: data.rate
      })

      readStatus(data);
      readType(data)

    },
    error: err => console.log(err),
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    }
  })

</script>

<script>
  // TODO: Fill the data of the table
  $("#order-products-table").DataTable(
    {
      processing: true,
      ajax: {
        type: 'GET',
        url: "http://spoon.api.myspoon.me/api/admin/v1/providers?",
        dataSrc: "data",
        headers: {
          "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
        },
      },
      columns: [
        { data: "id" },
        { data: data => `<a href='providerProfile?id=${data.id}'>${data.name}</a>` },
        {
          data: (data) => data.branch.grocery == null ? 'Restaurant' : 'Grocery'
        },
        {
          data: (data) => `<a href="branches?id=${data.id}">${data.branch_count}</a>`
        },
        { data: "num_favorite" },
        { data: "order_count" },
        { data: "taxes" },
        {
          mRender: function (data, type, row, meta) {
            return `
                <button class="btn btn-sm btn-cyan btn-view">View</button>
                <button class="btn btn-sm btn-info btn-edit">Edit</button>
                <button class="btn btn-sm btn-danger" id="delete">Delete</button>
              `;
          },
          className: "table-actions"
        }
      ]
    }
  );
</script>
@endsection