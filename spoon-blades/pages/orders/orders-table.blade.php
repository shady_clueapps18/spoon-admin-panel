@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <table class="table table-striped table-bordered text-inputs-searching" id="orders-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Provider</th>
            <th>User Name</th>
            <th>Type</th>
            <th>Status</th>
            <th>QTY</th>
            <th>Total Price</th>
            <th>Rate</th>
            <th>Created At</th>
            <th>Finished At</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>

    </div>
  </div>
</div>

<!-- Track Modal -->
<div class="modal fade" id="track-order-modal" tabindex="-1" role="dialog" aria-labelledby="track-order-modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Track Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="order-timeline">
          <div class="order-timeline-content">
            <h2>Created</h2>
            <p class="track-created"></p>
          </div>
          <div class="order-timeline-content">
            <h2>Approved</h2>
            <p class="track-approved"></p>
          </div>
          <div class="order-timeline-content">
            <h2>In Progress</h2>
            <p class="track-in-progress"></p>
          </div>
          <div class="order-timeline-content">
            <h2>Ready</h2>
            <p class="track-ready"></p>
          </div>
          <div class="order-timeline-content">
            <h2>Finished</h2>
            <p class="track-finished"></p>
          </div>
          <div class="order-timeline-content">
            <h2>Arrived</h2>
            <p class="track-arrived"></p>
          </div>
          <div class="order-timeline-content">
            <h2>Canceled</h2>
            <p class="track-canceled"></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Change Order Status Modal -->
<div class="modal fade" id="change-order-status" tabindex="-1" role="dialog" aria-labelledby="change-order-status" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Change Order Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="badge badge-light change-pending d-none">Pending</div>
        <div class="badge badge-light change-approved">Approved</div>
        <div class="badge badge-light change-in-progress">In Progress</div>
        <div class="badge badge-light change-ready">Ready</div>
        <div class="badge badge-light change-finished">Finished</div>
        <div class="badge badge-light change-canceled-client d-none">Canceled by Client</div>
        <div class="badge badge-light change-canceled-admin">Canceled by Admin</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split('id=')[1]
  const ordersTable = $("#orders-table").DataTable(
    {
      processing: true,
      ajax: {
        type: 'GET',
        url: "http://spoon.api.myspoon.me/api/admin/v1/orders?",
        dataSrc: "data",
        headers: {
          "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
        },
      },
      columns: [
        { data: "id" },
        { data: data => `<a href='../providers/provider-profile.html?id=${data.provider.id}'>${data.provider.name}</a>` },
        { data: data => `<a href="../user/user-profile.html?id=${data.user_id}">${data.user_name}</a>` },
        {
          data: data => {
            switch (data.type) {
              case "RESTAURANT_PICKUP":
                return `<a href="">Restaurant + Restaurant Pickup</a>`
                break;

              case "CAR_PICKUP":
                return `<a href="">Restaurant + Car Pickup</a>`
                break;

              case "BOOKING":
                return `<a href="">Booking</a>`
                break;

              case "DINE_IN":
                return `<a href="">Restaurant + Dine-in</a>`
                break;

              case "PICKUP":
                return `<a href="">Grocery Pickup</a>`
                break;

              default:
                break;
            }
          }
        },
        {
          data: data => {
            switch (data.status) {
              case "PENDING":
                return `<a href="#" data-toggle="modal" class="change-status" data-target="#change-order-status">Pending</a>`
                break;

              case "IN_PROGRESS":
                return `<a href="#" data-toggle="modal" class="change-status" data-target="#change-order-status">In Progress</a>`
                break;

              case "APPROVED":
                return `<a href="#" data-toggle="modal" class="change-status" data-target="#change-order-status">Approved</a>`
                break;

              case "FINISHED":
                return `<a href="#" data-toggle="modal" class="change-status" data-target="#change-order-status">Finished</a>`
                break;

              case "READY":
                return `<a href="#" data-toggle="modal" class="change-status" data-target="#change-order-status">Ready</a>`
                break;

              case "CANCELED_BY_CLIENT":
                return `<a href="#" data-toggle="modal" class="change-status" data-target="#change-order-status">Canceled By Client</a>`
                break;

              case "CANCELED_BY_ADMIN":
                return `<a href="#" data-toggle="modal" class="change-status" data-target="#change-order-status">Canceled By Restaurant</a>`
                break;

              default:
                break;
            }
          }
        },
        {
          data: data => {
            return `<a href="${data.id}">${data.order_products.reduce((acc, curr) => acc + curr.quantity, 0)}</a>`
          }
        },
        { data: data => `${data.total_price} EGP` },
        { data: "rate" },
        { data: "created_at" },
        { data: "finished_at" },
        {
          mRender: function (data, type, row, meta) {
            return `
                    <a href="./order-view.html?id=" class="btn btn-sm btn-cyan view-order">View</a>
                    <a href="" class="btn btn-sm btn-info track-order" data-toggle="modal" data-target="#track-order-modal">Track</a>
                    <a href="" class="btn btn-sm btn-danger edit-order">Edit</a>
                    <button class="btn btn-sm btn-danger delete-order">Delete</button>
                  `;
          },
          className: "table-actions"
        }
      ]
    }
  );

  const addSorting = (wrapper, id, title, ...options) => {
    const markup = `<div class="sort-by-wrapper d-flex align-items-center w-50">
            <label ckass="w-30"> ${title} </label>
            <select class="sort-by form-control ml-1" id="${id}">
            </select>
      </div >`;
    $(wrapper).append(markup);
    options.forEach(opt => $('.sort-by-wrapper').find(`#${id}`).append(`<option value="${opt}">${opt}</option>`))
    $('.sort-by').find('option:first-of-type').val('')
    options = []
  };
  addSorting(
    '#orders-table_filter',
    'order-type-sort',
    'Order Type:',
    'All',
    'Restaurant + Car Pickup',
    'Restaurant + Restaurant Pickup',
    'Restaurant + Dine-in',
    'Grocery Pickup',
    'Booking');

  addSorting(
    '#orders-table_filter',
    'status-sort',
    'Status: ',
    'All',
    'Pending',
    'Approved',
    'In Progress',
    'Ready',
    'Finished',
    'Canceled By Customer',
    'Canceled By Restaurant',
    'Rated');

  addSorting(
    '#orders-table_length',
    'search-sort',
    'Search: ',
    'Choose Search Parameter',
    'User Id',
    'Order Id',
    'User Mobile',
    'Provider Name');

  $('#orders-table_filter')
    .parent()
    .addClass('col-md-8')
    .removeClass('col-md-6')

  $('#orders-table_length')
    .parent()
    .addClass('col-md-4')
    .removeClass('col-md-6')

  // TODO: Test and Refactor this mess!

  $('#order-type-sort').on('change', function () {
    ordersTable.column(3).search($(this).val(), false, false).draw()
  });

  $('#status-sort').on('change', function () {
    ordersTable.column(4).search($(this).val(), false, false).draw()
  });


  $('#search-sort').on('change', function () {
    let doneTypingInterval = 2000;

    // Append the search input
    $(this).parent().append(`<input type="text" id="search-sort-input">`)

    const searchOrderId = () => {
      let typingTimer;

      //on keyup, start the countdown
      $(document).on('keyup', '#search-sort-input', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
      });

      //user is "finished typing," do something
      function doneTyping() {
        if ($('#search-sort-input').val()) {
          ordersTable.column(0).search($('#search-sort-input').val(), false, false).draw()
        } else {
          ordersTable.ajax.reload()
        }
      }
    }

    // Start Searching after a Delay
    const searchUserId = () => {
      let typingTimer;


      //on keyup, start the countdown
      $(document).on('keyup', '#search-sort-input', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
      });

      //user is "finished typing," do something
      function doneTyping() {
        if ($('#search-sort-input').val()) {
          $.ajax({
            type: 'GET',
            url: 'http://spoon.api.myspoon.me/api/admin/v1/orders',
            headers: {
              "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
            },
            success: ({ data }) => {
              const selectedUserId = data.find((user) => user.user_id == $('#search-sort-input').val())
              ordersTable.clear().draw()
              ordersTable.rows.add([selectedUserId]).draw()
            },
            error: err => console.log(err)
          })
        } else {
          ordersTable.ajax.reload()
        }
      }
    }

    const searchUserMobile = () => {
      let typingTimer;

      //on keyup, start the countdown
      $(document).on('keyup', '#search-sort-input', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
      });

      //user is "finished typing," do something
      function doneTyping() {
        if ($('#search-sort-input').val()) {
          $.ajax({
            type: 'GET',
            url: 'http://spoon.api.myspoon.me/api/admin/v1/orders',
            headers: {
              "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
            },
            success: ({ data }) => {
              const selectedUserMobile = data.find((user) => user.user_mobile == $('#search-sort-input').val())
              ordersTable.clear().draw()
              ordersTable.rows.add([selectedUserMobile]).draw()
            },
            error: err => console.log(err)
          })
        } else {
          ordersTable.ajax.reload()
        }
      }
    }

    const searchProviderName = () => {
      let typingTimer;

      //on keyup, start the countdown
      $(document).on('keyup', '#search-sort-input', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
      });

      //user is "finished typing," do something
      function doneTyping() {
        if ($('#search-sort-input').val()) {
          $.ajax({
            type: 'GET',
            url: 'http://spoon.api.myspoon.me/api/admin/v1/orders',
            headers: {
              "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
            },
            success: ({ data }) => {
              const selectedProvider = data
                .find((user) => (user.provider.name.toLowerCase() == $('#search-sort-input').val().toLowerCase()) ||
                  (user.provider.name.toLowerCase().includes($('#search-sort-input').val().toLowerCase())))
              ordersTable.clear().draw()
              ordersTable.rows.add([selectedProvider]).draw()
            },
            error: err => console.log(err)
          })
        } else {
          ordersTable.ajax.reload()
        }
      }
    }



    switch ($(this).val()) {
      case 'User Id':
        searchUserId()
        break;

      case 'Order Id':
        searchOrderId()
        break;

      case 'User Mobile':
        searchUserMobile()
        break;

      case 'Provider Name':
        searchProviderName()
        break;

      default:
        break;
    }
  });

  $(document).on('click', '.delete-order', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    console.log($id)
    $.ajax({
      type: 'DELETE',
      url: `https://spoon.api.myspoon.me/api/admin/v1/orders/${$id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: {
        id: $id
      },
      success: data => {
        $(this).parents('tr').remove()
        swal(
          'Success!',
          'Order has been deleted successfully',
          'success'
        )
      },
      error: err => console.log(err)
    });
  });

  $(document).on('click', '.edit-order', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    $(this).attr('href', `editOrder?id=${$id}`)
  });

  $(document).on('click', '.view-order', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    $(this).attr('href', `viewOrder.html?id=${$id}`)
  });

  $(document).on('click', '.track-order', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    $.ajax({
      type: 'GET',
      url: `https://spoon.api.myspoon.me/api/admin/v1/orders/${$id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      success: ({ data }) => {
        $('.track-created').text(data.created_at === '' ? 'Not Yet' : data.created_at)
        $('.track-approved').text(data.approved_at === '' ? 'Not Yet' : data.approved_at)
        $('.track-in-progress').text(data.in_progress_at === '' ? 'Not Yet' : data.in_progress_at)
        $('.track-ready').text(data.ready_at === '' ? 'Not Yet' : data.ready_at)
        $('.track-finished').text(data.finished_at === '' ? 'Not Yet' : data.finished_at)
        $('.track-arrived').text(data.arrived_at === '' ? 'Not Yet' : data.arrived_at)
        $('.track-canceled').text(data.canceled_at === '' ? 'Not Yet' : data.canceled_at)
      },
      error: err => console.log(err)
    })
  });

  $(document).on('click', '.change-status', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    const orderStatusOrder = ['PENDING', 'APPROVED', 'IN_PROGRESS', 'READY', 'FINISHED', 'CANCELED_BY_CLIENT', 'CANCELED_BY_ADMIN'];
    $.ajax({
      type: 'GET',
      url: `https://spoon.api.myspoon.me/api/admin/v1/orders/${$id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      success: ({ data }) => {
        if (data.status === 'CANCELED_BY_CLIENT') {
          $('#change-order-status .modal-body .badge').addClass('d-none');
          $('#change-order-status .modal-body .change-canceled-client').removeClass('d-none badge-light').addClass('badge-primary');
        } else {
          $('#change-order-status .modal-body .badge').removeClass('d-none')
          $('#change-order-status .modal-body .change-canceled-client').removeClass('badge-primary').addClass('d-none badge-light');
          $('#change-order-status .modal-body .change-pending').addClass('d-none');

          const currentStatusIndex = orderStatusOrder.findIndex(status => status === data.status)
          $('#change-order-status .modal-body .badge').each(function (i) {
            i <= currentStatusIndex && $(this).removeClass('badge=light').addClass('badge-primary')
          });
        }
      },
      error: err => console.log(err)
    });
  });

</script>
@endsection