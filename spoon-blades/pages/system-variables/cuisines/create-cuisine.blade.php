@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <section id="validation">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Add Cuisine</h4>
                <a class="heading-elements-toggle">
                  <i class="la la-ellipsis-h font-medium-3"></i>
                </a>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <form enctype="multipart/form-data" id="add-cuisines-form">
                    <div class="row">
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label for="cuisine-name">Name</label>
                          <input type="text" id="cuisine-name" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="cuisine-order">Order</label>
                          <input type="number" id="cuisine-order" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-12 col-md-6">
                        <div id="cuisine-img"></div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary" id="add-menu">Add Cuisine</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Form wizard with step validation section end -->

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split("id=")[1]
  $('#add-cuisines-form').on('submit', function (e) {
    const name = $('#cuisine-name').val();
    const order = $('#cuisine-order').val();
    const image = $('.dz-image > img').attr('src').split('base64,')[1]
    e.preventDefault();
    $.ajax({
      type: 'POST',
      url: 'http://spoon.api.myspoon.me/api/admin/v1/cuisines',
      headers: {
        Authorization:
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
      },
      data: {
        name,
        order,
        image
      },
      // TODO: Add Sweet Alert to notify the user that the form is submitted
      success: () => $('#add-category-form')[0].reset(),
      error: err => console.log(err)
    });
  });
</script>
@endsection