@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <table class="table table-striped table-bordered text-inputs-searching" id="cuisines-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Image</th>
            <th>Order</th>
            <th>Created At</th>
            <th>Number of Providers</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split('id=')[1]
  const providers = [];
  let provider = {}
  $("#cuisines-table").DataTable(
    {
      processing: true,
      ajax: {
        type: 'GET',
        url: "http://spoon.api.myspoon.me/api/admin/v1/cuisines?",
        dataSrc: "data",
        headers: {
          "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
        },
      },
      columns: [
        { data: "id" },
        { data: "name" },
        {
          data: data => `<div style="max-width: 150px; max-height: 150px">
                            <img style="width: 100%; height: 100%" src="${data.image}" />
                           </div>`
        },
        { data: "order" },
        { data: "created_at" },
        { data: data => `<a href="" class="provider-count">${data.provider_count}</a>` },
        {
          mRender: function (data, type, row, meta) {
            return `
                <a href="" class="btn btn-sm btn-info cuisine-edit">Edit</a>
                <button class="btn btn-sm btn-danger cuisine-delete">Delete</button>
              `;
          },
          className: "table-actions"
        }
      ]
    }
  );

  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/admin/v1/cuisines',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      data.forEach(elm => {
        provider.id = elm.id;
        provider.provider = elm.provider
        providers.push(provider);
        provider = {}
      });
      console.log(providers);
    },
    error: err => console.log(err)
  })

  $(document).on('click', '.provider-count', function (e) {
    e.preventDefault();
    const $providerId = $(this).parents('tr').find('td:first-of-type').text()
    const providerIds = [];
    const selectedProvider = providers.find(provider => provider.id == $providerId).provider
    selectedProvider.forEach(provider => providerIds.push(provider.name))
    const redirectLink = `providersOverview?providers=${providerIds.toString()}`
    window.location = redirectLink
  });

  $(document).on('click', '.cuisine-delete', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    console.log($id)
    $.ajax({
      type: 'DELETE',
      url: `https://spoon.api.myspoon.me/api/admin/v1/cuisines/${$id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: {
        id: $id
      },
      success: data => {
        $(this).parents('tr').remove()
        swal(
          'Success!',
          'Cuisine has been removed successfully!',
          'success'
        )
      },
      error: err => console.log(err)
    });
  });

  $(document).on('click', '.cuisine-edit', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    $(this).attr('href', `editCuisine?id=${$id}`)
  })
</script>
@endsection