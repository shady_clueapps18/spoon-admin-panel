@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <table class="table table-striped table-bordered text-inputs-searching" id="promo-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Code</th>
            <th>Owner</th>
            <th>Discount Amount</th>
            <th>Wallet</th>
            <th>Usage</th>
            <th>Status</th>
            <th>Created At</th>
            <th>Expired At.</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split("id=")[1];
  const promoTable = $("#promo-table").DataTable(
    {
      processing: true,
      ajax: {
        type: 'GET',
        url: "http://spoon.api.myspoon.me/api/admin/v1/promoCode?",
        dataSrc: "data",
        headers: {
          "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
        },
      },
      columns: [
        { data: "id" },
        { data: "code" },
        { data: "promocode_type" },
        { data: data => data.discount_type === "fixed" ? `${data.discount_amount} EGP` : `${data.discount_amount} %` },
        { data: data => `${data.wallet_amount} ${data.wallet_type === 'fixed' ? 'EGP' : '%'}` },
        { data: data => `<a href="" class="promo-usage">${data.max_usage}</a>` },
        { data: data => data.is_active === 1 ? "Active" : "Inactive" },
        { data: "created_at" },
        { data: "expired_at" },
        {
          mRender: function (data, type, row, meta) {
            return `
                <a href="" class="btn btn-sm btn-primary promo-view">View</a>
                <button class="btn btn-sm btn-info promo-edit">Edit</button>
                <button class="btn btn-sm btn-danger promo-delete">Delete</button>
              `;
          },
          className: "table-actions"
        }
      ]
    }
  );

  $(document).on('click', '.promo-delete', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    console.log($id)
    $.ajax({
      type: 'DELETE',
      url: `https://spoon.api.myspoon.me/api/admin/v1/promoCode/${$id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: {
        id: $id
      },
      success: ({ data }) => {
        $(this).parents('tr').remove()
        // Sweet Alert
        swal(
          'Success!',
          `Provider has been successfully removed!`,
          'success'
        )
      },
      error: err => console.log(err)
    });
  });

  $(document).on('click', '.promo-usage', function (e) {
    e.preventDefault()
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    $.ajax({
      type: 'GET',
      // TODO: Get the order count and object from Mario...
      url: ''
    })
  });

  $(document).on('click', '.promo-view', function (e) {
    e.preventDefault()
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    const redirectLink = `./promocodeView.html?id=${$id}`
    window.location = redirectLink;
  });

  // $(document).on('change', '.solved-messages', function () {
  //   const $id = $(this).parents('tr').find('td:first-of-type').text()
  //   const data = { "is_solved": $(this).prop('checked') === true ? 1 : 0 }
  //   $.ajax({
  //     type: 'PUT',
  //     url: `https://spoon.api.myspoon.me/api/admin/v1/contactUs/${$id}`,
  //     headers: {
  //       "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
  //     },
  //     data: data,
  //     success: data => swal(
  //       'Success!',
  //       `Provider has been successfully removed!`,
  //       'success'
  //     ),
  //     error: err => console.log(err)
  //   })
  // });
</script>
@endsection