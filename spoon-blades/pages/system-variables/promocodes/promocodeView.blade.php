@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Provider Profile</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
      <div class="content-header-right col-md-6 col-12 mb2">
        <div class="promo-actions d-flex justify-content-end">
          <button class="btn btn-primary mr-2 px-2">Edit </button>
        </div>
      </div>
    </div>
    <div class="content-body">
      <!-- CONTENT GOES HERE -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h1>Promo Card Overview</h1>
              <a class="heading-elements-toggle">
                <i class="la la-ellipsis-v font-medium-3"></i>
              </a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li>
                    <a data-action="collapse">
                      <i class="ft-minus"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-content collapse show">
              <div class="card-body">
                <div class="row">
                  <div class="col-12 col-md-6">
                    <div class="promo-main-info d-flex">
                      <div class="promo-id-name w-100">
                        <div class="promo-id text-bold-700 h2">
                          ID :
                          <span class="text-bold-400"></span>
                        </div>
                        <div class="promo-name text-bold-700 h2">Name :
                          <span class="text-bold-400"></span>
                        </div>
                        <div class="promo-status text-bold-700 h2 d-flex">Status :
                          <select name="promoStatus" id="promo-status" class="form-control w-25 ml-2">
                            <option value="active">Active</option>
                            <option value="inactive">Inactive</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="promo-sec-info">
                      <div class="promo-owner text-bold-700 h5"> Owner :
                        <span class="text-bold-400"></span>
                      </div>
                      <div class="promo-discount-amount text-bold-700 h5"> Discount Amount :
                        <span class="text-bold-400"></span>
                      </div>
                      <div class="promo-discount-type text-bold-700 h5"> Discount Type :
                        <span class="text-bold-400"></span>
                      </div>
                      <div class="promo-max-discount-amount text-bold-700 h5"> Max Discount Amount :
                        <span class="text-bold-400"></span>
                      </div>
                      <div class="promo-created-at text-bold-700 h5"> Created At :
                        <span class="text-bold-400"></span>
                      </div>
                      <div class="promo-expired-at text-bold-700 h5"> Expired At :
                        <span class="text-bold-400"></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 col-md-6">
                    <div class="promo-usage-info">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <ul>
                              <li class="promo-usage text-bold-700 h5 no-bullets"> Max Usage :
                                <span class="text-bold-400"></span>
                              </li>
                              <li class="promo-max-user-usage text-bold-700 h5"> Max. Usage Per User :
                                <span class="text-bold-400"></span>
                              </li>
                              <li class="promo-min-order-usage text-bold-700 h5"> Min. Order for Usage :
                                <span class="text-bold-400"></span>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="promo-wallet-info">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <ul>
                              <li class="promo-wallet text-bold-700 h5 no-bullets"> Wallet :
                                <span class="text-bold-400"></span>
                              </li>
                              <li class="promo-max-wallet-usage text-bold-700 h5"> Max. Wallet Amount :
                                <span class="text-bold-400"></span>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  let id = window.location.search.split("id=")[1];
  let $id = $('.promo-id > span')
  let name = $('.promo-name > span')
  let status = $('.promo-status');
  let owner = $('.promo-owner > span')
  let discountAmount = $('.promo-discount-amount > span')
  let discountType = $('.promo-discount-type > span')
  let maxDiscountAmount = $('.promo-max-discount-amount > span')
  let createdAt = $('.promo-created-at > span')
  let expireddAt = $('.promo-expired-at > span')
  let usage = $('.promo-usage > span')
  let maxUserUsage = $('.promo-max-user-usage > span');
  let minOrderUsage = $('.promo-min-order-usage > span');
  let wallet = $('.promo-wallet > span')
  let maxWalletUsage = $('.promo-max-wallet-usage > span');

  $.ajax({
    type: 'GET',
    url: `http://spoon.api.myspoon.me/api/admin/v1/promoCode/${id}`,
    success: ({ data }) => {
      $($id).text(data.id)
      $(name).text(data.code)
      $(status).val(data.is_active === 1 ? 'active' : 'inactive')
      $(owner).text(data.promocode_type)
      $(discountAmount).text(`${data.discount_amount} ${data.discount_type === 'fixed' ? 'EGP' : '%'}`)
      $(discountType).text(data.discount_type === 'fixed' ? 'Fixed' : 'Percentage')
      // TODO: Fill the right data of max discount amount
      $(maxDiscountAmount).text(`${data.discount_amount} ${data.discount_type === 'fixed' ? 'EGP' : '%'}`)
      $(createdAt).text(data.created_at)
      $(expireddAt).text(data.expired_at)
      $(usage).text(data.max_usage)
      $(maxUserUsage).text(data.max_user_usage)
      $(minOrderUsage).text(`${data.min_order_total} EGP`)
      $(wallet).text(`${data.wallet_amount} ${data.wallet_type === 'FIXED' ? 'EGP' : '%'}`)
      $(maxWalletUsage).text(`${data.max_wallet} ${data.wallet_type === 'FIXED' ? 'EGP' : '%'}`)
    },
    error: err => console.log(err),
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    }
  })

  $(status).on('change', function () {
    $.ajax({
      type: 'PUT',
      url: `http://spoon.api.myspoon.me/api/admin/v1/promoCode/${id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: {
        is_active: $(status).val() === 'active' ? 1 : 0
      },
      success: data => console.log(data),
      error: err => console.log(err)
    })
  })

</script>
@endsection