@extends('layout.layout')

@section('name')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <section id="validation">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Add Promo Code</h4>
                <a class="heading-elements-toggle">
                  <i class="la la-ellipsis-h font-medium-3"></i>
                </a>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <form enctype="multipart/form-data" id="add-promo-form">
                    <div class="row">
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label for="promo-name">Name</label>
                          <input type="text" id="promo-name" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="promo-owner">Promo Code Owner</label>
                          <select name="promoOwner" id="promo-owner" class="form-control" required>
                            <option value="spoon">Spoon</option>
                            <option value="branch">Branch</option>
                            <option value="spoon-branch">Spoon and a branch</option>
                          </select>
                        </div>
                        <div class="form-group row d-none" id="promo-shares">
                          <div class="col-12 col-md-6">
                            <label for="spoon-promo-share">Spoon Share</label>
                            <input type="number" id="spoon-promo-share" class="form-control">
                          </div>
                          <div class="col-12 col-md-6">
                            <label for="branch-promo-share">Branch Share</label>
                            <input type="number" id="branch-promo-share" class="form-control">
                          </div>
                        </div>
                        <div class="row d-none" id="spoon-branch-share">
                          <div class="col-12 col-md-6">
                            <div class="form-group">
                              <label for="spoon-share">Spoon Amount</label>
                              <input type="number" id="spoon-share" class="form-control">
                            </div>
                          </div>
                          <div class="col-12 col-md-6">
                            <div class="form-group">
                              <label for="branch-share">Branch Amount</label>
                              <input type="number" id="branch-share" class="form-control">
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-6">
                            <label for="promo-wallet">Wallet Amount</label>
                            <input type="number" value="0" id="promo-wallet" class="form-control">
                          </div>
                          <div class="col-6">
                            <label for="promo-wallet-type">Fixed / Percentage</label>
                            <select name="promoWalletType" id="promo-wallet-type" class="form-control">
                              <option value="fixed">Fixed</option>
                              <option value="percentage">Percentage</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="payment-method">Payment Method</label>
                          <select name="paymentMethod" id="payment-method" class="form-control">
                            <option value="CASH">Cash</option>
                            <option value="CREDIT">Credit Card</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="promo-max-wallet">Max. Wallet Amount</label>
                          <input type="number" id="promo-max-wallet" class="form-control">
                        </div>
                        <div class="form-group">
                          <label for="promo-expire">Expired At</label>
                          <div class="row">
                            <div class="col-12 col-md-6">
                              <input type="text" id="promo-expire-date" class="form-control" placeholder="Date" required>
                            </div>
                            <div class="col-12 col-md-6">
                              <input type="text" id="promo-expire-time" class="form-control" placeholder="Time" required>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="max-order-total">Max Order for Usage</label>
                          <input type="number" id="max-order-total" class="form-control">
                        </div>
                      </div>
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label for="promo-status">Promo Code Status</label>
                          <select name="promoStatus" id="promo-status" class="form-control">
                            <option value="active">Active</option>
                            <option value="inactive">Inactive</option>
                          </select>
                        </div>
                        <div class="form-group row">
                          <div class="col-6">
                            <label for="promo-discount">Discount Amount</label>
                            <input type="number" value="0" id="promo-discount" class="form-control" required>
                          </div>
                          <div class="col-6">
                            <label for="promo-discount-type">Fixed / Percentage</label>
                            <select name="promoDiscountType" id="promo-discount-type" class="form-control">
                              <option value="fixed">Fixed</option>
                              <option value="percentage">Percentage</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <!-- TODO: Ask Mario About 'ALL' type and what are the types? -->
                          <label for="order-type">Order Type</label>
                          <select name="orderType" id="order-type" class="form-control" multiple>
                            <option value="DINE_IN">Dine-In</option>
                            <option value="PICKUP">Pickup</option>
                            <option value="CAR_PICKUP">Car Pickup</option>
                            <option value="RESTAURANT_PICKUP">Restaurant Pickup</option>
                            <option value="BOOKING">Booking</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="promo-usage">Usage</label>
                          <input type="number" id="promo-usage" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="promo-usage-user">Max Usage Per User</label>
                          <input type="number" id="promo-usage-user" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="min-order-promo-usage">Min Order for Usage</label>
                          <input type="number" id="min-order-promo-usage" class="form-control" required>
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary" id="add-promo">Add Promo</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Form wizard with step validation section end -->

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split("id=")[1]

  $('#promo-expire-date').dateDropper();
  $('#promo-expire-time').timeDropper({ format: 'HH:mm' });

  $('#order-type').select2();

  $('#promo-wallet').on('keyup change', function () {
    $(this).val() > 0 ? $('#promo-max-wallet').prop('required', true) : $('#promo-max-wallet').prop('required', false)
  })

  $('#promo-owner').on('change', function () {
    $(this).val() === 'spoon-branch' ? $('#promo-shares').removeClass('d-none') :
      $('#promo-shares').addClass('d-none');

  });

  $.ajax({
    type: 'GET',
    url: `http://spoon.api.myspoon.me/api/admin/v1/promoCode/${id}`,
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    },
    success: ({ data }) => {
      $('#promo-name').val(data.code);
      $('#promo-owner').val(data.promocode_type)
      $('#spoon-promo-share').val(data.spoon_discount_amount)
      $('#branch-promo-share').val(data.branch_discount_amount)
      $('#promo-wallet').val(data.wallet_amount)
      $('#promo-wallet-type').val(data.wallet_type)
      $('#promo-max-wallet').val(data.max_wallet)
      $('#promo-expire-date').val(data.expired_at.split(' ')[0].replace(/\-/g, '/'))
      $('#promo-expire-time').val(`${data.expired_at.split(' ')[1]}`)
      $('#promo-status').val(data.is_active === 1 ? 'active' : 'inactive')
      $('#promo-discount').val(data.discount_amount)
      $('#promo-discount-type').val(data.discount_type)
      $('#promo-usage').val(data.max_usage)
      $('#promo-usage-user').val(data.max_user_usage)
      $('#min-order-promo-usage').val(data.min_order_total)
      $('#max-order-total').val(data.max_order_total)
      $('#order-type').val(data.order_type)
      $('#order-type').trigger('change')
      $('#payment-method').val(data.payment_method)

    },
    error: err => console.log(err)
  })

  $('#add-promo-form').on('submit', function (e) {
    e.preventDefault();
    const name = $('#promo-name').val();
    const owner = $('#promo-owner').val()
    const spoonShare = $('#spoon-promo-share').val()
    const branchShare = $('#branch-promo-share').val()
    const walletAmount = $('#promo-wallet').val()
    const walletType = $('#promo-wallet-type').val()
    const maxWalletAmount = $('#promo-max-wallet').val()
    const expire = `${$('#promo-expire-date').val().replace(/\//g, '-')} ${$('#promo-expire-time').val()}:00`
    const status = $('#promo-status').val()
    const discount = $('#promo-discount').val()
    const discountType = $('#promo-discount-type').val()
    const usage = $('#promo-usage').val()
    const maxUserUsage = $('#promo-usage-user').val()
    const minOrderUsage = $('#min-order-promo-usage').val()
    const maxOrderTotal = $('#max-order-total').val()
    const orderType = $('#order-type').val()
    const paymentMethod = $('#payment-method').val()

    $.ajax({
      type: 'PUT',
      url: `http://spoon.api.myspoon.me/api/admin/v1/promoCode/${id}`,
      headers: {
        Authorization:
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
      },
      data: {
        code: name,
        promocode_type: owner,
        discount_type: discountType,
        discount_amount: discount,
        wallet_type: walletType === 'fixed' ? 1 : 2,
        wallet_amount: walletAmount,
        max_wallet: maxWalletAmount,
        spoon_discount_amount: owner === 'spoon-branch' ? spoonShare : owner === 'spoon' ? discount : 0,
        branch_discount_amount: owner === 'spoon-branch' ? branchShare : owner === 'branch' ? discount : 0,
        order_type: orderType,
        max_usage: usage,
        max_user_usage: maxUserUsage,
        min_order_total: minOrderUsage,
        max_order_total: maxOrderTotal,
        payment_method: paymentMethod,
        is_active: status === 'active' ? 1 : 0
      },
      success: () => swal(
        'Success!',
        `PromoCode has been successfully added!`,
        'success'
      ),
      error: err => console.log(err)
    });
  });
</script>
@endsection