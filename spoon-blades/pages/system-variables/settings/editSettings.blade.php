@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <section id="validation">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Edit Settings</h4>
                <a class="heading-elements-toggle">
                  <i class="la la-ellipsis-h font-medium-3"></i>
                </a>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <form enctype="multipart/form-data" id="edit-settings-form">
                    <div class="row">
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label for="settings-status">App Current Status</label>
                          <select name="settingsStatus" id="settings-status" class="form-control" required>
                            <option value="active">Active</option>
                            <option value="inactive">Inactive</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="settings-android">Android Version</label>
                          <input type="text" id="settings-android" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="settings-ios">iOS Version</label>
                          <input type="text" id="settings-ios" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="settings-wallet-percentage">Wallet Percentage</label>
                          <input type="number" id="settings-wallet-percentage" class="form-control">
                        </div>
                        <div class="form-group">
                          <label for="settings-max-wallet">Max. Wallet Discount</label>
                          <input type="number" id="settings-max-wallet" class="form-control">
                        </div>
                        <div class="form-group">
                          <label for="settings-taxes">Taxes</label>
                          <input type="number" id="settings-taxes" class="form-control">
                        </div>
                      </div>
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label for="settings-slots">Slots Number</label>
                          <input type="number" id="settings-slots" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="settings-min">Minutes Number</label>
                          <input type="number" id="settings-min" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="settings-fixed-commission">Fixed Commission</label>
                          <input type="number" id="settings-fixed-commission" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="settings-percent-commission">Percentage Commission</label>
                          <input type="number" id="settings-percent-commission" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="settings-referral-amount">Referral Amount</label>
                          <input type="number" id="settings-referral-amount" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="settings-referral-duration">Referral Duration</label>
                          <input type="number" id="settings-referral-duration" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="settings-payment-fees">Online Payment Fees</label>
                          <input type="number" id="settings-payment-fees" class="form-control">
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary" id="edit-settings">Edit Settings</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Form wizard with step validation section end -->

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split("id=")[1]
  const status = $('#settings-status')
  const andrioid = $('#settings-android')
  const ios = $('#settings-ios')
  const walletPercentage = $('#settings-wallet-percentage')
  const maxWalletDiscount = $('#settings-max-wallet')
  const taxes = $('#settings-taxes')
  const slots = $('#settings-slots')
  const mins = $('#settings-min');
  const fixedCommission = $('#settings-fixed-commission')
  const percentCommission = $('#settings-percent-commission')
  const referralAmount = $('#settings-referral-amount')
  const referralDuration = $('#settings-referral-duration')
  const onlinePaymentFees = $('#settings-payment-fees')

  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/admin/v1/setting',
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    },
    success: ({ data }) => {
      $(status).val(data[0].app_active === 1 ? 'active' : 'inactive')
      $(andrioid).val(data[0].android_version)
      $(ios).val(data[0].ios_version)
      $(slots).val(data[0].num_slot)
      $(mins).val(data[0].num_minute)
      $(walletPercentage).val(data[0].wallet_percentage)
      $(maxWalletDiscount).val(data[0].max_wallet_discount)
      $(fixedCommission).val(data[0].fixed_commission)
      $(percentCommission).val(data[0].perecentage_commission)
      $(taxes).val(data[0].taxes)
      $(onlinePaymentFees).val(data[0].online_payment_fees)
      $(referralAmount).val(data[0].referral_amount)
      $(referralDuration).val(data[0].referral_duration)
    },
    error: err => console.log(err)
  })

  $('#edit-settings-form').on('submit', function (e) {
    e.preventDefault();
    const data = {
      app_active: $(status).val() === 'active' ? 1 : 0,
      android_version: $(andrioid).val(),
      ios_version: $(ios).val(),
      num_slot: $(slots).val(),
      num_minute: $(mins).val(),
      wallet_percentage: $(walletPercentage).val(),
      max_wallet_discount: $(maxWalletDiscount).val(),
      fixed_commission: $(fixedCommission).val(),
      perecentage_commission: $(percentCommission).val(),
      taxes: $(taxes).val(),
      online_payment_fees: $(onlinePaymentFees).val(),
      referral_amount: $(referralAmount).val(),
      referral_duration: $(referralDuration).val()
    }

    $.ajax({
      type: 'PUT',
      url: 'http://spoon.api.myspoon.me/api/admin/v1/setting/1',
      headers: {
        Authorization:
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
      },
      data: data,
      success: () => swal(
        'Success!',
        `Settings has been successfully updated!`,
        'success'
      ),
      error: err => console.log(err)
    });
  });
</script>
@endsection