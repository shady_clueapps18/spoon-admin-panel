@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Provider Profile</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
      <div class="content-header-right col-md-6 col-12 mb2">
        <div class="promo-actions d-flex justify-content-end">
          <button class="btn btn-primary mr-2 px-2">Edit </button>
        </div>
      </div>
    </div>
    <div class="content-body">
      <!-- CONTENT GOES HERE -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h1>Spoon Settings</h1>
              <a class="heading-elements-toggle">
                <i class="la la-ellipsis-v font-medium-3"></i>
              </a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li>
                    <a data-action="collapse">
                      <i class="ft-minus"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-content collapse show">
              <div class="card-body">
                <div class="row">
                  <div class="col-12 col-md-6">
                    <div class="settings-main-info d-flex">
                      <div class="settings-status-version w-100">
                        <div class="settings-status text-bold-700 h2">Current Status :
                          <span class="text-bold-400"></span>
                        </div>
                        <div class="settings-android text-bold-700 h2">Android Version :
                          <span class="text-bold-400"></span>
                        </div>
                        <div class="settings-ios text-bold-700 h2">iOS Version :
                          <span class="text-bold-400"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="settings-sec-info">
                      <div class="settings-slot text-bold-700 h5"> Slots Number :
                        <span class="text-bold-400"></span>
                      </div>
                      <div class="settings-min text-bold-700 h5"> Minutes Number :
                        <span class="text-bold-400"></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 col-md-6">
                    <div class="settings-wallet">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <ul>
                              <li class="settings-wallet-percentage text-bold-700 h5 no-bullets"> Wallet Percentage :
                                <span class="text-bold-400"></span>
                              </li>
                              <li class="settings-max-wallet-discount text-bold-700 h5"> Max. Wallet Discount :
                                <span class="text-bold-400"></span>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="settings-commission-info">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <ul>
                              <li class="settings-fixed-commission text-bold-700 h5 no-bullets"> Fixed Commission :
                                <span class="text-bold-400"></span>
                              </li>
                              <li class="settings-percentage-commission text-bold-700 h5"> Percentage Commission :
                                <span class="text-bold-400"></span>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 col-md-6">
                    <div class="settings-taxes-payments">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <ul>
                              <li class="settings-taxes text-bold-700 h5 no-bullets"> Taxes :
                                <span class="text-bold-400"></span>
                              </li>
                              <li class="settings-online-payment-fees text-bold-700 h5"> Online Payment Fees :
                                <span class="text-bold-400"></span>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="settings-referral">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <ul>
                              <li class="settings-referral-amount text-bold-700 h5 no-bullets"> Referral Amount :
                                <span class="text-bold-400"></span>
                              </li>
                              <li class="settings-referral-duration text-bold-700 h5"> Referral Duration :
                                <span class="text-bold-400"></span>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  let id = window.location.search.split("id=")[1];
  let status = $('.settings-status > span');
  let android = $('.settings-android > span')
  let ios = $('.settings-ios > span')
  let slot = $('.settings-slot > span')
  let minNumber = $('.settings-min > span')
  let walletPercentage = $('.settings-wallet-percentage > span')
  let walletDiscount = $('.settings-max-wallet-discount > span')
  let fixedCommission = $('.settings-fixed-commission > span')
  let percentageCommission = $('.settings-percentage-commission > span');
  let taxes = $('.settings-taxes > span');
  let onlinePaymentFees = $('.settings-online-payment-fees > span')
  let referralAmount = $('.settings-referral-amount > span');
  let referralDuration = $('.settings-referral-duration > span');

  $.ajax({
    type: 'GET',
    url: `http://spoon.api.myspoon.me/api/admin/v1/setting`,
    success: ({ data }) => {
      console.log(data)
      $(status).text(data[0].app_active === 1 ? 'Active' : 'Inactive')
      $(android).text(data[0].android_version)
      $(ios).text(data[0].ios_version)
      $(slot).text(data[0].num_slot)
      $(minNumber).text(data[0].num_minute)
      $(walletPercentage).text(data[0].wallet_percentage)
      $(walletDiscount).text(data[0].max_wallet_discount)
      $(fixedCommission).text(data[0].fixed_commission)
      $(percentageCommission).text(data[0].perecentage_commission)
      $(taxes).text(data[0].taxes)
      $(onlinePaymentFees).text(data[0].online_payment_fees)
      $(referralAmount).text(data[0].referral_amount)
      $(referralDuration).text(data[0].referral_duration)
    },
    error: err => console.log(err),
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    }
  })

  $(status).on('change', function () {
    $.ajax({
      type: 'PUT',
      url: `http://spoon.api.myspoon.me/api/admin/v1/promoCode/${id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: {
        is_active: $(status).val() === 'active' ? 1 : 0
      },
      success: data => console.log(data),
      error: err => console.log(err)
    })
  })

</script>
@endsection