@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <section id="validation">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Add Event</h4>
                <a class="heading-elements-toggle">
                  <i class="la la-ellipsis-h font-medium-3"></i>
                </a>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <form enctype="multipart/form-data" id="add-event-form">
                    <div class="row">
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label for="event-name">Name</label>
                          <input type="text" id="event-name" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="event-description">Description</label>
                          <textarea name="eventDescription" class="form-control" id="event-description" cols="30" rows="5"></textarea>
                        </div>
                        <div class="form-group">
                          <div class="row">
                            <div class="col-6">
                              <label for="event-from">From</label>
                              <input type="date" id="event-from" class="form-control" required>
                            </div>
                            <div class="col-6">
                              <label for="event-to">To</label>
                              <input type="date" id="event-to" class="form-control" required>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-12 col-md-6">
                            <div class="form-group">
                              <label for="event-default">Default Event</label>
                              <select name="eventDefault" id="event-default" class="form-control" required>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-12 col-md-6">
                            <div class="form-group">
                              <label for="event-status">Event Status</label>
                              <select name="eventStatus" id="event-status" class="form-control" required>
                                <option value="active">Active</option>
                                <option value="inactive">Inactive</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-md-6">
                        <div id="event-img"></div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary" id="add-menu">Add Category</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Form wizard with step validation section end -->

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split("id=")[1]

  $('#add-event-form').on('submit', function (e) {
    e.preventDefault()
    let name = $('#event-name').val();
    let description = $('#event-description').val()
    let from = $('#event-from').val()
    let to = $('#event-to').val()
    let eventDdefault = $('#event-default').val()
    let status = $('#event-status').val()
    let image = $('.dz-image > img').attr('src').split('base64,')[1]
    // console.log({ name }, { description }, { from }, { to }, { status }, { image }, { eventDdefault })

    $.ajax({
      type: 'POST',
      url: `http://spoon.api.myspoon.me/api/admin/v1/events`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: {
        name: name,
        description: description,
        from: from,
        to: to,
        active: status === "active" ? 1 : 0,
        deafult: eventDdefault === "yes" ? 1 : 0,
        icon: image

      },
      success: data => {
        // TODO: Put location.reload() as a call back on sweet alert!
        location.reload()
      },
      error: err => console.log(err)
    })

  });
</script>
@endsection