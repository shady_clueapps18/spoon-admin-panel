@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <section id="validation">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Edit Event</h4>
                <a class="heading-elements-toggle">
                  <i class="la la-ellipsis-h font-medium-3"></i>
                </a>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <form enctype="multipart/form-data" id="edit-event-form">
                    <div class="row">
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label for="event-name">Name</label>
                          <input type="text" id="event-name" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="event-description">description</label>
                          <textarea name="eventDescription" id="event-description" class="form-control" cols="30" rows="5" required></textarea>
                        </div>
                        <div class="form-group">
                          <div class="row">
                            <div class="col-6">
                              <label for="event-from">From</label>
                              <input type="text" id="event-from" class="form-control" required>
                            </div>
                            <div class="col-6">
                              <label for="event-to">To</label>
                              <input type="text" id="event-to" class="form-control" required>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-12 col-md-6">
                            <div class="form-group">
                              <label for="event-default">Default Event</label>
                              <select name="eventDefault" id="event-default" class="form-control" required>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-12 col-md-6">
                            <div class="form-group">
                              <label for="event-status">Event Status</label>
                              <select name="eventStatus" id="event-status" class="form-control" required>
                                <option value="active">Active</option>
                                <option value="inactive">Inactive</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-md-6">
                        <div id="edit-event-img" class="edit-img-wrapper">
                          <img src="" alt="">
                          <div class="edit-image-overlay">
                            <button class="btn btn-cyan edit-image">Change Image</button>
                            <button class="btn btn-danger delete-image">Delete Image</button>
                          </div>
                        </div>
                        <div id="edit-img-dropzone"></div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary" id="add-menu">Edit Event</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Form wizard with step validation section end -->

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split("id=")[1]
  const name = $('#event-name');
  const description = $('#event-description');
  const from = $('#event-from');
  const to = $('#event-to');
  const eventDefault = $('#event-default');
  const status = $('#event-status');


  $.ajax({
    type: 'GET',
    url: `http://spoon.api.myspoon.me/api/admin/v1/events/${id}`,
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      $(name).val(data.name);
      $(description).val(data.description);
      $(from).val(data.from).timeDropper({
        format: 'HH:mm'
      });
      $(to).val(data.to).timeDropper({
        format: 'HH:mm'
      });
      $(eventDefault).val(data.deafult === 1 ? 'yes' : 'no');
      $(status).val(data.active === 1 ? 'active' : 'inactive');
      $('#edit-event-img img').attr('src', data.icon)
    }
  })

  $('#edit-event-form').on('submit', function (e) {
    e.preventDefault()

    if ($('.dz-image').length) {
      const icon = $('.dz-image > img').attr('src').split('base64,')[1]
    }

    const data = {
      name: $(name).val(),
      description: $(description).val(),
      from: $(from).val().split(' ')[0] + ':00',
      to: $(to).val().split(' ')[0] + ':00',
      active: $(status).val() === 'active' ? 1 : 0,
      deafult: $(eventDefault).val() === 'yes' ? 1 : 2,
    }

    if ($('.dz-image').length) {
      data.icon = $('.dz-image > img').attr('src').split('base64,')[1];;
    }

    $.ajax({
      type: 'PUT',
      url: `http://spoon.api.myspoon.me/api/admin/v1/events/${id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: data,
      success: () => {
        swal(
          'Success!',
          'Event has been edited successfully!',
          'success')
      },
      error: err => console.log(err)
    })
  });
</script>
@endsection