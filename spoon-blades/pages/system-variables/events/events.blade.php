@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <table class="table table-striped table-bordered text-inputs-searching" id="events-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name (outside)</th>
            <th>Description (inside)</th>
            <th>Icon</th>
            <th>From</th>
            <th>To</th>
            <th>Default</th>
            <th>Status</th>
            <th>Created At</th>
            <th>Number of Providers</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>

    </div>
  </div>

  <!-- ADD PROVIDER MODAL -->
  <div class="modal fade" id="add-provider-modal" tabindex="-1" role="dialog" aria-labelledby="add-provider-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add Provider to the Event</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>
            <span class="text-danger">*</span>
            <span>The Providers Will Appear In the Same Order of Choice</span>
          </p>
          <select name="" id="all-providers" multiple style="width: 100%"></select>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary save-event-providers">Save changes</button>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split('')[1]
  let $id;
  const providers = [];
  let provider = {}
  const availableProvidersToEvents = []

  $("#events-table").DataTable(
    {
      processing: true,
      ajax: {
        type: 'GET',
        url: "http://spoon.api.myspoon.me/api/admin/v1/events?",
        dataSrc: "data",
        headers: {
          "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
        },
      },
      columns: [
        { data: "id" },
        { data: "name" },
        { data: "description" },
        {
          data: data => `<div style="max-width: 150px; max-height: 150px">
                            <img style="width: 100%; height: 100%" src="${data.icon}" />
                           </div>`
        },
        { data: "from" },
        { data: "to" },
        { data: data => data.deafult === 0 ? 'No' : 'Yes' },
        { data: data => `<a href='editEvent?id=${data.id}'>${data.status === 0 ? "Inactive" : "Active"}</a>` },
        { data: 'created_at' },
        {
          data: data => {
            return `<a href="" class="provider-count">${data.provider_count}</a>`
          }
        },
        {
          mRender: function (data, type, row, meta) {
            return `
                <button class="btn btn-sm btn-secondary event-provider-view" data-toggle="modal" data-target="#add-provider-modal">View Providers</button>
                <button class="btn btn-sm btn-primary event-provider-add" data-toggle="modal" data-target="#add-provider-modal">Add Provider</button>
                <a href="" class="btn btn-sm btn-info event-edit">Edit</a>
                <button class="btn btn-sm btn-danger event-delete">Delete</button>
              `;
          },
          className: "table-actions"
        }
      ]
    }
  );

  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/admin/v1/events',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      data.forEach(elm => {
        provider.id = elm.id;
        provider.provider = elm.provider
        providers.push(provider);
        provider = {}
      });
    },
    error: err => console.log(err)
  })

  // GET ALL PROVIDERS FORM the API
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/admin/v1/providers',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      data.forEach(i => availableProvidersToEvents.push({ id: i.id, name: i.name }))
      availableProvidersToEvents.forEach(prov => $('#all-providers').append(`
        <option value=${prov.id}>${prov.name}</option>
      `))
      $('#all-providers').select2({ placeholder: 'Add Provider to this event' });
    },
    error: err => console.log(err)
  });

  // Get the the event $id
  $(document).on('click', '.event-provider-add', function () {
    $id = $(this).parents('tr').find('td:first-of-type').text()
  })

  // Adding provider id to the list of providers  
  $(document).on('click', '.select2-selection__rendered', function () {
    $('.select2-results__option').each(function () {
      let idValue = $(this).attr('id').split('-').pop();
      $(this).attr('data-provider-id', idValue);
    });
  })

  // Passing the provider id to the actual selected providers to send them with AJAX
  $('#all-providers').on('select2:select', function (e) {
    $('#all-providers').trigger('change');
    $('.select2-selection__choice').each(function (i) {
      $(this).attr('data-provider-id', $('#all-providers').val()[i])
    })
  });

  // View Providers Related to event
  $(document).on('click', '.event-provider-view', function () {
    $id = $(this).parents('tr').find('td:first-of-type').text()
    $('.select2-selection__choice').each(function (i) {
      $(this).attr('data-provider-id', $('#all-providers').val()[i])
    })
    $.ajax({
      type: 'GET',
      url: `http://spoon.api.myspoon.me/api/admin/v1/providersByEventId/${$id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      success: ({ data }) => {
        console.log(data)
        const ids = data.map(i => i.id);
        data.forEach(i => $('#all-providers').val(ids))
        $('#all-providers').trigger('change')
        $('.select2-selection__choice').each(function (i) {
          $(this).attr('data-provider-id', $('#all-providers').val()[i])
        })
      },
      error: err => console.log(err)
    })
  });

  // Remove Provider from event
  $(document).on('click', '.select2-selection__choice__remove', function () {
    const $providerId = $(this).parent().attr('data-provider-id')
    console.log({ $id, $providerId })
    $('.select2-selection__choice').each(function (i) {
      $(this).attr('data-provider-id', $('#all-providers').val()[i])
    })
    $.ajax({
      type: 'DELETE',
      url: `http://spoon.api.myspoon.me/api/admin/v1/deleteProviderFromEventId`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: {
        event_id: parseInt($id),
        provider_id: parseInt($providerId)
      },
      success: () => swal(
        'Success!',
        `Provider has been successfully removed from this event!`,
        'success'
      ),
      error: err => console.log(err)
    });
  });

  $(document).on('click', '.save-event-providers', function () {
    // Build 'data' array which contains details about provider id and order
    const selectedProviders = [];
    $('.select2-selection__choice').each(function (i) {
      selectedProviders.push({ provider_id: parseInt($(this).attr('data-provider-id')), order: i + 1 })
    });

    // Close the Modal
    $('button[data-dismiss="modal"]').trigger('click');

    // Request to add the provider to the event
    $.ajax({
      type: 'POST',
      url: `http://spoon.api.myspoon.me/api/admin/v1/addProvidersToEventId`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: {
        id: $id,
        data: selectedProviders
      },
      success: data => {
        swal(
          'Success!',
          'Event has been edited successfully!',
          'success'),
          $('#all-providers').select2().val(null).trigger("change");
      },
      error: err => console.log(err)
    })
  })

  $(document).on('click', '.provider-count', function (e) {
    e.preventDefault();
    const $providerId = $(this).parents('tr').find('td:first-of-type').text()
    const providerIds = [];
    const selectedProvider = providers.find(provider => provider.id == $providerId).provider
    selectedProvider.forEach(provider => providerIds.push(provider.name))
    const redirectLink = `providersOverView?providers=${providerIds.toString()}`
    window.location = redirectLink
  });

  $(document).on('click', '.event-delete', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    console.log($id)
    $.ajax({
      type: 'DELETE',
      url: `https://spoon.api.myspoon.me/api/admin/v1/events/${$id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: {
        id: $id
      },
      success: data => {
        $(this).parents('tr').remove()
        swal(
          'Success!',
          'Event has been edited successfully!',
          'success')
      },
      error: err => console.log(err)
    });
  });

  $(document).on('click', '.event-edit', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    $(this).attr('href', `editEvent?id=${$id}`)
  })
</script>
@endsection