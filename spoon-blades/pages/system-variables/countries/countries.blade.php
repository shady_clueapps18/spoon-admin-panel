@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <table class="table table-striped table-bordered text-inputs-searching" id="countries-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Flag</th>
            <th>Country Code</th>
            <th>Mobile Numbers</th>
            <th>Order</th>
            <th>Status</th>
            <th>Number of Cities</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split('id=')[1];
  const cities = [];
  let city = {};

  $("#countries-table").DataTable(
    {
      processing: true,
      ajax: {
        type: 'GET',
        url: "http://spoon.api.myspoon.me/api/admin/v1/countries?",
        dataSrc: "data",
        headers: {
          "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
        },
      },
      columns: [
        { data: "id" },
        { data: "name" },
        {
          data: data => `<div style="max-width: 150px; max-height: 150px">
                            <img style="width: 100%; height: 100%" src="${data.flag}" />
                           </div>`
        },
        { data: "country_code" },
        { data: "mobile_number" },
        // TODO: Fill the right data
        { data: "order" },
        { data: data => data.active === 1 ? 'Active' : 'Inactive' },
        { data: data => `<a href="" class="city-count">${data.city_count}</a>` },

        {
          mRender: function (data, type, row, meta) {
            return `
                <a href="" class="btn btn-sm btn-info btn-edit edit-country">Edit</a>
                <button class="btn btn-sm btn-danger delete-country">Delete</button>
              `;
          },
          className: "table-actions"
        }
      ]
    }
  );

  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/admin/v1/countries',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      data.forEach(elm => {
        city.id = elm.id;
        city.city = elm.city
        cities.push(city);
        city = {}
      });
    },
    error: err => console.log(err)
  })

  $(document).on('click', '.city-count', function (e) {
    e.preventDefault();
    const $countryId = $(this).parents('tr').find('td:first-of-type').text()
    const cityIds = [];
    const selectedcities = cities.find(city => city.id == $countryId).city
    selectedcities.forEach(city => cityIds.push(city.name))
    const redirectLink = `cities?cities=${cityIds.toString()}`
    window.location = redirectLink
  });

  $(document).on('click', '.delete-country', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    console.log($id)
    $.ajax({
      type: 'DELETE',
      url: `https://spoon.api.myspoon.me/api/admin/v1/countries/${$id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: {
        id: $id
      },
      success: data => {
        $(this).parents('tr').remove()
        //TODO: Add Sweet Alert
      },
      error: err => console.log(err)
    });
  });

  $(document).on('click', '.edit-country', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    $(this).attr('href', `editCountry?id=${$id}`)
  });

</script>
@endsection