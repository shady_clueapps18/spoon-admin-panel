@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <section id="validation">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Add Country</h4>
                <a class="heading-elements-toggle">
                  <i class="la la-ellipsis-h font-medium-3"></i>
                </a>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <form enctype="multipart/form-data" id="add-country-form">
                    <div class="row">
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label for="country-name">Name</label>
                          <input type="text" id="country-name" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="country-order">Order</label>
                          <input type="number" id="country-order" class="form-control" required>
                        </div>
                        <div class="row">
                          <div class="col-6">
                            <div class="form-group">
                              <label for="country-default">Default</label>
                              <select name="countryDefault" id="country-default" class="form-control" required>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="form-group">
                              <label for="country-active">Active</label>
                              <select name="countryActive" id="country-active" class="form-control" required>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                              </select>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-6">
                            <div class="form-group">
                              <label for="country-code">Country Code</label>
                              <input type="text" class="form-control" id="country-code" required>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="form-group">
                              <label for="mobile-number">Mobile Number</label>
                              <input type="number" class="form-control" id="mobile-number" required>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-md-6">
                        <div id="country-flag"></div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary" id="add-category">Add Country</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Form wizard with step validation section end -->

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split("id=")[1]


  $('#add-country-form').on('submit', function (e) {
    const countryName = $('#country-name').val();
    const order = $('#country-order').val();
    const countryDefault = $('#country-default').val() === 'yes' ? 1 : 0;
    const active = $('#country-active').val() === 'yes' ? 1 : 0;
    const countryCode = $('#country-code').val();
    const mobileNumber = $('#mobile-number').val();
    const countryImg = $('.dz-image > img').attr('src').split('base64,')[1]
    e.preventDefault();
    // console.log({ countryName, cityName, name, order, active })
    $.ajax({
      type: 'POST',
      url: 'http://spoon.api.myspoon.me/api/admin/v1/countries',
      headers: {
        Authorization:
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
      },
      data: {
        name: countryName,
        order,
        active,
        flag: countryImg,
        mobile_number: mobileNumber,
        country_code: countryCode
      },
      success: data => swal(
        'Success!',
        'Country has been added successfully!',
        'success'
      ),
      error: err => console.log(err)
    });
  });
</script>
@endsection