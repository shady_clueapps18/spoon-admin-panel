@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <table class="table table-striped table-bordered text-inputs-searching" id="services-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Description</th>
            <th>Duration</th>
            <th>Light Price</th>
            <th>Platinum Price</th>
            <th>Silver Price</th>
            <th>Gold Price</th>
            <th>Create At</th>
            <th>Updated At</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split('id=')[1];
  const cities = [];
  let city = {};

  $("#services-table").DataTable(
    {
      processing: true,
      ajax: {
        type: 'GET',
        url: "http://spoon.api.myspoon.me/api/admin/v1/service?",
        dataSrc: "data",
        headers: {
          "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
        },
      },
      columns: [
        { data: "id" },
        { data: "name" },
        { data: "description" },
        { data: "duration" },
        { data: "light_price" },
        { data: "platinum_price" },
        { data: "silver_price" },
        { data: "gold_price" },
        { data: "created_at" },
        { data: "updated_at" },

        {
          mRender: function (data, type, row, meta) {
            return `
                <a href="" class="btn btn-sm btn-info btn-edit edit-service">Edit</a>
                <button class="btn btn-sm btn-danger delete-service">Delete</button>
              `;
          },
          className: "table-actions"
        }
      ]
    }
  );

  $(document).on('click', '.delete-service', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    console.log($id)
    $.ajax({
      type: 'DELETE',
      url: `https://spoon.api.myspoon.me/api/admin/v1/service/${$id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: {
        id: $id
      },
      success: data => {
        swal(
          'Success!',
          'Service has been removed successfully!',
          'success'
        )
      },
      error: err => console.log(err)
    });
  });

  $(document).on('click', '.edit-service', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    $(this).attr('href', `./editService.html?id=${$id}`)
  });

</script>
@endsection