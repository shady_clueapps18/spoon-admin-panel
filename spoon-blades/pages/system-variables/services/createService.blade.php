@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <section id="validation">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Add Service</h4>
                <a class="heading-elements-toggle">
                  <i class="la la-ellipsis-h font-medium-3"></i>
                </a>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <form enctype="multipart/form-data" id="add-service-form">
                    <div class="row">
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label for="services-name">Name</label>
                          <input type="text" id="services-name" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="services-description">Description</label>
                          <textarea name="" id="services-description" cols="30" rows="6" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                          <label for="services-duration">Duration</label>
                          <input type="text" id="services-duration" class="form-control">
                        </div>
                      </div>
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label for="services-lite">Services Lite Prices</label>
                          <input type="number" name="services-lite" id="services-lite" class="form-control">
                        </div>
                        <div class="form-group">
                          <label for="services-silver">Services Silver Prices</label>
                          <input type="number" name="services-silver" id="services-silver" class="form-control">
                        </div>
                        <div class="form-group">
                          <label for="services-gold">Services Gold Prices</label>
                          <input type="number" name="services-gold" id="services-gold" class="form-control">
                        </div>
                        <div class="form-group">
                          <label for="services-platinum">Services Platinum Prices</label>
                          <input type="number" name="services-platinum" id="services-platinum" class="form-control">
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary" id="add-promo">Add Promo</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Form wizard with step validation section end -->

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split("id=")[1]

  $('#add-service-form').on('submit', function (e) {
    e.preventDefault();
    const name = $('#services-name').val();
    const description = $('#services-description').val()
    const duration = $('#services-duration').val()
    const lite = $('#services-lite').val()
    const silver = $('#services-silver').val()
    const gold = $('#services-gold').val()
    const platinum = $('#services-platinum').val()

    $.ajax({
      type: 'POST',
      url: 'http://spoon.api.myspoon.me/api/admin/v1/service',
      headers: {
        Authorization:
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
      },
      data: {
        name,
        description,
        duration,
        platinum_price: platinum,
        silver_price: silver,
        gold_price: gold

        // TODO: Add Lite Price
      },
      success: () => swal(
        'Success!',
        `PromoCode has been successfully added!`,
        'success'
      ),
      error: err => console.log(err)
    });
  });
</script>
@endsection