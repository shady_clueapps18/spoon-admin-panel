@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <section id="validation">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Edit Category</h4>
                <a class="heading-elements-toggle">
                  <i class="la la-ellipsis-h font-medium-3"></i>
                </a>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <form enctype="multipart/form-data" id="add-category-form">
                    <div class="row">
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label for="category-name">Name</label>
                          <input type="text" id="category-name" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="category-order">Order</label>
                          <input type="number" id="category-order" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-12 col-md-6 d-flex justify-content-end">
                        <div id="edit-category-img" class="edit-img-wrapper">
                          <img src="" alt="">
                          <div class="edit-image-overlay">
                            <button class="btn btn-cyan edit-image">Change Image</button>
                            <button class="btn btn-danger delete-image">Delete Image</button>
                          </div>
                        </div>
                        <div id="edit-img-dropzone"></div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary" id="add-category">Add Category</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Form wizard with step validation section end -->

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split("id=")[1]
  const categoryImg = $('#category-img')

  $.ajax({
    type: 'GET',
    url: `https://spoon.api.myspoon.me/api/admin/v1/categories/${id}`,
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      $('#category-name').val(data.name);
      $('#category-order').val(data.order);
      $('#edit-category-img img').attr('src', data.image)
    }
  })

  $('#add-category-form').on('submit', function (e) {
    e.preventDefault();
    const name = $('#category-name').val();
    const order = $('#category-order').val();
    if ($('.dz-img').length) {
      const image = $('.dz-image > img').attr('src').split('base64,')[1]
    }
    const data = {
      name,
      order
    }
    if ($('.dz-img').length) {
      data.image = image;
    }

    $.ajax({
      type: 'PUT',
      url: `http://spoon.api.myspoon.me/api/admin/v1/categories/${id}`,
      headers: {
        Authorization:
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
      },
      data: data,
      success: data => {
        console.log(data)
        swal(
          'Success!',
          'The category has been edited successfully',
          'success'
        )
      },
      error: err => console.log(err)
    });
  });
</script>
@endsection