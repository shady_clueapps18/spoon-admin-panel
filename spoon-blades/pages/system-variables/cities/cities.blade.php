@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <table class="table table-striped table-bordered text-inputs-searching" id="cities-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Country Name</th>
            <th>Name</th>
            <th>Order</th>
            <th>Status</th>
            <th>Number of Areas</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split('id=')[1];
  const areas = [];
  let area = {};

  /**
   * TODO: Fill the right Data in the table
   */
  const citiesTable = $("#cities-table").DataTable(
    {
      processing: true,
      ajax: {
        type: 'GET',
        url: "http://spoon.api.myspoon.me/api/admin/v1/cities?",
        dataSrc: "data",
        headers: {
          "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
        },
      },
      columns: [
        { data: "id" },
        { data: "country_name" },
        { data: "name" },
        { data: "order" },
        { data: data => data.active === 1 ? "Active" : "Inactive" },
        { data: data => `<a href="" class="area-count">${data.area_count}</a>` },
        {
          mRender: function (data, type, row, meta) {
            return `
                <a href="" class="btn btn-sm btn-info city-edit">Edit</a>
                <button class="btn btn-sm btn-danger city-delete">Delete</button>
              `;
          },
          className: "table-actions"
        }
      ]
    }
  );

  if (document.URL.includes('cities=')) {
    const searchKey = decodeURI(window.location.search.split('cities=')[1].replace(/\,/g, '|'))
    citiesTable.column(2).search(searchKey, true, false).draw()
  }

  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/admin/v1/cities',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      data.forEach(elm => {
        area.id = elm.id;
        area.area = elm.areas
        areas.push(area);
        area = {}
      });
    },
    error: err => console.log(err)
  })

  $(document).on('click', '.area-count', function (e) {
    e.preventDefault();
    const $cityId = $(this).parents('tr').find('td:first-of-type').text()
    const areaIds = [];
    const selectedareas = areas.find(area => area.id == $cityId).area
    selectedareas.forEach(area => areaIds.push(area.name))
    const redirectLink = `areas?areas=${areaIds.toString()}`
    window.location = redirectLink
  });


  $(document).on('click', '.city-delete', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    console.log($id)
    $.ajax({
      type: 'DELETE',
      url: `https://spoon.api.myspoon.me/api/admin/v1/cities/${$id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: {
        id: $id
      },
      success: ({ data }) => {
        $(this).parents('tr').remove()
        // Sweet Alert
        swal(
          'Success!',
          `You have deleted ${data.name}`,
          'success'
        )
      },
      error: err => console.log(err)
    });
  });

  $(document).on('click', '.city-edit', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    $(this).attr('href', `editCity?id=${$id}`)
  })

</script>
@endsection