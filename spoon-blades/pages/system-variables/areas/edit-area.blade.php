@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <section id="validation">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Add Area</h4>
                <a class="heading-elements-toggle">
                  <i class="la la-ellipsis-h font-medium-3"></i>
                </a>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <form enctype="multipart/form-data" id="edit-area-form">
                    <div class="row">
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label for="area-country">Country Name</label>
                          <select name="areaCountry" id="area-country" style="width:100%"></select>
                        </div>
                        <div class="form-group">
                          <label for="area-city">City Name</label>
                          <select name="areaCity" id="area-city" style="width:100%"></select>
                        </div>
                        <div class="form-group">
                          <label for="area-name">Name</label>
                          <input type="text" id="area-name" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label for="area-order">Order</label>
                          <input type="number" id="area-order" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="area-default">Default</label>
                          <select name="areaDefault" id="area-default" class="form-control" required>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary" id="add-category">Update Area</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Form wizard with step validation section end -->

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split("id=")[1]

  let countries = [];
  let cities = [];

  // Get All Countries
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/v1/countries',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      data.forEach(i => countries.push({ id: i.id, name: i.name }))
      countries.forEach(country => $('#area-country').append(`<option value=${country.id}>${country.name}</option>`))
      $('#area-country').select2();
    }
  });

  // Get All Cities with country Id
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/v1/cities?countryId=1',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      data.forEach(i => cities.push({ id: i.id, name: i.name }))
      cities.forEach(city => $('#area-city').append(`<option value=${city.id}>${city.name}</option>`))
      $('#area-city').select2();
    }
  });

  // Change cities on Country Change
  $('#provider-countries').on('change', function () {
    cities = [];
    $.ajax({
      type: 'GET',
      url: `http://spoon.api.myspoon.me/api/v1/cities?countryId=${$(this).val()}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      success: ({ data }) => {
        $('#area-city').find('option').each(function () {
          $(this).remove();
        });
        data.forEach(i => cities.push({ id: i.id, name: i.name }))
        console.log(cities)
        cities.forEach(city => $('#area-city').append(`<option value=${city.id}>${city.name}</option>`))
      }
    })
  })

  //Get the Data of the area 
  $.ajax({
    type: 'GET',
    url: `http://spoon.api.myspoon.me/api/admin/v1/areas/${id}`,
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    },
    success: ({ data }) => {
      $('#area-name').val(data.name);
      $('#area-order').val(data.order);
      $('#area-default').val(data.active === 1 ? 'yes' : 'no');
    },
    error: err => console.log(err)
  })

  $('#edit-area-form').on('submit', function (e) {
    const countryName = $('#area-country').val();
    const cityName = $('#area-city').val();
    const name = $('#area-name').val();
    const order = $('#area-order').val();
    const active = $('#area-default').val() === 'yes' ? 1 : 0;
    e.preventDefault();
    console.log({ countryName, cityName, name, order, active })
    $.ajax({
      type: 'PUT',
      url: `http://spoon.api.myspoon.me/api/admin/v1/areas/${id}`,
      headers: {
        Authorization:
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
      },
      data: {
        city_id: cityName,
        name,
        order,
        active
      },
      // TODO: Add Sweet Alert to notify the user that the form is submitted
      success: swal(
        'Success!',
        'Area Has been Updated',
        'success'
      ),
      error: err => console.log(err)
    });
  });
</script>
@endsection