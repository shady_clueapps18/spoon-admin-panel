@extends('layout.layout')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">User Wallets</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">Home</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- CONTENT GOES HERE -->
            <a href="" id="add-wallet" class="btn btn-purple table-add-more">Add Wallet </a>

            <div class="wallet-credit">
                <p>whole Total Credit:
                    <span id="whole-credit"></span>EGP
                </p>
                <p>current total credit:
                    <span id="current-credit"></span>EGP
                </p>
            </div>
            <table class="table table-striped table-bordered text-inputs-searching" id="user-wallets-table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Amount</th>
                        <th>Type</th>
                        <th>Referral code</th>
                        <th>promo code</th>
                        <th>Comments</th>
                        <th>Wallet usage</th>

                        <th>Created at</th>
                        <th>Expired at</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>

                <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Amount</th>
                        <th>Type</th>
                        <th>Referral code</th>
                        <th>promo code</th>
                        <th>Comments</th>
                        <th>Wallet usage</th>
                        <th>Created at</th>
                        <th>Expired at</th>
                    </tr>
                </tfoot>

            </table>

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    function parseURLParams(url) {
        var queryStart = url.indexOf("?") + 1,
            queryEnd = url.indexOf("#") + 1 || url.length + 1,
            query = url.slice(queryStart, queryEnd - 1),
            pairs = query.replace(/\+/g, " ").split("&"),
            parms = {}, i, n, v, nv;

        if (query === url || query === "") return;

        for (i = 0; i < pairs.length; i++) {
            nv = pairs[i].split("=", 2);
            n = decodeURIComponent(nv[0]);
            v = decodeURIComponent(nv[1]);

            if (!parms.hasOwnProperty(n)) parms[n] = [];
            parms[n].push(nv.length === 2 ? v : null);
        }
        return parms;
    }
    var urlString = window.location.href;
    urlParams = parseURLParams(urlString);
    userId = urlParams.id[0];

    console.log(urlString, userId);

    $(document).on('click', '#add-wallet', function (e) {
        e.preventDefault();
        console.log(userId, "id");
        window.location = 'add-user-wallet.html?id=' + userId;
    });

    var getWalletById = function (userId) {
        var userWallets;

        $.ajax({
            method: "POST",
            url: "http://spoon.api.myspoon.me/api/admin/v1/userById",
            data: {
                id: userId,
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
            }
        }).done(function (msg) {
            console.log(msg);
            userWallets = msg.user[0].wallets;
            $("#current-credit").html(msg.user[0].wallet_credit);
            $("#whole-credit").html(msg.user[0].total_credit);
            console.log(userWallets);


            $(".text-inputs-searching")
                .DataTable()
                .destroy();

            // Setup - add a text input to each footer cell
            $(".table tfoot th").each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');
            });
            console.log(userWallets);

            // DataTable
            var tableCarSearching = $("#user-wallets-table").DataTable(
                {
                    processing: true,
                    data: userWallets,
                    columns: [
                        {
                            data: function (data, type, row, meta) {
                                return `<p>${data.id}</p>`
                            }
                        },
                        {
                            data: function (data, type, row, meta) {
                                return `<p>${data.credit} EGP</p>`
                            }
                        },
                        { data: "type" },
                        {
                            data: function (data, type, row, meta) {
                                return `<a href="#" id="referralCode">${data.referral_id}</a>` //no user id for this user
                            }
                        },
                        {
                            data: function (data, type, row, meta) {
                                return `<a href="#" id="promoCode">${data.promocode_id}</a>` //no user id for this user
                            }
                        },
                        { data: "comments" },
                        {
                            data: function (data, type, row, meta) {
                                return `<a href="#" id="walletOrders">${data.use_age_count}</a>` //no user id for this user
                            }
                        },
                        { data: "created_at" },
                        { data: "expired_at" },
                        {
                            mRender: function (data, type, row) {
                                return `
                                <button class="btn btn-sm btn-info" id="edit-action">Edit</button>
                                `;
                            },
                            className: "table-actions"
                        }
                    ]
                }
            );

        });
    }

    $(document).on('click', '#edit-action', function (e) {
        e.preventDefault();
        var walletId = $(this).parent().prev().prev().prev().prev().prev().prev().prev().prev().find("p").text();
        console.log("from click", walletId);
        // window.location.assign("user-wallet.html");
        window.location = 'edit-user-wallet.html?id=' + walletId;

    })
    getWalletById(userId);
</script>
@endsection