@extends('layout.layout')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">All Users</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">Home</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <a href="../../spoon/user/create-user.html" class="btn btn-purple table-add-more">Add User</a>

            <!-- CONTENT GOES HERE -->
            <table class="table table-striped table-bordered text-inputs-searching" id="user-table">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>Orders no.</th>
                        <th>Wallet Credit</th>
                        <th>Created At</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>

                <tfoot>
                    <tr>
                        <th>id</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>Orders no.</th>
                        <th>Wallet Credit</th>
                        <th>Created At</th>
                    </tr>
                </tfoot>

            </table>

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#user-table").DataTable(
        {
            processing: true,
            ajax: {
                type: 'GET',
                url: "http://spoon.api.myspoon.me/api/admin/v1/users",
                dataSrc: "data",
                headers: {
                    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
                },
            },
            columns: [
                {
                    data: function (data, type, row, meta) {
                        return `<p id="userId">${data.id}</a>`
                    }
                },
                {
                    data: function (data, type, row, meta) {
                        return `<a href="#" id="userDetails">${data.name}</a>`
                    }
                },
                {
                    data: function (data, type, row, meta) {
                        return data.country_code + data.mobile;
                    }
                },
                { data: "email" },
                {
                    data: function (data, type, row, meta) {
                        return `<a href="user-orders.html" id="userOrders">${data.orders_num}</a>`
                    }
                },
                {
                    data: function (data, type, row, meta) {
                        return `<a href="user-wallet.html" id="userWallets">${data.wallet_credit}</a>`
                    }
                },
                { data: "created_at" },
                {
                    mRender: function (data, type, row) {
                        return `
              <button class="btn btn-sm btn-cyan" id="view-action">View</button>
              <button class="btn btn-sm btn-info" id="edit-action">Edit</button>
            `;
                    },
                    className: "table-actions"
                }
            ],
        },

    );
    $(document).on('click', '#userDetails', function (e) {
        e.preventDefault();
        var id = $(this).parent().prev().find("p").text();
        console.log("from click", id);

        $.ajax({
            method: "GET",
            url: "http://spoon.api.myspoon.me/api/admin/v1/users/?id=" + id,
            type: 'GET',
            data: {
                token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
                // token: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
            }
        }).done(function (msg) {
            console.log(msg, msg.data[0].cars);
            $("#id").html(msg.data[0].id);
            // window.location.assign("user-profile.html");
            window.location = 'user-profile.html?id=' + id;
        });
    })

    $(document).on('click', '#view-action', function (e) {
        e.preventDefault();
        window.location = 'user-profile.html?id=' + id;
    });

    $(document).on('click', '#edit-action', function (e) {
        e.preventDefault();
        var id = $(this).parent().prev().prev().prev().prev().prev().prev().prev().find("p").text();
        window.location = 'edit-user.html?id=' + id;
    });



    $(document).on('click', '#userWallets', function (e) {
        e.preventDefault();
        var id = $(this).parent().prev().prev().prev().prev().prev().find("p").text();
        console.log("from click", id);
        // window.location.assign("user-wallet.html");
        window.location = 'user-wallet.html?id=' + id;

    })

    $(document).on('click', '#userOrders', function (e) {
        e.preventDefault();
        var id = $('#userId').text();
        console.log("from click", id);
        $.ajax({
            method: "GET",
            url: "http://spoon.api.myspoon.me/api/admin/v1/userById/" + id,
            type: 'GET',
            data: {
                token: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
                // token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
            }
        }).done(function (msg) {
            console.log(msg, 'orders', msg.user[0].order);
            $("#id").html(msg.user[0].id);
            // window.location.assign("user-orders.html");

        });
    })
// $(document).on('click', '#userDetails', function (e) {
//   e.preventDefault();
//   alert("Handler for .click() called.");
// });



// Apply the search
// tableSearching.columns().every(function () {
//   var that = this;

//   $("input", this.footer()).on("keyup change", function () {
//     if (that.search() !== this.value) {
//       that.search(this.value).draw();
//     }
//   });
// });

//   var getAllUsers = function(){
//   $.ajax({
//     method: "GET",
//     url: "http://spoon.api.myspoon.me/api/admin/v1/users?",
//     data: {
//       token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
//     }
//   }).done(function( msg ) {
//       console.log(msg);
//     });

// }
// getAllUsers();

</script>
@endsection