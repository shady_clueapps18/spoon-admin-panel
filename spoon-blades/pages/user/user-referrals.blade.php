@extends('layout.layout')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">User Referrals</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">Home</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- CONTENT GOES HERE -->
            <table class="table table-striped table-bordered text-inputs-searching" id="user-referral-table">
                <thead>
                    <tr>
                        <th>Amount</th>
                        <th>User Name</th>
                        <th>Created At</th>
                        <th>Expired at</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>

                <tfoot>
                    <tr>
                        <th>Amount</th>
                        <th>User Name</th>
                        <th>Created At</th>
                        <th>Expired at</th>
                    </tr>
                </tfoot>

            </table>

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    function parseURLParams(url) {
        var queryStart = url.indexOf("?") + 1,
            queryEnd = url.indexOf("#") + 1 || url.length + 1,
            query = url.slice(queryStart, queryEnd - 1),
            pairs = query.replace(/\+/g, " ").split("&"),
            parms = {}, i, n, v, nv;

        if (query === url || query === "") return;

        for (i = 0; i < pairs.length; i++) {
            nv = pairs[i].split("=", 2);
            n = decodeURIComponent(nv[0]);
            v = decodeURIComponent(nv[1]);

            if (!parms.hasOwnProperty(n)) parms[n] = [];
            parms[n].push(nv.length === 2 ? v : null);
        }
        return parms;
    }
    var urlString = window.location.href;
    urlParams = parseURLParams(urlString);
    userId = urlParams.id[0];

    console.log(urlString, urlParams.id[0]);

    var userReferrals;

    $.ajax({
        method: "POST",
        url: "http://spoon.api.myspoon.me/api/admin/v1/userById",
        data: {
            id: userId,
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
        }
    }).done(function (msg) {
        console.log(msg);
        userReferrals = msg.user[0].referral;
        console.log(userReferrals);


        $(".text-inputs-searching")
            .DataTable()
            .destroy();

        // Setup - add a text input to each footer cell
        $(".table tfoot th").each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');
        });

        // DataTable
        var tableCarSearching = $("#user-referral-table").DataTable(
            {
                processing: true,
                data: userReferrals,
                columns: [
                    {
                        data: function (data, type, row, meta) {
                            return `<p id="Id">${data.id}</p>` //no user id for this user
                        }
                    },
                    {

                        data: function (data, type, row, meta) {
                            return `<a href="" id="referralUserName">${data.name}</a>` //no user id for this user
                        }
                    },
                    { data: "created_at" },
                    { data: "expired_at" }
                ]
            }
        );
    });
    $(document).on('click', '#referralUserName', function (e) {
        e.preventDefault();
        var referralUserId = $(this).parent().prev().find("p").text();
        console.log(referralUserId, "id");
        window.location = 'user-profile.html?id=' + referralUserId;
    });



</script>
@endsection