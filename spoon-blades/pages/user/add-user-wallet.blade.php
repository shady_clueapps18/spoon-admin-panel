@extends('layout.layout')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">Add Wallet</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">Home</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">

            <!-- CONTENT GOES HERE -->
            <form class="form form-horizontal" id="create-user-wallet" novalidate>
                <div class="form-body">
                    <h4 class="form-section">
                        <i class="ft-user"></i> Wallet Info</h4>
                    <div class="form-group row">
                        <label class="col-md-2 label-control" for="amount" required>Amount
                        </label>
                        <div class="controls col-md-9">
                            <input type="number" id="amount" class="form-control" placeholder="amount" name="amount" required>
                        </div>
                    </div>
                    <!-- <div class="form-group row">
                        <label class="col-md-2 label-control" for="type">type</label>
                        <div class="controls col-md-9">
                            <input type="text" id="email" class="form-control" placeholder="type" name="type">
                        </div>
                    </div> -->
                    <div class="form-group row">
                        <label class="col-md-2 label-control" for="comments">comments</label>
                        <div class="controls col-md-9">
                            <input type="text" id="comments" class="form-control" placeholder="write your comments" name="comments">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 label-control" for="expired_at">Expired at</label>
                        <div class="controls col-md-9">
                            <input type="date" id="expired_at" class="form-control" placeholder="Expired at" name="expired_at">
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" class="btn btn-warning mr-1">
                        <i class="ft-x"></i> Cancel
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <i class="la la-check-square-o"></i> Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>

    function parseURLParams(url) {
        var queryStart = url.indexOf("?") + 1,
            queryEnd = url.indexOf("#") + 1 || url.length + 1,
            query = url.slice(queryStart, queryEnd - 1),
            pairs = query.replace(/\+/g, " ").split("&"),
            parms = {}, i, n, v, nv;

        if (query === url || query === "") return;

        for (i = 0; i < pairs.length; i++) {
            nv = pairs[i].split("=", 2);
            n = decodeURIComponent(nv[0]);
            v = decodeURIComponent(nv[1]);

            if (!parms.hasOwnProperty(n)) parms[n] = [];
            parms[n].push(nv.length === 2 ? v : null);
        }
        return parms;
    }
    var urlString = window.location.href;
    urlParams = parseURLParams(urlString);
    userId = urlParams.id[0];

    console.log(urlString, urlParams.id[0]);



    $('#create-user-wallet').on('submit', function (e) {
        e.preventDefault();
        var wallet = {};
        wallet.amount = $('#amount').val();
        // user.type = $('#type').val();
        wallet.comments = $('#comments').val();
        wallet.expired_at = $('#expired_at').val();
        console.log(wallet, userId)
        $.ajax({
            method: "POST",
            url: "http://spoon.api.myspoon.me/api/admin/v1/wallets",
            data: {
                expired_at: wallet.expired_at,
                credit: wallet.amount,
                comments: wallet.comments,
                user_id: userId,
                token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
            }
        })
            .done(function (msg) {
                console.log(msg);
                window.location = 'user-wallet.html?id=' + userId;
                console.log(wallet);
            });
    });


</script>
@endsection