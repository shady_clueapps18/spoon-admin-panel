@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <table class="table table-striped table-bordered text-inputs-searching" id="messages-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>User Name</th>
            <th>User Mobile</th>
            <th>Content</th>
            <th>Created At</th>
            <th>Solved</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split("id=")[1];
  const userMessagesTable = $("#messages-table").DataTable(
    {
      processing: true,
      ajax: {
        type: 'GET',
        url: "http://spoon.api.myspoon.me/api/admin/v1/contactUs?",
        dataSrc: "data",
        headers: {
          "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
        },
      },
      columns: [
        { data: "id" },
        { data: data => `<a href='userProfile?id=${data.user_id}'>${data.name}</a>` },
        { data: "phone" },
        { data: 'message' },
        { data: "created_at" },
        { data: data => data.is_solved === 1 ? `<input class="solved-messages" type="checkbox" checked disabled>` : `<input class="solved-messages" type="checkbox">` },
        {
          mRender: function (data, type, row, meta) {
            return `
                <button class="btn btn-sm btn-danger messages-delete">Delete</button>
              `;
          },
          className: "table-actions"
        }
      ]
    }
  );

  $(document).on('click', '.messages-delete', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    console.log($id)
    $.ajax({
      type: 'DELETE',
      url: `https://spoon.api.myspoon.me/api/admin/v1/contactUs/${$id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: {
        id: $id
      },
      success: ({ data }) => {
        $(this).parents('tr').remove()
        // Sweet Alert
        swal(
          'Success!',
          `Provider has been successfully removed!`,
          'success'
        )
      },
      error: err => console.log(err)
    });
  });

  $(document).on('change', '.solved-messages', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text()
    $.ajax({
      type: 'PUT',
      url: `https://spoon.api.myspoon.me/api/admin/v1/contactUs/${$id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: {
        is_solved: 1
      },
      success: () => $(this).prop('disabled', true),
      error: err => console.log(err)
    })
  });
</script>
@endsection