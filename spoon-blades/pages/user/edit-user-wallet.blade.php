@extends('layout.layout')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">Edit Wallet</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">Home</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">

            <!-- CONTENT GOES HERE -->
            <form class="form form-horizontal" id="edit-user-wallet" novalidate>
                <div class="form-body">
                    <h4 class="form-section">
                        <i class="ft-user"></i> Wallet Info</h4>
                    <div class="form-group row">
                        <label class="col-md-2 label-control" for="credit" required>credit
                        </label>
                        <div class="controls col-md-9">
                            <input type="number" id="credit" class="form-control" placeholder="credit" name="credit" required>
                        </div>
                    </div>
                    <!-- <div class="form-group row">
                        <label class="col-md-2 label-control" for="type">type</label>
                        <div class="controls col-md-9">
                            <input type="text" id="email" class="form-control" placeholder="type" name="type">
                        </div>
                    </div> -->
                    <div class="form-group row">
                        <label class="col-md-2 label-control" for="comments">comments</label>
                        <div class="controls col-md-9">
                            <input type="text" id="comments" class="form-control" placeholder="write your comments" name="comments" disabled="true">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 label-control" for="expired_at">Expired at</label>
                        <div class="controls col-md-9">
                            <input type="date" id="expired_at" class="form-control" placeholder="Expired at" name="expired_at">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 label-control">Wallet status</label>
                        <div class="col-md-9">
                            <fieldset id="wallet-status">
                                <div class="d-inline-block custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input" name="input-radio-3" id="Active" value="1">
                                    <label class="custom-control-label" for="Active">Active</label>
                                </div>
                                <div class="d-inline-block custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input" name="input-radio-3" id="Inactive" value="0">
                                    <label class="custom-control-label" for="Inactive">Inactive</label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" class="btn btn-warning mr-1">
                        <i class="ft-x"></i> Cancel
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <i class="la la-check-square-o"></i> Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var walletStatus;
    function parseURLParams(url) {
        var queryStart = url.indexOf("?") + 1,
            queryEnd = url.indexOf("#") + 1 || url.length + 1,
            query = url.slice(queryStart, queryEnd - 1),
            pairs = query.replace(/\+/g, " ").split("&"),
            parms = {}, i, n, v, nv;

        if (query === url || query === "") return;

        for (i = 0; i < pairs.length; i++) {
            nv = pairs[i].split("=", 2);
            n = decodeURIComponent(nv[0]);
            v = decodeURIComponent(nv[1]);

            if (!parms.hasOwnProperty(n)) parms[n] = [];
            parms[n].push(nv.length === 2 ? v : null);
        }
        return parms;
    }
    var urlString = window.location.href;
    urlParams = parseURLParams(urlString);
    walletId = urlParams.id[0];

    console.log(urlString, urlParams.id[0]);



    $.ajax({
        method: "Get",
        url: "http://spoon.api.myspoon.me/api/admin/v1/walletById/" + walletId,
        data: {
            token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
        }
    })
        .done(function (msg) {
            console.log(msg);
            $("#comments").val(msg.data.comments);
            $("#expired_at").val(msg.data.expired_at);
            $("#credit").val(msg.data.credit);

            // window.location = 'user-wallet.html?id=' + userId;
            // console.log(wallet);
        });

    $('#wallet-status input[type=radio]').change(function () {
        walletStatus = $(this).val();
        console.log(walletStatus);
    })

    $('#edit-user-wallet').on('submit', function (e) {
        e.preventDefault();
        var wallet = {};
        wallet.credit = $('#credit').val();
        wallet.status = walletStatus;
        wallet.expired_at = $('#expired_at').val();
        console.log(wallet)

        $.ajax({
            method: "PUT",
            url: "http://spoon.api.myspoon.me/api/admin/v1/wallets/" + walletId,
            data: {
                expired_at: wallet.expired_at,
                credit: wallet.credit,
                status: wallet.status,
                token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
            }
        }).done(function (msg) {
            console.log(msg);
        });

    });
</script>
@endsection