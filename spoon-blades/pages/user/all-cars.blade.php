@extends('layout.layout')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">All Cars</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">Home</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <a href="../../spoon/user/create-user.html" class="btn btn-purple table-add-more">Add User</a>

            <!-- CONTENT GOES HERE -->
            <table class="table table-striped table-bordered text-inputs-searching" id="user-table">
                <thead>
                    <tr>
                        <tr>
                            <th>id</th>
                            <th>model</th>
                            <th>color</th>
                            <th>plate number</th>
                            <th>created At</th>
                        </tr>
                    </tr>
                </thead>
                <tbody>
                </tbody>

                <tfoot>
                    <tr>
                        <tr>
                            <th>id</th>
                            <th>model</th>
                            <th>color</th>
                            <th>plate number</th>
                            <th>created At</th>
                        </tr>
                    </tr>
                </tfoot>

            </table>

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#user-table").DataTable(
        {
            processing: true,
            ajax: {
                type: 'GET',
                url: "http://spoon.api.myspoon.me/api/admin/v1/cars",
                dataSrc: "data",
                headers: {
                    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
                },
            },
            columns: [
                {
                    data: "id"
                },
                {
                    data: "model"
                },
                {
                    data: "color"
                },
                {
                    data: "plate_num"
                },
                {
                    data: "created_at"
                }

            ]
        },

    );
</script>
@endsection