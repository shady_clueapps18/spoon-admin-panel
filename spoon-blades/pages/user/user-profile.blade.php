@extends('layout.layout')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">User Profile</h3>
                <div class="row breadcrumbs-top d-inline-block">
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- CONTENT GOES HERE -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h1>User Details</h1>
                            <a class="heading-elements-toggle">
                                <i class="la la-ellipsis-v font-medium-3"></i>
                            </a>
                            <div id="user-actions">
                                <button type="button" class="btn btn-icon btn-success mr-1 " id="send-notification-action" data-toggle="tooltip" data-placement="bottom"
                                    title="Send Notification">
                                    <i class=" ft-bell "></i>
                                </button>
                                <button type="button " class="btn btn-icon btn-info mr-1 " id="edit-user-action" data-toggle="tooltip" data-placement="bottom"
                                    title="Edit">
                                    <i class="ft-edit"></i>
                                </button>
                                <button type="button" class="btn btn-icon btn-secondary mr-1" id="view-reviews-action" data-toggle="tooltip" data-placement="bottom"
                                    title="View Review">
                                    <i class=" ft-star "></i>
                                </button>
                                <!-- <button type="button " class="btn btn-icon btn-danger mr-1 " id="cancel-button" data-toggle="tooltip" data-placement="bottom"
                                    title="Block">
                                    <i class="ft-slash"></i>
                                </button> -->
                                <button type="button " class="btn btn-icon btn-danger mr-1 " id="block-user" data-toggle="tooltip" data-placement="bottom"
                                    title="Block">
                                    <i class="ft-slash"></i>
                                </button>
                            </div>

                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li>
                                        <a data-action="collapse">
                                            <i class="ft-minus"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="personal-info col-6">
                                        <div class="row">
                                            <div class="col-6">
                                                <img id="image" src="" alt="No image loaded" />
                                            </div>
                                            <div class="col-6 main-details">
                                                <div class="card-label">
                                                    <span class="title">ID:</span>
                                                    <span id="id"></span>
                                                </div>
                                                <div class="card-label">
                                                    <span class="title">Name:</span>
                                                    <span id="Name"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 user-info">
                                        <div class="card-body">

                                            <div class="card-label">
                                                <span class="title">Profession:</span>
                                                <span id="Profession"></span>
                                            </div>

                                            <div class="card-label">
                                                <span class="title">wallet credit: </span>
                                                <a href="" id="wallet_credit"></a>
                                                <span class="title">EGP</span>
                                            </div>
                                            <div class="card-label">
                                                <span class="title">No. of orders: </span>
                                                <a href="" id="orders"></a>
                                            </div>
                                            <div class="card-label">
                                                <span class="title">No. of favorites: </span>
                                                <span id="favorites"></span>
                                            </div>
                                            <div class="card-label">
                                                <span class="title">No. of referrals: </span>
                                                <a href="" id="referrals"></a>
                                            </div>
                                            <div class="card-label">
                                                <span class="title">referral code: </span>
                                                <span id="referral_code"></span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 user-info">
                                        <div class="card-body">
                                            <div class="card-label">
                                                <span class="title">Email: </span>
                                                <span id="email"></span>
                                            </div>
                                            <div class="card-label">
                                                <span class="title">Age: </span>
                                                <span id="Age"></span>
                                            </div>
                                            <div class="card-label">
                                                <span class="title">Education: </span>
                                                <span id="Education"></span>
                                            </div>
                                            <div class="card-label">
                                                <span class="title">mobile: </span>
                                                <span id="Mobile"></span>
                                            </div>
                                            <div class="card-label">
                                                <span class="title">Created at: </span>
                                                <span id="Created_at"></span>
                                            </div>
                                            <div class="card-label">
                                                <span class="title">updated at: </span>
                                                <span id="updated_at"></span>
                                            </div>
                                            <div class="card-label">
                                                <span class="title">Login type:</span>
                                                <span id="Login_type"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1>User Devices</h1>
                        <a class="heading-elements-toggle">
                            <i class="la la-ellipsis-v font-medium-3"></i>
                        </a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li>
                                    <a data-action="collapse">
                                        <i class="ft-minus"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <table class="table table-striped table-bordered devices" id="user-devices-table">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>device id</th>
                                        <th>model</th>
                                        <th>online/offline</th>
                                        <th>platform</th>
                                        <th>app version</th>
                                        <th>created At</th>
                                        <th>updated at</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>device id</th>
                                        <th>model</th>
                                        <th>online/offline</th>
                                        <th>platform</th>
                                        <th>app version</th>
                                        <th>created At</th>
                                        <th>updated at</th>
                                    </tr>
                                </tfoot>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1>User cars</h1>
                        <a class="heading-elements-toggle">
                            <i class="la la-ellipsis-v font-medium-3"></i>
                        </a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li>
                                    <a data-action="collapse">
                                        <i class="ft-minus"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <table class="table table-striped table-bordered text-inputs-searching" id="user-cars-table">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>model</th>
                                        <th>color</th>
                                        <th>plate number</th>
                                        <th>created At</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>model</th>
                                        <th>color</th>
                                        <th>plate number</th>
                                        <th>created At</th>
                                    </tr>
                                </tfoot>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    const id = window.location.search.split('id=')[1];
    // function parseURLParams(url) {
    //     var queryStart = url.indexOf("?") + 1,
    //         queryEnd = url.indexOf("#") + 1 || url.length + 1,
    //         query = url.slice(queryStart, queryEnd - 1),
    //         pairs = query.replace(/\+/g, " ").split("&"),
    //         parms = {}, i, n, v, nv;

    //     if (query === url || query === "") return;

    //     for (i = 0; i < pairs.length; i++) {
    //         nv = pairs[i].split("=", 2);
    //         n = decodeURIComponent(nv[0]);
    //         v = decodeURIComponent(nv[1]);

    //         if (!parms.hasOwnProperty(n)) parms[n] = [];
    //         parms[n].push(nv.length === 2 ? v : null);
    //     }
    //     return parms;
    // }
    // var urlString = window.location.href;
    // urlParams = parseURLParams(urlString);
    // userId = urlParams.id[0];

    // console.log(urlString, urlParams.id[0]);
    // var userDevices;
    // var userCars;


    $.ajax({
        method: "GET",
        url: `http://spoon.api.myspoon.me//api/admin/v1/users/${id}`,
        headers: {
            "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
        },
        success: data => {
            $("#id").html(msg.data[0].id);
            $("#Name").html(msg.data[0].name);
            $("#image").attr("src", msg.data[0].image);
            $("#Age").html(msg.data[0].id); //didn't sent with api
            $("#Profession").html(msg.data[0].id);//disn't sent with api
            $("#Education").html(msg.data[0].id);
            $("#Mobile").html(msg.data[0].country_code + msg.data[0].mobile);
            $("#wallet_credit").html(msg.data[0].wallet_credit);
            $("#email").html(msg.data[0].email);
            $("#orders").html(msg.data[0].orders_num);
            $("#favorites").html(msg.data[0].favorites);//didn't sent with api
            $("#referrals").html(msg.data[0].referrals_num);
            $("#referral_code").html(msg.data[0].ref);
            $("#Created_at").html(msg.data[0].created_at);//didn't sent with api
            $("#updated_at").html(msg.data[0].updated_at);
            $("#Login_type").html(msg.data[0].signup_method);
        }
    }).done(function (msg) {
        console.log(msg);
        userDevices = msg.data[0].devices;
        userCars = msg.data[0].cars;
        console.log("devices", userDevices, "cars", userCars);

        // DataTable
        var tableDevice = $("#user-devices-table").DataTable(
            {
                processing: true,
                data: userDevices,
                columns: [
                    {
                        data: function (data, type, row, meta) {
                            return data.id;
                        }
                    },
                    {
                        data: function (data, type, row, meta) {
                            return data.device_id;
                        }
                    },
                    {
                        data: function (data, type, row, meta) {
                            return data.device_model;
                        }
                    },
                    {
                        data: function (data, type, row, meta) {
                            if (data.online) {
                                return "online"
                            }
                            if (!data.online) {
                                return "offline"
                            }
                        }
                    },
                    {
                        data: function (data, type, row, meta) {
                            return data.platform;
                        }
                    },
                    {
                        data: function (data, type, row, meta) {
                            return data.version;
                        }
                    },
                    {
                        data: function (data, type, row, meta) {
                            return data.created_at;
                        }
                    },
                    {
                        data: function (data, type, row, meta) {
                            return data.updated_at;
                        }
                    }

                ]
            }
        );

        // DataTable
        var tableCarSearching = $("#user-cars-table").DataTable(
            {

                processing: true,
                data: userCars,
                columns: [
                    {
                        data: "id"
                    },
                    {
                        data: "model"
                    },
                    {
                        data: "color"
                    },
                    {
                        data: "plate_num"
                    },
                    {
                        data: "created_at"
                    }

                ]
            }
        );


    });

    $(document).on('click', '#wallet_credit', function (e) {
        e.preventDefault();
        console.log(userId, "id");
        window.location = 'user-wallet.html?id=' + id;
    });

    $(document).on('click', '#orders', function (e) {
        e.preventDefault();
        console.log(id, "id");
        window.location = 'user-orders.html?id=' + id;
    });

    $(document).on('click', '#referrals', function (e) {
        e.preventDefault();
        console.log(id, "id");
        window.location = 'user-referrals.html?id=' + id;
    });

    $(document).on('click', '#send-notification-action', function (e) {
        e.preventDefault();
        window.location = '../create-notification.html?id=' + id;
    });

    $(document).on('click', '#view-reviews-action', function (e) {
        e.preventDefault();
        window.location = '../users-reviews.html?id=' + id;
    });

    $(document).on('click', '#edit-user-action', function (e) {
        e.preventDefault();
        window.location = 'edit-user.html?id=' + id;
    });

    $(document).on('click', '#block-user', function (e) {
        e.preventDefault();
        console.log(id);
        swal({
            title: "Are you sure?",
            text: "This user will be blocked!",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "No, cancel plz!",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: false,
                },
                confirm: {
                    text: "Yes, block it!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        })
            .then((isConfirm) => {
                if (isConfirm) {
                    $.ajax({
                        method: "PUT",
                        url: "http://spoon.api.myspoon.me/api/admin/v1/blockUser",
                        type: 'PUT',
                        data: {
                            id: id,
                            token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
                        }
                    }).done(function (msg) {
                        console.log("user Blocked");
                        swal("Deleted!", "This user has been blocked.", "success");
                    });

                } else {
                    swal("Cancelled", "This user is safe", "error");
                }
            });
    });

</script>
@endsection