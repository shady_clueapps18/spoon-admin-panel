@extends('layout.layout')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">Edit User</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">Home</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">

            <!-- CONTENT GOES HERE -->
            <form class="form form-horizontal" id="edit-user" novalidate>
                <div class="form-body">
                    <h4 class="form-section">
                        <i class="ft-user"></i> Personal Info</h4>
                    <div class="form-group row">
                        <label class="col-md-2 label-control" for="username" required>Name
                            <span class="required"> *</span>
                        </label>
                        <div class="controls col-md-9">
                            <input type="text" id="username" class="form-control" placeholder="Name" pattern="[a-z,A-Z,\s]{1,}" maxlength="50" data-validation-pattern-message="Special chars are not allowed"
                                name="name" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 label-control" for="email">E-mail</label>
                        <div class="controls col-md-9">
                            <input type="email" id="email" class="form-control" placeholder="E-mail" name="email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 label-control" for="selectedCountry">country key
                            <span class="required"> *</span>
                        </label>
                        <div class="col-md-9">
                            <select id="selectedCountry" name="selectedCountry" class="form-control">
                                <option value="Cairo" selected=""> Cairo</option>
                                <option value="Gizat">Giza</option>
                                <option value="Alexandria">Alexandria</option>
                                <option value="Benha">Benha</option>
                                <option value="Mansoura">Mansoura</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 label-control" for="phone">Mobile Number
                            <span class="required"> *</span>
                        </label>
                        <div class="col-md-2">
                            <input type="text" readonly="readonly" id="countryKey" class="form-control" value="">
                        </div>
                        <div class="col-md-7">
                            <input type="text" id="phone" class="form-control" placeholder="Mobile Number" maxlength="11" name="phone" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 label-control" for="password">Password
                            <span class="required"> *</span>
                        </label>
                        <div class="controls col-md-9">
                            <input type="password" id="password" class="form-control" placeholder="Password" name="password" pattern="[^\s]+" minlength="8"
                                data-validation-minlength-message="require min. 8 chars, no spaces allowed" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 label-control">User Type</label>
                        <div class="col-md-9">
                            <fieldset id="typeOfUser">
                                <div class="d-inline-block custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input" name="input-radio-3" id="userType" value="0">
                                    <label class="custom-control-label" for="userType">User</label>
                                </div>
                                <div class="d-inline-block custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input" name="input-radio-3" id="superAdminType" value="1">
                                    <label class="custom-control-label" for="superAdminType">Super Admin</label>
                                </div>
                                <div class="d-inline-block custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input" name="input-radio-3" id="adminPrividerType" value="2">
                                    <label class="custom-control-label" for="adminPrividerType">Admin Provider</label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 label-control" for="userImage">Image</span>
                    </label>
                    <div class="col-md-9">
                        <label class="file center-block">
                            <input type="file" id="userImage">
                            <img src="" id="image">
                            <span class="file-custom"></span>
                        </label>
                    </div>
                </div>

        </div>
        <div class="form-actions">
            <button type="button" class="btn btn-warning mr-1">
                <i class="ft-x"></i> Cancel
            </button>
            <button type="submit" class="btn btn-primary">
                <i class="la la-check-square-o"></i> Save
            </button>
        </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script>
    function parseURLParams(url) {
        var queryStart = url.indexOf("?") + 1,
            queryEnd = url.indexOf("#") + 1 || url.length + 1,
            query = url.slice(queryStart, queryEnd - 1),
            pairs = query.replace(/\+/g, " ").split("&"),
            parms = {}, i, n, v, nv;

        if (query === url || query === "") return;

        for (i = 0; i < pairs.length; i++) {
            nv = pairs[i].split("=", 2);
            n = decodeURIComponent(nv[0]);
            v = decodeURIComponent(nv[1]);

            if (!parms.hasOwnProperty(n)) parms[n] = [];
            parms[n].push(nv.length === 2 ? v : null);
        }
        return parms;
    }
    var urlString = window.location.href;
    urlParams = parseURLParams(urlString);
    userId = urlParams.id[0];

    console.log(urlString, urlParams.id[0]);


    $.ajax({
        method: "GET",
        url: "http://spoon.api.myspoon.me/api/admin/v1/users/?id=" + userId,
        data: {
            // id: userId,
            "Content-Type": 'application/json',
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
        }
    }).done(function (msg) {
        console.log(msg);
        $("#username").val(msg.data[0].name);
        $("#email").val(msg.data[0].email);
        $("#selectedCountry").val(msg.data[0].country);
        $("#countryKey").val(msg.data[0].country_code);
        $("#phone").val(msg.data[0].mobile);
        $("#password").val(msg.data[0].password);
        $("#type").val(msg.data[0].type);
        $("#image").attr('src', msg.data[0].image);

    });

    //change countryKey based on selected country

    $(document).on("change", "#selectedCountry", function () {
        $("#countryKey").val(this.value); // this.value is enough for you
    }).val($('#countryKey').val('cairo')).change(); // for pre-selection trigger

    // var handleFormSubmit = function () {
    var typeOfUser;
    var userImage;

    function readFile() {

        if (this.files && this.files[0]) {

            var FR = new FileReader();

            FR.addEventListener("load", function (e) {
                console.log(e.target.result);
                console.log(e.target.result.split(','))
                var base64 = e.target.result.split(',');
                userImage = base64[1];
                console.log(base64[1]);
            });

            FR.readAsDataURL(this.files[0]);
            console.log(this.files[0]);
        }

    }

    document.getElementById("userImage").addEventListener("change", readFile);


    $('#typeOfUser input[type=radio]').change(function () {
        typeOfUser = $(this).val();
        console.log(typeOfUser);
    })

    $('#edit-user').on('submit', function (e) {
        e.preventDefault();
        var user = {};
        user.name = $('#username').val();
        user.email = $('#email').val();
        user.country = $('#selectedCountry').val();
        user.countryKey = $('#countryKey').val();
        user.phone = $('#phone').val();
        user.password = $('#password').val();
        user.type = typeOfUser;
        user.image = userImage;
        console.log(user)
        $.ajax({
            method: "PUT",
            url: "http://spoon.api.myspoon.me/api/admin/v1/users/" + userId,
            data: {
                "Content-Type": 'application/json',
                // id: userId,
                country_code: '002',
                name: user.name,
                mobile: user.phone,
                password: user.password,
                type: user.type,
                email: user.email,
                image: user.image,
                token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
            }
        })
            .done(function (msg) {
                console.log(msg);
                window.location = 'user-profile.html?id=' + userId;

            });

        console.log(user);
    });
</script>
@endsection