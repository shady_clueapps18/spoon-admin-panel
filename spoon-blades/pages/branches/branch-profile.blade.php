@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Provider Profile</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
      <div class="content-header-right col-md-6 col-12 mb2">
        <div class="provider-actions">
          <button class="btn btn-primary dropdown-toggle">Branch Actions</button>
        </div>
      </div>
    </div>
    <div class="content-body">
      <!-- CONTENT GOES HERE -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h1>Branch Info</h1>
              <a class="heading-elements-toggle">
                <i class="la la-ellipsis-v font-medium-3"></i>
              </a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li>
                    <a data-action="collapse">
                      <i class="ft-minus"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-content collapse show">
              <div class="card-body">
                <div class="row">
                  <div class="col-12 col-md-6">
                    <div class="provider-main-info d-flex">
                      <div class="branch-img-wrapper">
                        <img src="" alt="">
                      </div>
                      <div class="branch-id-name">
                        <div class="branch-id text-bold-700 h2">
                          ID :
                          <span class="text-bold-400"></span>
                        </div>
                        <div class="branch-name text-bold-700 h2">Name :
                          <span class="text-bold-400"></span>
                        </div>
                        <div class="branch-bundle text-bold-700 h2">Bundle :
                          <span class="text-bold-400"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="provider-sec-info">
                      <div class="branch-main-branch text-bold-700 h5"> Main Branch :
                        <span class="text-bold-400"></span>
                      </div>
                      <div class="branch-type text-bold-700 h5"> Branch Type :
                        <span class="text-bold-400"></span>
                      </div>
                      <div class="branch-created-at text-bold-700 h5"> Created At :
                        <span class="text-bold-400"></span>
                      </div>
                      <div class="branch-updated-at text-bold-700 h5"> Updated At :
                        <span class="text-bold-400"></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 col-md-6">
                    <div class="branch-contacts">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <ul>
                              <li class="branch-email text-bold-700 h5 no-bullets"> Email Address :
                                <span class="text-bold-400"></span>
                              </li>
                              <li class="branch-phone text-bold-700 h5"> Phone :
                                <span class="text-bold-400"></span>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="branch-categories">
                      <h2>Categories : </h2>
                      <div class="badge badge-primary"></div>
                      <div class="badge badge-primary"></div>
                      <div class="badge badge-primary"></div>
                      <div class="badge badge-primary"></div>
                    </div>
                    <div class="branch-features">
                      <h2>Features : </h2>
                      <div class="badge badge-primary"></div>
                      <div class="badge badge-primary"></div>
                      <div class="badge badge-primary"></div>
                      <div class="badge badge-primary"></div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="branch-description">
                      <h2>Description</h2>
                      <p></p>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <div class="branch-additional-info">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <h2>Additional Info</h2>
                            <ul>
                              <div class="row">
                                <div class="col-6">
                                  <li class="branch-services text-bold-700 h5 no-bullets"> Services :
                                    <span class="text-bold-400"></span>
                                  </li>
                                  <li class="branch-flags text-bold-700 h5"> Flags :
                                    <span class="text-bold-400"></span>
                                  </li>
                                  <li class="branch-weight text-bold-700 h5"> Weight :
                                    <span class="text-bold-400"></span>
                                  </li>
                                </div>
                                <div class="col-6">
                                  <li class="branch-service-percent text-bold-700 h5"> Service % :
                                    <span class="text-bold-400"></span>
                                  </li>
                                  <li class="branch-taxes text-bold-700 h5"> Taxes % :
                                    <span class="text-bold-400"></span>
                                  </li>
                                </div>
                              </div>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h1>Branch Stats</h1>
              <a class="heading-elements-toggle">
                <i class="la la-ellipsis-v font-medium-3"></i>
              </a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li>
                    <a data-action="collapse">
                      <i class="ft-minus"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-content collapse show">
              <div class="card-body">
                <div class="row">
                  <div class="col-6 col-md-3">
                    <div class="branch-orders">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <div class="media d-flex">
                              <div class="align-self-center">
                                <i class="la la-rocket primary font-large-2 float-left"></i>
                              </div>
                              <div class="media-body text-right">
                                <h3></h3>
                                <span>Orders</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-6 col-md-3">
                    <div class="branch-branches">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <div class="media d-flex">
                              <div class="align-self-center">
                                <i class="la la-coffee warning font-large-2 float-left"></i>
                              </div>
                              <div class="media-body text-right">
                                <h3></h3>
                                <span>Branches</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-6 col-md-3">
                    <div class="branch-favs">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <div class="media d-flex">
                              <div class="align-self-center">
                                <i class="la la-heart-o danger font-large-2 float-left"></i>
                              </div>
                              <div class="media-body text-right">
                                <h3></h3>
                                <span>Favorites</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-6 col-md-3">
                    <div class="branch-reviews">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <div class="media d-flex">
                              <div class="align-self-center">
                                <i class="la la-star-o yellow font-large-2 float-left"></i>
                              </div>
                              <div class="media-body text-right">
                                <h3></h3>
                                <span>Reviews</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-6 col-md-3">
                    <div class="branch-pickups">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <div class="media d-flex">
                              <div class="align-self-center">
                                <i class="la la-star-o yellow font-large-2 float-left"></i>
                              </div>
                              <div class="media-body text-right">
                                <h3></h3>
                                <span>Pickups</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-6 col-md-3">
                    <div class="branch-dineins">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <div class="media d-flex">
                              <div class="align-self-center">
                                <i class="la la-star-o yellow font-large-2 float-left"></i>
                              </div>
                              <div class="media-body text-right">
                                <h3></h3>
                                <span></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-6 col-md-3">
                    <div class="branch-bookings">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <div class="media d-flex">
                              <div class="align-self-center">
                                <i class="la la-star-o yellow font-large-2 float-left"></i>
                              </div>
                              <div class="media-body text-right">
                                <h3></h3>
                                <span>Bookings</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h1>Branch Admins</h1>
              <a class="heading-elements-toggle">
                <i class="la la-ellipsis-v font-medium-3"></i>
              </a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li>
                    <a data-action="collapse">
                      <i class="ft-minus"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-content collapse show">
              <div class="card-body">

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split('id=')[1]
  let $id = $('.branch-id > span')
  let name = $('.branch-name > span')
  let bundle = $('.branch-bundle > span')
  let logo = $('.branch-img-wrapper img');
  let mainBranch = $('.branch-main-branch > span')
  let type = $('.branch-type > span')
  let createdAt = $('.branch-created-at > span')
  let updatedAt = $('.branch-updated-at > span')
  let email = $('.branch-email > span')
  let phone = $('.branch-phone > span')
  let descripiton = $('.branch-description p')
  let categories = $('.branch-categories');
  let features = $('.branch-features');
  let services = $('.branch-services');
  let flags = $('.branch-flags > span');
  let weight = $('.branch-weight > span');
  let servicesPercentage = $('.branch-service-percent > span');
  let taxes = $('.branch-taxes > span');
  let orders = $('.branch-orders h3')
  let branches = $('.branch-branches h3')
  let favs = $('.branch-favs h3')
  let reviews = $('.branch-reviews h3')
  let pickups = $('.branch-pickups h3')
  let dineIns = $('.branch-dineins h3')
  let bookings = $('.branch-bookings h3')
  // let preparationTime = $('.branch-res-prep').find('span');
  // let cuisines = $('.branch-res-cuisines');
  // let $priceRange = $('.branch-res-price').find('span');

  $.ajax({
    type: 'GET',
    url: `http://spoon.api.myspoon.me/api/admin/v1/providerById/${id}`,
    success: ({ data }) => {
      $($id).text(data.id)
      $(name).text(data.name)
      $(logo).attr('src', data.logo)

      //FIXME: Return the main branch, created at, updated at!
      $(mainBranch).text(`${data.branch.area} - ${data.branch.city}`)
      $(createdAt).text(data.created_at)
      $(updatedAt).text(data.updated_at)


      $(descripiton).text(data.description)
      $(type).text(data.grocery == null ? "Resturant" : "Grocery")
      $(email).text(data.contact_email == null ? "Not Provided" : data.contact_email)
      $(phone).text(data.contact_phone == null ? "Not Provided" : data.contact_phone)
      $(services).text(data.service)
      $(taxes).text(data.taxes)
      $(orders).text(data.branch.order_count)
      $(favs).text(data.num_favorite)
      $(branches).text(data.branch_count)
      $(reviews).text(data.num_review)
      if (data.grocery == null) {

        $(preparationTime).text(data.branch.restaurant.preparation_time);

        switch (data.branch.restaurant.price_range) {
          case 1:
            $($priceRange).text('$')
            break;

          case 2:
            $($priceRange).text('$$')
            break;

          case 3:
            $($priceRange).text('$$$')
            break;

          default:
            break;
        }

        if (!data.branch.restaurant.cuisines.length) {
          $(cuisines).append(`<span>No Cuisines Found!</span>`)
        } else {
          $(cuisines).append(data.branch.restaurant.cuisines.map(cuisine => {
            return `
          <div class="badge badge-primary">${cuisine}</div>
            `
          }))
        }
      }

      $(categories).append(data.categories.map(cat => {
        return `
          <div class="badge badge-primary">${cat}</div>
        `
      }))

      if (data.grocery != null) {
        $('.add-rest-info, .rest-menu-sec').addClass('d-none');

      }
    },
    error: err => console.log(err),
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    }
  })
</script>
@endsection