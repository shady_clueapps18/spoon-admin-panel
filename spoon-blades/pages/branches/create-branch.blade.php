@extends('layouts.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <section id="validation">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Create Branch</h4>
                <a class="heading-elements-toggle">
                  <i class="la la-ellipsis-h font-medium-3"></i>
                </a>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <form class="steps-validation wizard-circle" id="add-branch-form">

                    <!-- Step 1 -->
                    <h6>Branch Info</h6>
                    <fieldset>
                      <div class="row">

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="branch-country">
                              Country :
                              <span class="danger">*</span>
                            </label>
                            <select name="branchCountry" id="branch-country" style="width:100%">
                            </select>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="branch-city">
                              City :
                              <span class="danger">*</span>
                            </label>
                            <select name="branchcity" id="branch-city" style="width:100%">
                            </select>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="branch-area">
                              Country :
                              <span class="danger">*</span>
                            </label>
                            <select name="branchArea" id="branch-area" style="width:100%">
                            </select>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="branch-address">
                              Address :
                              <span class="danger">*</span>
                            </label>
                            <input type="text" class="form-control required" id="branch-address" name="branchAddress">
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <div class="row">
                              <div class="col-12 col-md-6">
                                <label for="latitude">
                                  Latitude :
                                  <span class="danger">*</span>
                                </label>
                                <input type="text" class="form-control required" id="latitude" name="latitude">
                              </div>
                              <div class="col-12 col-md-6">
                                <label for="longitude">
                                  Longitude :
                                  <span class="danger">*</span>
                                </label>
                                <input type="text" class="form-control required" id="longitude" name="longitude">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-12 col-md-6">
                          <label>Branch Image</label>
                          <div id="branch-images"></div>
                        </div>
                        <div class="col-12 col-md-6">
                          <div class="form-group">
                            <label for="branch-open-from">
                              Open From :
                              <span class="danger">*</span>
                            </label>
                            <input type="text" class="form-control required" id="branch-open-from" name="branchOpenFrom">
                          </div>
                          <div class="form-group">
                            <label for="branch-open-to">
                              Open To :
                              <span class="danger">*</span>
                            </label>
                            <input type="text" class="form-control required" id="branch-open-to" name="branchOpenTo">
                          </div>
                          <div class="form-group">
                            <label for="max-arrival">Max Arrival Time</label>
                            <input type="number" id="max-arrival" class="form-control" required>
                          </div>
                        </div>
                      </div>
                    </fieldset>

                    <!-- Step 2 -->
                    <h6>Step 2</h6>
                    <fieldset>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="branch-features">
                              Features :
                              <span class="danger">*</span>
                            </label>
                            <select name="branchfeatures" id="branch-features" multiple="multiple" style="width:100%">
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group position-relative">
                            <label for="branch-min-order">
                              Min Order :
                              <span class="danger">*</span>
                            </label>
                            <input type="number" class="form-control required" id="branch-min-order" name="branchTaxes">
                            <span class="percentage">EGP</span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="branch-price-range">
                              Price Range :
                            </label>
                            <select class="custom-select form-control" id="branch-price-range" name="branchPriceRang">
                              <option value="1">$</option>
                              <option value="2">$$</option>
                              <option value="3">$$$</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <p style="color: #2b335e">Services</p>
                            <div class="form-check">
                              <label for="branch-car-pickup" class="form-check-label">
                                Car Pickup :
                                <span class="danger">*</span>
                                <input type="checkbox" id="branch-car-pickup" class="form-check-input">
                                <span class="checkmark"></span>
                              </label>
                            </div>
                            <div class="form-check">
                              <label for="branch-pickup" class="form-check-label">
                                Pickup :
                                <span class="danger">*</span>
                                <input type="checkbox" id="branch-pickup" class="form-check-input">
                                <span class="checkmark"></span>
                              </label>
                            </div>
                            <div class="form-check">
                              <label for="branch-dine-in" class="form-check-label">
                                Dine-In :
                                <span class="danger">*</span>
                                <input type="checkbox" id="branch-dine-in" class="form-check-input">
                                <span class="checkmark"></span>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row d-none" id="additional-dinein-data">
                        <div class="col-12 col-md-6">
                          <p style="color: #2b335e">Table Places</p>
                          <div class="form-check">
                            <label for="table-place-in" class="form-check-label">Indoor
                              <input type="checkbox" id="table-place-in" class="form-check-input">
                              <span class="checkmark"></span>
                            </label>
                          </div>
                          <div class="form-check">
                            <label for="table-place-out" class="form-check-label">Outdoor
                              <input type="checkbox" id="table-place-out" class="form-check-input">
                              <span class="checkmark"></span>
                            </label>
                          </div>
                          <div class="form-group">
                            <label for="max-num-tables">Max number of tables</label>
                            <input type="number" id="max-num-tables" class="form-control">
                          </div>
                          <div class="form-group">
                            <label for="max-num-guests">Max number of guests</label>
                            <input type="number" id="max-num-guests" class="form-control">
                          </div>
                        </div>
                        <div class="col-12 col-md-6">

                          <p style="color: #2b335e">Deposit Required</p>
                          <div class="form-check">
                            <label for="deposit-required-yes" class="form-check-label">Yes
                              <input type="radio" name="depositRequired" value="yes" id="deposit-required-yes">
                              <span class="checkmark checkmark-radio"></span>
                            </label>
                          </div>
                          <div class="form-check">
                            <label for="deposit-required-no" class="form-check-label">No
                              <input type="radio" name="depositRequired" value="no" id="deposit-required-no" checked>
                              <span class="checkmark checkmark-radio"></span>
                            </label>
                          </div>
                          <div class="form-group row">
                            <div class="col-12 col-md-6">
                              <label for="deposit-type">Deposit Type</label>
                              <select name="depositType" id="deposit-type" class="form-control" disabled>
                                <option value="deposit-fixed">Fixed</option>
                                <option value="deposit-percentage">Percentage</option>
                              </select>
                            </div>
                            <div class="col-12 col-md-6">
                              <label for="deposit-amount">Deposit Amount</label>
                              <input type="text" placeholder="Deposit Amount" class="form-control" id="deposit-amount" disabled>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="days-before-booking">Days Before Booking</label>
                            <input type="number" id="days-before-booking" class="form-control">
                          </div>
                        </div>
                      </div>
                    </fieldset>

                    <!-- Step 3 -->
                    <h6>Admin Info</h6>
                    <fieldset>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="branch-admin-name">
                              Name:
                              <span class="danger">*</span>
                            </label>
                            <input type="text" class="form-control required" id="branch-admin-name" name="branchAdminName">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="branch-admin-phone">
                              Phone Number :
                              <span class="danger">*</span>
                            </label>
                            <input type="tel" class="form-control required" id="branch-admin-phone" name="branchAdminPhone">
                          </div>
                        </div>
                        <div class="col-12 col-md-6">
                          <div class="form-group">
                            <label for="branch-admin-email">
                              Email Address :
                              <span class="danger">*</span>
                            </label>
                            <input type="email" class="form-control required" id="branch-admin-email" name="branchAdminEmail">
                          </div>
                        </div>
                        <div class="col-12 col-md-6">
                          <div class="form-group">
                            <label for="branch-admin-password">
                              Password :
                              <span class="danger">*</span>
                            </label>
                            <input type="password" class="form-control required" id="branch-admin-password" name="branchAdminPassword">
                          </div>
                        </div>
                      </div>
                    </fieldset>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Form wizard with step validation section end -->

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>

  $('input[name="depositRequired"]').on('change', function () {
    if ($(this).val() === 'yes') {
      $('#deposit-type, #deposit-amount').prop('disabled', false)
    } else {
      $('#deposit-type, #deposit-amount').prop('disabled', true)
    }
  });

  $('#branch-open-from').timeDropper({ format: 'HH:mm' })
  $('#branch-open-to').timeDropper({ format: 'HH:mm' })

  $('#branch-price-range option').each(function (i) {
    let optionText = '$'.repeat(i + 1)
    $(this).text(optionText)
  })

  const id = window.location.search.split('id=')[1]

  let countries = [];
  let cities = [];
  let areas = [];
  let features = [];



  // Get All Countries
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/v1/countries',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      data.forEach(i => countries.push({ id: i.id, name: i.name }))
      countries.forEach(country => $('#branch-country').append(`<option value=${country.id}>${country.name}</option>`))
      $('#branch-country').select2();
    },
    error: err => console.log(err)
  });

  // Get All Cities with country Id
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/v1/cities?countryId=1',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      data.forEach(i => cities.push({ id: i.id, name: i.name }))
      cities.forEach(city => $('#branch-city').append(`<option value=${city.id}>${city.name}</option>`))
      $('#branch-city').select2();
    }
  });

  // Get Areas with City Id
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/v1/areas?cityId=1',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      data.forEach(i => areas.push({ id: i.id, name: i.name }))
      areas.forEach(area => $('#branch-area').append(`<option value=${area.id}>${area.name}</option>`))
      $('#branch-area').select2();
    }
  });

  // Get All Features
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/admin/v1/features?',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      data.forEach(i => features.push({ id: i.id, name: i.name }))
      features.forEach(feature => $('#branch-features').append(`<option value=${feature.id}>${feature.name}</option>`))
      $('#branch-features').select2();
    }
  });

  // Change cities on Country Change
  $('#branch-country').on('change', function () {
    cities = [];
    $.ajax({
      type: 'GET',
      url: `http://spoon.api.myspoon.me/api/v1/cities?countryId=${$(this).val()}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      success: ({ data }) => {
        $('#branch-city').find('option').each(function () {
          $(this).remove();
        });
        data.forEach(i => cities.push({ id: i.id, name: i.name }))
        console.log(cities)
        cities.forEach(city => $('#branch-city').append(`<option value=${city.id}>${city.name}</option>`))
      }
    })
  })

  // Change Areas on City Change
  $('#branch-city').on('change', function () {
    areas = [];
    $.ajax({
      type: 'GET',
      url: `http://spoon.api.myspoon.me/api/v1/areas?cityId=${$(this).val()}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      success: ({ data }) => {
        $('#branch-area').find('option').each(function () {
          $(this).remove();
        });
        data.forEach(i => areas.push({ id: i.id, name: i.name }))
        console.log(areas)
        areas.forEach(area => $('#branch-area').append(`<option value=${area.id}>${area.name}</option>`))
      }
    })
  })

  // On Change Dine In Settings
  $('#branch-dine-in').on('change', function () {
    $('#additional-dinein-data').toggleClass('d-none');
  });

  $('a[href="#finish"]').on('click', function (e) {
    e.preventDefault();
    const country = $('#branch-country').val();
    const city = $('#branch-city').val();
    const area = $('#branch-area').val();
    const address = $('#branch-address').val();
    const latitude = $('#latitude').val();
    const longitude = $('#longitude').val();


    const branchImages = [];
    const openFrom = `${$('#branch-open-from').val()}:00`;
    const openTo = `${$('#branch-open-to').val()}:00`;
    const maxArrivalTime = $('#max-arrival').val();
    const minOrder = $('#branch-min-order').val();
    const priceRange = $('#branch-price-range').val();
    const pickup = $('#branch-pickup');
    const carPickup = $('#branch-car-pickup');
    const dineIn = $('#branch-dine-in');
    let tableIndoor = $('#table-place-in').val();
    let tableOutdoor = $('#table-place-out').val();
    let maxNumTable = $('#max-num-tables').val();
    let maxNumGuests = $('#max-num-guests').val();
    let daysBeforeBooking = $('#days-before-booking').val();
    let depositRequired = $('#deposit-required-yes').prop('checked') === true ? $('#deposit-required-yes').val() : $('#deposit-required-no').val();
    const depositType = $('#deposit-type').val();
    let depositAmount = $('#deposit-amount').val();
    const adminName = $('#branch-admin-name').val();
    const adminPhone = $('#branch-admin-phone').val();
    const adminEmail = $('#branch-admin-email').val();
    const adminPassword = $('#branch-admin-password').val();
    const features = $('#branch-features').val();

    // If the services doesn't contain Dine-In
    if ($(dineIn).prop('checked') === false) {
      console.log('Passed')
      depositRequired = 'no';
      depositAmount = 0;
      daysBeforeBooking = 0;
      maxNumTable = 0;
      maxNumGuests = 0;
    }

    // If no Deposit is required
    if ($('#deposit-required-no').prop('checked') === true) {
      depositAmount = 0;
    }



    // Fill the data of images
    $('.dz-image img').each(function (i) {
      branchImages.push({ 'image': $(this).prop('src').split('base64,')[1], 'order': i + 1 })
    });

    const data = {
      provider_id: id,
      country_id: country,
      city_id: city,
      area_id: area,
      has_offer: 0,
      // TODO: Type to be removed [Restaurant || Grocery]
      type: 1,
      address: address,
      lat: latitude,
      lng: longitude,
      open_from: openFrom,
      open_to: openTo,
      max_arrival_time: maxArrivalTime,
      images: branchImages,
      name: adminName,
      mobile: adminPhone,
      email: adminEmail,
      password: adminPassword,
      minimum_charge: minOrder,
      deposit: depositRequired === 'yes' ? true : false,
      deposit_amount: depositType === 'deposit-fixed' ? parseInt(depositAmount) : 0,
      deposit_percentage: depositType === 'deposit-percentage' ? parseInt(depositAmount) : 0,
      pickup: $(pickup).prop('checked') === true ? true : false,
      car_pickup: $(carPickup).prop('checked') === true ? true : false,
      dine_in: $(dineIn).prop('checked') === true ? true : false,
      table_place: ($(tableIndoor).prop('checked') === true && $(tableOutdoor).prop('checked') === true) ? 'ALL' : $(tableIndoor).prop('checked') === true ? 'IN_DOOR' : 'OUT_DOOR',
      max_days: daysBeforeBooking,
      max_guests: maxNumGuests,
      num_table: maxNumTable,
      features: features
    }

    console.log(data)
    $.ajax({
      type: 'POST',
      url: 'http://spoon.api.myspoon.me/api/admin/v1/branches',
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: data,
      success: data => {
        console.log(data)
        swal('Success!', 'Branch is created Successfully', 'success')
        location.reload()
      },
      error: err => console.log(err)
    })
  })

</script>
@endsection