@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <table class="table table-striped table-bordered text-inputs-searching" id="branches-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Provider</th>
            <th>Type</th>
            <th>Branch</th>
            <th>Orders no.</th>
            <th>Total Payments</th>
            <th>Services</th>
            <th>Weight</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  // DataTable
  const id = window.location.search.split("id=")[1];
  console.log(id)
  $("#branches-table").DataTable(
    {
      processing: true,
      ajax: {
        type: 'GET',
        url: `http://spoon.api.myspoon.me/api/admin/v1/branchesByProviderId/1`,
        dataType: "json",
        dataSrc: "data",
        headers: {
          "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
        }
      },
      columns: [
        { data: data => data.branch.id },
        { data: "name" },
        {
          data: data => data.branch.type// == 1 ? "Restaurant" : "Grocery"

        },
        {
          data: data => data.branch.area// + ' - ' + data.branch.city

        },
        {
          data: data => `<a href="orders">${data.branch.num_order}</a>`
        },

        // TODO: Ask Mario to return "total payment"
        { data: "service" },
        { data: "favorite" },
        { data: "taxes" },
        {
          mRender: function (data, type, row, meta) {
            return `
              <button class="btn btn-sm btn-cyan view-branch"> View</button>
              <button class="btn btn-sm btn-info edit-branch">Edit</button>
              <button class="btn btn-sm btn-danger delete-branch">Delete</button>
              `;
          },
          className: "table-actions"
        }
      ]
    }
  );

  $(document).on('click', '.view-branch', function () {
    const branchId = $(this).parents('tr').find('td:first-of-type').text()
    const id = $(this).parents('tr').find('td:first-of-type').text();
    window.location = `./branch-profile.html?id=${id},branchId=${branchId}`
  });

  $(document).on('click', '.edit-branch', function () {
    const branchId = $(this).parents('tr').find('td:first-of-type').text()
    const id = $(this).parents('tr').find('td:first-of-type').text();
    window.location = `./editBranch.html?id=${id},branchId=${branchId}`
  });
</script>
@endsection