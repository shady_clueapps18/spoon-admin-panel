@extends('layout.layout')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
          <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
          <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item active">Home</li>
              </ol>
            </div>
          </div>
        </div>
      </div>

      <div class="content-body">

        <!-- CONTENT GOES HERE -->
        <section id="validation">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Create Notification</h4>
                  <a class="heading-elements-toggle">
                    <i class="la la-ellipsis-h font-medium-3"></i>
                  </a>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <form enctype="multipart/form-data" id="add-promo-form">
                      <div class="row">
                        <div class="col-12 col-md-6">
                          <div class="form-group">
                            <label for="notification-title">Title</label>
                            <input type="text" id="notification-title" class="form-control" required>
                          </div>
                          <div class="form-group">
                            <label for="notification-content">notification Content</label>
                            <textarea name="notificationContent" id="notification-content" class="form-control" cols="30 " rows="5" required></textarea>
                          </div>
                          <div class="form-group">
                            <label for="notification-platform">Platform</label>
                            <select name="" id="notification-platform" class="form-control">
                              <option value="ALL">All</option>
                              <option value="ANDROID">Android</option>
                              <option value="IOS">iOS</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="notification-to">To</label>
                            <select name="notificationTo" id="notification-to" class="form-control">
                              <option value="ALL">All Users</option>
                              <option value="SENDTOUSER">Certian Users</option>
                              <option value="NOTREGISTERED">Not Registered Users</option>
                              <option value="REGISTERED">Registered Users</option>
                            </select>
                          </div>
                          <div class="form-group d-none" id="notifications-certain-users">
                            <label for="certain-users-text ">Please enter the phone numbers of the users separated by comma</label>
                            <textarea name="certain-users-text" id="certain-users-text" placeholder="Ex: 01xxxxxxxxx, 01xxxxxxxxxx" class="form-control"
                              cols="30" rows="5"></textarea>
                          </div>
                          <div class="form-group ">
                            <label for="notification-opens">Opens In</label>
                            <select name="notificationOpens" id="notification-opens" class="form-control ">
                              <option value="GENERAL">App</option>
                              <option value="NOTIFICATION">Notifications</option>
                              <option value="ORDER">Order</option>
                              <option value="BRANCH">Branch</option>
                            </select>
                          </div>
                          <div class="form-group d-none" id="notification-branch">
                            <label for="notification-branch-id">Branch id</label>
                            <input type="number" id="notification-branch-id" class="form-control">
                          </div>
                          <div class="form-check">
                            <label for="save-notification" class="form-check-label">
                              Save Notification
                              <input type="checkbox" name="saveNotification" id="save-notification" class="form-check-input">
                              <span class="checkmark"></span>
                            </label>
                          </div>
                        </div>
                        <div class="col-12 col-md-6 ">
                          <div id="notification-img"></div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-12 d-flex justify-content-end">
                          <button type="submit " class="btn btn-primary " id="add-promo ">Add Promo</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- Form wizard with step validation section end -->

      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script>
    const id = window.location.search.split("id=")[1]

    $('#notification-to').on('change', function () {
      $(this).val() === 'SENDTOUSER' ? $('#notifications-certain-users').removeClass('d-none') :
        $('#notifications-certain-users').addClass('d-none');
    });

    $('#notification-opens').on('change', function () {
      console.log($(this).val())
      $(this).val() === 'BRANCH' ? $('#notification-branch').removeClass('d-none') :
        $('#notification-branch').addClass('d-none');
    });

    $('#add-promo-form').on('submit', function (e) {
      e.preventDefault();
      const title = $('#notification-title').val();
      const content = $('#notification-content').val();
      const image = $('.dz-image img').prop('src').split('base64,')[1]
      const to = $('#notification-to').val();
      const certainMobiles = $('#certain-users-text').val()
      const platform = $('#notification-platform').val();
      const open = $('#notification-opens').val();
      const certainBranch = $('#notification-branch-id').val()
      const save = $('#save-notification').prop('checked') === true ? true : false;

      console.log({
        title,
        content,
        image,
        to,
        certainMobiles,
        platform,
        open,
        certainBranch,
        save
      })

      // TODO: To Mario => type_id != required, save must be true or false ?

      $.ajax({
        type: 'POST',
        url: 'http://spoon.api.myspoon.me/api/admin/v1/notifications',
        headers: {
          Authorization:
            'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
        },
        data: {
          title,
          content,
          image,
          platform,
          send_to: to,
          send_to_mobiles: certainMobiles,
          type: open,
          type_id: certainBranch,
          save,
        },
        success: () => swal(
          'Success!',
          `Notification has been successfully sent!`,
          'success'
        ),
        error: err => console.log(err)
      });
    });
  </script>
@endsection