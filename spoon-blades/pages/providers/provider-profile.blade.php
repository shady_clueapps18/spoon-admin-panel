@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Provider Profile</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
      <div class="content-header-right col-md-6 col-12 mb2">
        <div class="provider-actions">
          <button class="btn btn-icon btn-warning mr-2" data-toggle="tooltip" data-placement="bottom" data-original-title="View Images">
            <i class="la la-camera"></i>
          </button>
          <a href="" class="btn btn-icon btn-info create-branch" data-toggle="tooltip" data-placement="bottom" data-original-title="Create Branch">
            <i class="la la-plus"></i>
          </a>
        </div>
      </div>
    </div>
    <div class="content-body">
      <!-- CONTENT GOES HERE -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h1>Provider Info</h1>
              <a class="heading-elements-toggle">
                <i class="la la-ellipsis-v font-medium-3"></i>
              </a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li>
                    <a data-action="collapse">
                      <i class="ft-minus"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-content collapse show">
              <div class="card-body">
                <div class="row">
                  <div class="col-12 col-md-6">
                    <div class="provider-main-info d-flex">
                      <div class="provider-img-wrapper">
                        <img src="" alt="">
                      </div>
                      <div class="provider-id-name">
                        <div class="provider-id text-bold-700 h2">
                          ID :
                          <span class="text-bold-400"></span>
                        </div>
                        <div class="provider-name text-bold-700 h2">Name :
                          <span class="text-bold-400"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="provider-sec-info">
                      <div class="provider-main-branch text-bold-700 h5"> Main Branch :
                        <span class="text-bold-400"></span>
                      </div>
                      <div class="provider-type text-bold-700 h5"> Provider Type :
                        <span class="text-bold-400"></span>
                      </div>
                      <div class="provider-created-at text-bold-700 h5"> Created At :
                        <span class="text-bold-400"></span>
                      </div>
                      <div class="provider-updated-at text-bold-700 h5"> Updated At :
                        <span class="text-bold-400"></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 col-md-6">
                    <div class="provider-contacts">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <ul>
                              <li class="provider-email text-bold-700 h5 no-bullets"> Email Address :
                                <span class="text-bold-400"></span>
                              </li>
                              <li class="provider-phone text-bold-700 h5"> Phone :
                                <span class="text-bold-400"></span>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="provider-categories">
                      <h2>Categories : </h2>
                    </div>
                    <div class="provider-add-info">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <h2>Additional Info</h2>
                            <ul>
                              <li class="provider-service-percent text-bold-700 h5"> Service
                                <span class="text-bold-400"></span> % :
                              </li>
                              <li class="provider-taxes text-bold-700 h5"> Taxes
                                <span class="text-bold-400"></span> % :
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="provider-description">
                      <h2>Description</h2>
                      <p></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h1>Provider Stats</h1>
              <a class="heading-elements-toggle">
                <i class="la la-ellipsis-v font-medium-3"></i>
              </a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li>
                    <a data-action="collapse">
                      <i class="ft-minus"></i>
                    </a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-6 col-md-3">
                    <div class="provider-orders">
                      <a href="">
                        <div class="card">
                          <div class="card-content">
                            <div class="card-body">
                              <div class="media d-flex">
                                <div class="align-self-center">
                                  <i class="la la-rocket primary font-large-2 float-left"></i>
                                </div>
                                <div class="media-body text-right">
                                  <h3></h3>
                                  <span>Orders</span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-6 col-md-3">
                    <div class="provider-branches">
                      <a href="">
                        <div class="card">
                          <div class="card-content">
                            <div class="card-body">
                              <div class="media d-flex">
                                <div class="align-self-center">
                                  <i class="la la-coffee warning font-large-2 float-left"></i>
                                </div>
                                <div class="media-body text-right">
                                  <h3></h3>
                                  <span>Branches</span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-6 col-md-3">
                    <div class="provider-favs">
                      <a href="">
                        <div class="card">
                          <div class="card-content">
                            <div class="card-body">
                              <div class="media d-flex">
                                <div class="align-self-center">
                                  <i class="la la-heart-o danger font-large-2 float-left"></i>
                                </div>
                                <div class="media-body text-right">
                                  <h3></h3>
                                  <span>Favorites</span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-6 col-md-3">
                    <div class="provider-reviews">
                      <a href="">
                        <div class="card">
                          <div class="card-content">
                            <div class="card-body">
                              <div class="media d-flex">
                                <div class="align-self-center">
                                  <i class="la la-star-o yellow font-large-2 float-left"></i>
                                </div>
                                <div class="media-body text-right">
                                  <h3></h3>
                                  <span>Reviews</span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row add-rest-info">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h1>Additional Restaurant Info</h1>
              <a class="heading-elements-toggle">
                <i class="la la-ellipsis-v font-medium-3"></i>
              </a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li>
                    <a data-action="collapse">
                      <i class="ft-minus"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-content collapse show">
              <div class="card-body">
                <div class="row">
                  <div class="col-6 col-md-4">
                    <div class="provider-res-prep">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <div class="media d-flex">
                              <div class="align-self-center">
                                <i class="la la-heart-o danger font-large-2 float-left"></i>
                              </div>
                              <div class="media-body text-right">
                                <h3>Preparation Time</h3>
                                <span></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-6 col-md-4">
                    <div class="provider-res-price">
                      <div class="card">
                        <div class="card-content">
                          <div class="card-body">
                            <div class="media d-flex">
                              <div class="align-self-center">
                                <i class="la la-heart-o danger font-large-2 float-left"></i>
                              </div>
                              <div class="media-body text-right">
                                <h3>Price Range</h3>
                                <span></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-6 col-md-4">
                    <div class="provider-res-cuisines">
                      <h2>Cuisines : </h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row rest-menu-sec">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h1>Provider Menu Sections</h1>
              <a class="heading-elements-toggle">
                <i class="la la-ellipsis-v font-medium-3"></i>
              </a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li>
                    <a data-action="collapse">
                      <i class="ft-minus"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-content collapse show">
              <div class="card-body" id="menu-table-wrapper">

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  let id = window.location.search.split("id=")[1];
  const tableMarkup = `
  <table class="table table-striped table-bordered" id="provider-menu-section">
    <thead>
      <tr>
        <th>Id</th>
        <th>Name</th>
        <th>No. of Products</th>
        <th>Orders</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
`

  $.ajax({
    type: 'GET',
    url: `http://spoon.api.myspoon.me/api/admin/v1/menuProvider/${id}`,
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    },
    success: ({ data }) => {
      // FIXME: Sometimes returns "No Data Available" Table!
      if (data.length == 0) {
        $('#menu-table-wrapper').append(`<a href="createMenuSection?id=${id}" class="btn btn-primary">Add Menu</a>`)
      } else {
        $('#menu-table-wrapper').append(tableMarkup);

        $('#provider-menu-section').DataTable({
          processing: true,
          ajax: {
            type: 'GET',
            url: `http://spoon.api.myspoon.me/api/admin/v1/menuProvider/${id}`,
            dataSrc: 'data',
            headers: {
              Authorization:
                'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
            }
          },
          columns: [
            { data: 'id' },
            { data: 'name' },
            //TODO: Return the number of products
            { data: 'price' },
            {
              data: function (data, type, row, meta) {
                return data.price == 0 ? 0 : `<a href="">${data.price}</a?`;
              }
            },
            {
              mRender: function (data, type, row) {
                return `
              <a href="" class="btn btn-sm btn-cyan add-product">Add Product</a>
              <a href="" class="btn btn-sm btn-secondary menu-edit">Edit</a>
              <button class="btn btn-sm btn-danger menu-delete">Delete</button>
            `;
              },
              className: 'table-actions'
            }
          ]
        });
      }
    }
  })
</script>

<script>
  let $id = $('.provider-id > span')
  let name = $('.provider-name > span')
  let logo = $('.provider-img-wrapper img');
  let mainBranch = $('.provider-main-branch > span')
  let type = $('.provider-type > span')
  let createdAt = $('.provider-created-at > span')
  let updatedAt = $('.provider-updated-at > span')
  let email = $('.provider-email > span')
  let phone = $('.provider-phone > span')
  let descripiton = $('.provider-description p')
  let categories = $('.provider-categories');
  let orders = $('.provider-orders h3')
  let favs = $('.provider-favs h3')
  let branches = $('.provider-branches h3')
  let reviews = $('.provider-reviews h3')
  let services = $('.provider-service-percent > span');
  let taxes = $('.provider-taxes > span');
  let preparationTime = $('.provider-res-prep').find('span');
  let cuisines = $('.provider-res-cuisines');
  let $priceRange = $('.provider-res-price').find('span');

  $('.create-branch').on('click', function () {
    // TODO: ROUTE
    $(this).attr('href', `../branches/create-branch.html?id=${id}`)
  });

  $(document).on('click', '.add-product', function () {
    $(this).attr('href', `addProduct?id=${id}`)
  });

  $(document).on('click', '.menu-edit', function () {
    $(this).attr('href', ``);
  });

  $(document).on('click', '.menu-delete', function () {
    $(this).parents('tr').remove();

    $.ajax({
      type: 'DELETE',
      url: ''
    })
  });

  $.ajax({
    type: 'GET',
    url: `http://spoon.api.myspoon.me/api/admin/v1/providerById/${id}`,
    success: ({ data }) => {
      $($id).text(data.id)
      $(name).text(data.name)
      $(logo).attr('src', data.logo)
      $(mainBranch).text(`${data.branch.area} - ${data.branch.city}`)
      $(createdAt).text(data.created_at)
      $(updatedAt).text(data.updated_at)
      $(descripiton).text(data.description)
      $(type).text(data.grocery == null ? "Resturant" : "Grocery")
      $(createdAt).text(data.created_at)
      $(updatedAt).text(data.updated_at)
      $(email).text(data.contact_email == null ? "Not Provided" : data.contact_email)
      $(phone).text(data.contact_phone == null ? "Not Provided" : data.contact_phone)
      $(services).text(data.service)
      $(taxes).text(data.taxes)
      $(orders).text(data.order_count)
      $(favs).text(data.num_favorite)
      $(branches).text(data.branch_count)
      $(reviews).text(data.num_review)
      if (data.grocery == null) {

        $(preparationTime).text(data.branch.restaurant.preparation_time);

        switch (data.branch.restaurant.price_range) {
          case 1:
            $($priceRange).text('$')
            break;

          case 2:
            $($priceRange).text('$$')
            break;

          case 3:
            $($priceRange).text('$$$')
            break;

          default:
            break;
        }

        if (!data.branch.restaurant.cuisines.length) {
          $(cuisines).append(`<span>No Cuisines Found!</span>`)
        } else {
          $(cuisines).append(data.branch.restaurant.cuisines.map(cuisine => {
            return `
          <div class="badge badge-primary">${cuisine}</div>
            `
          }))
        }
      }

      $(categories).append(data.categories.map(cat => {
        return `
          <div class="badge badge-primary">${cat}</div>
        `
      }))

      if (data.grocery != null) {
        $('.add-rest-info, .rest-menu-sec').addClass('d-none');

      }
    },
    error: err => console.log(err),
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    }
  })

  $('.provider-orders a').attr('href', `orders?id=${id}`)
  $('.provider-branches a').attr('href', `branches?id=${id}`)
  $('.provider-favs a').attr('href', `users?id=${id}`)

</script>
@endsection