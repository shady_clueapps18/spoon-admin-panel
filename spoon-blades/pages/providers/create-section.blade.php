@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <section id="validation">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Add Menu Section</h4>
                <a class="heading-elements-toggle">
                  <i class="la la-ellipsis-h font-medium-3"></i>
                </a>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <form enctype="multipart/form-data" id="add-menu-form">
                    <div class="row">
                      <div class="col-12">
                        <div class="form-group">
                          <label for="section-name">Name</label>
                          <input type="text" id="section-name" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="section-weight">Weight</label>
                          <input type="number" id="section-weight" class="form-control" required>
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary" id="add-menu">Add Menu</button>
                  </form>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Add Menu Section</h4>
                <a class="heading-elements-toggle">
                  <i class="la la-ellipsis-h font-medium-3"></i>
                </a>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <table class="table table-striped table-bordered" id="provider-menu-section">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>No. of Products</th>
                        <th>Orders</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Form wizard with step validation section end -->

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split("id=")[1]
  let menu = [];
  $('#add-menu-form').on('submit', function (e) {
    e.preventDefault()
    let menuName = $('#section-name').val();
    let menuWeight = $('#section-weight').val()
    menu.push({ name: menuName, order: menuWeight })
    console.log(menu)

    $('#provider-menu-section').DataTable().row.add({
      name: menuName,
      price: 10,
      price1: menuWeight,
      actions: 1
    }).draw(false)

    $.ajax({
      type: 'POST',
      url: `http://spoon.api.myspoon.me/api/admin/v1/createMenuProvider/${id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: { menu },
      success: data => console.log(data),
      error: err => console.log(err)
    })
    menu = []
    $('#add-menu-form')[0].reset()

    $('#provider-menu-section').click();

  });
</script>
@endsection