@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <section id="validation">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"></h4>
                <a class="heading-elements-toggle">
                  <i class="la la-ellipsis-h font-medium-3"></i>
                </a>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <form action="upload.php" enctype="multipart/form-data" method="POST" class="steps-validation wizard-circle">

                    <!-- Step 1 -->
                    <h6>Step 1</h6>
                    <fieldset>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-name">
                              Name:
                              <span class="danger">*</span>
                            </label>
                            <input type="text" class="form-control required" id="provider-name" name="providerName">
                          </div>
                          <div class="form-group">
                            <label for="provider-email">
                              Email Address :
                            </label>
                            <input type="email" class="form-control" id="provider-email" name="providerEmail">
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="lastName3">
                              Short Description:
                              <span class="danger">*</span>
                            </label>
                            <textarea name="providerDescription" class="form-control required" id="provider-description" cols="30" rows="6"></textarea>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-phone">
                              Phone Number :
                            </label>
                            <input type="tel" class="form-control" id="provider-phone" name="providerPhone">
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-type">
                              Type :
                              <span class="danger">*</span>
                            </label>
                            <select class="custom-select form-control required" id="provider-type" name="providerType">
                              <option value="Restaurant">Restaurant</option>
                              <option value="Grocery">Grocery</option>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="row">

                        <div class="col-md-6">
                          <label>Main Branch Images</label>
                          <div id="provider-branch-images">
                          </div>
                        </div>
                    </fieldset>

                    <!-- Step 2 -->
                    <h6>Step 2</h6>
                    <fieldset>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group position-relative">
                            <label for="provider-taxes">
                              Taxes :
                              <span class="danger">*</span>
                            </label>
                            <input type="number" class="form-control required" id="provider-taxes" name="providerTaxes">
                            <span class="percentage">%</span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group position-relative">
                            <label for="provider-service">
                              Service :
                              <span class="danger">*</span>
                            </label>
                            <input type="number" class="form-control required" id="provider-service" name="providerService">
                            <span class="percentage">%</span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-categories">
                              Categories :
                              <span class="danger">*</span>
                            </label>
                            <select name="providercategories" id="provider-categories" multiple="multiple" style="width:100%"> </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group position-relative">
                            <label for="provider-taxes">
                              Preparation Time :
                              <span class="danger">*</span>
                            </label>
                            <input type="number" class="form-control required" id="provider-taxes" name="providerTaxes">
                            <span class="percentage">min.</span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-price-range">
                              Price Range :
                            </label>
                            <select class="custom-select form-control" id="provider-price-range" name="providerPriceRange">
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-cuisines">
                              Cuisines :
                              <span class="danger">*</span>
                            </label>
                            <select name="providercuisines" id="provider-cuisines" multiple="multiple" style="width:100%"></select>
                          </div>
                        </div>
                      </div>
                    </fieldset>

                    <!-- Step 3 -->
                    <h6>Step 3</h6>
                    <fieldset>
                      <div class="row">

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-countries">
                              Countries :
                              <span class="danger">*</span>
                            </label>
                            <select name="providerCountries" id="provider-countries" style="width:100%"></select>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-city">
                              City :
                              <span class="danger">*</span>
                            </label>
                            <select name="providercity" id="provider-city" style="width:100%"></select>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-area">
                              Area :
                              <span class="danger">*</span>
                            </label>
                            <select name="providerArea" id="provider-area" style="width:100%"></select>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-address">
                              Address :
                              <span class="danger">*</span>
                            </label>
                            <input type="text" class="form-control required" id="provider-address" name="providerAddress">
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <div class="row">
                              <div class="col-12 col-md-6">
                                <label for="eventName3">
                                  Latitude :
                                  <span class="danger">*</span>
                                </label>
                                <input type="text" class="form-control required" id="eventName3" name="eventName">
                              </div>
                              <div class="col-12 col-md-6">
                                <label for="eventName3">
                                  Longitude :
                                  <span class="danger">*</span>
                                </label>
                                <input type="text" class="form-control required" id="eventName3" name="eventName">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </section>
      <!-- Form wizard with step validation section end -->

      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script>
  let countries = [];
  let cities = [];
  let areas = [];
  let categories = [];
  let cuisines = [];

  // Get All Countries
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/v1/countries',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      data.forEach(i => countries.push({ id: i.id, name: i.name }))
      countries.forEach(country => $('#provider-countries').append(`<option value=${country.id}>${country.name}</option>`))
      $('#provider-countries').select2();
    }
  });

  // Get All Cities with country Id
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/v1/cities?countryId=1',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      data.forEach(i => cities.push({ id: i.id, name: i.name }))
      cities.forEach(city => $('#provider-city').append(`<option value=${city.id}>${city.name}</option>`))
      $('#provider-city').select2();
    }
  });

  // Get Areas with City Id
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/v1/areas?cityId=1',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      data.forEach(i => areas.push({ id: i.id, name: i.name }))
      areas.forEach(area => $('#provider-area').append(`<option value=${area.id}>${area.name}</option>`))
      $('#provider-area').select2();
    }
  });

  // Get Categories Suggestion
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/v1/home/categories',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      data.forEach(i => categories.push({ id: i.id, name: i.name }))
      categories.forEach(category => $('#provider-categories').append(`<option value=${category.id}>${category.name}</option>`))
      $('#provider-categories').select2({
        placeholder: 'Categories',
      });
    }
  });

  // Get Cuisines Suggestion
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/v1/search/cuisines',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      data.forEach(i => cuisines.push({ id: i.id, name: i.name }))
      cuisines.forEach(cuisine => $('#provider-cuisines').append(`<option value=${cuisine.id}>${cuisine.name}</option>`))
      $('#provider-cuisines').select2({
        placeholder: 'Cuisines',
      });
    }
  });

  // Change cities on Country Change
  $('#provider-countries').on('change', function () {
    cities = [];
    $.ajax({
      type: 'GET',
      url: `http://spoon.api.myspoon.me/api/v1/cities?countryId=${$(this).val()}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      success: ({ data }) => {
        $('#provider-city').find('option').each(function () {
          $(this).remove();
        });
        data.forEach(i => cities.push({ id: i.id, name: i.name }))
        console.log(cities)
        cities.forEach(city => $('#provider-city').append(`<option value=${city.id}>${city.name}</option>`))
      }
    })
  })

  // Change Areas on City Change
  $('#provider-city').on('change', function () {
    areas = [];
    $.ajax({
      type: 'GET',
      url: `http://spoon.api.myspoon.me/api/v1/areas?cityId=${$(this).val()}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      success: ({ data }) => {
        $('#provider-area').find('option').each(function () {
          $(this).remove();
        });
        data.forEach(i => areas.push({ id: i.id, name: i.name }))
        console.log(areas)
        areas.forEach(area => $('#provider-area').append(`<option value=${area.id}>${area.name}</option>`))
      }
    })
  })

</script>

<script>

  const id = window.location.search.split('id=')[1]
  $(document).on('click', 'a[href="#finish"]', function (e) {
    const name = $('#provider-name').val();
    const description = $('#provider-description').val();
    const logo = window.btoa('Hello World!');
    const type = $('#provider-type').val() === "Restaurant" ? 1 : 2;
    const mobile = $('#provider-phone').val();
    const email = $('#provider-email').val();
    const categories = [];
    const taxes = $('#provider-taxes').val();
    const service = $('#provider-service').val();
    const weight = 1;
    const preparationTime = 10;
    const priceRange = $('#provider-price-range').val();
    const cuisines = [];

    e.preventDefault();
    $.ajax({
      type: 'POST',
      url: 'http://spoon.api.myspoon.me/api/admin/v1/providers',
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: {
        name: name,
        description: description,
        logo: logo,
        type: type,
        contact_mobile: mobile,
        contact_email: email,
        categories: categories,
        taxes: taxes,
        service: service,
        weight: weight,
        preparation_time: preparationTime,
        price_range: priceRange,
        cuisines: cuisines
      },
      success: data => console.log(data),
      error: err => console.log(err)
    })
  })
</script>
@endsection