@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <table class="table table-striped table-bordered text-inputs-searching" id="providers-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Type</th>
            <th>Branches no.</th>
            <th>Favorites no.</th>
            <th>Orders no.</th>
            <th>Rate</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const id = window.location.search.split('id=')[1]
  const users = [];
  const user = {};
  const providersTable = $("#providers-table").DataTable(
    {
      processing: true,
      ajax: {
        type: 'GET',
        url: "http://spoon.api.myspoon.me/api/admin/v1/providers?",
        dataSrc: "data",
        headers: {
          "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
        },
      },
      columns: [
        { data: "id" },
        { data: data => `<a href='providerProfile?id=${data.id}'>${data.name}</a>` },
        {
          data: (data) => data.branch.grocery == null ? 'Restaurant' : 'Grocery'
        },
        {
          data: (data) => `<a href="branches?id=${data.id}">${data.branch_count}</a>`
        },
        { data: data => `<a href="" class="provider-fav">${data.num_favorite}</a>` },
        { data: "order_count" },
        { data: "num_review" },
        {
          mRender: function (data, type, row, meta) {
            return `
                <button class="btn btn-sm btn-cyan provider-view">View</button>
                <button class="btn btn-sm btn-info provider-edit">Edit</button>
                <button class="btn btn-sm btn-danger provider-delete">Delete</button>
              `;
          },
          className: "table-actions"
        }
      ]
    }
  );

  // Case redirected from other table (filtering based on parameter)!
  if (document.URL.includes('providers=')) {
    const searchKey = decodeURI(window.location.search.split('providers=')[1].replace(/\,/g, '|'))
    providersTable.column(1).search(searchKey, true, false).draw()
  }

  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/admin/v1/providers',
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: data => ({ data }) => {
      // data.forEach(elm => {
      //   user.id = elm.id;
      //   user.user = elm.user
      //   users.push(user);
      //   user = {}
      // });
    },
    error: err => console.log(err)
  });

  $(document).on('click', '.provider-view', function () {
    const id = $(this).parents('tr').find('td:first-of-type').text();
    window.location = `providerProfile?id=${id}`
  });

  $(document).on('click', '.provider-delete', function () {
    const $id = $(this).parents('tr').find('td:first-of-type').text();
    console.log($id)
    $.ajax({
      type: 'DELETE',
      url: `https://spoon.api.myspoon.me/api/admin/v1/providers/${$id}`,
      headers: {
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
      },
      data: {
        id: $id
      },
      success: ({ data }) => {
        $(this).parents('tr').remove()
        // Sweet Alert
        swal(
          'Success!',
          `Provider has been successfully removed!`,
          'success'
        )
      },
      error: err => console.log(err)
    });
  });

  $(document).on('click', '.provider-edit', function () {
    const id = $(this).parents('tr').find('td:first-of-type').text();
    window.location = `editProvider?id=${id}`
  });

  $(document).on('click', '.provider-fav', function (e) {
    e.preventDefault();
    const $userId = $(this).parents('tr').find('td:first-of-type');
    const userIds = [];
    const selectedusers = users.find(user => user.id == $userId).user
    selectedusers.forEach(user => userIds.push(user.name))
    const redirectLink = `users?users=${userIds.toString()}`
    window.location = redirectLink
  })

</script>
@endsection