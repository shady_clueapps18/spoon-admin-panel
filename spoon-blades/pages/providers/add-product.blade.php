@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">form</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <form class="form form-horizontal" id="create-user" novalidate>
        <div class="form-body">
          <h4 class="form-section">
            <i class="ft-user"></i> Add Product</h4>
          <div class="row">
            <div class="col-12 col-md-6">
              <div class="form-group row">
                <label class="col-md-2 label-control" for="name" required>Name
                  <span class="required"> *</span>
                </label>
                <div class="controls col-md-10">
                  <input type="text" id="product-name" class="form-control" placeholder="productName" required>
                </div>
              </div>
              <div class="col-12">
                <div id="product-img"></div>
              </div>
            </div>
            <div class="col-12 col-md-6">
              <div class="form-group row">
                <label class="col-md-2 label-control" for="product-price">Price
                  <span class="required"> *</span>
                </label>
                <div class="col-md-10">
                  <input type="text" id="product-price" class="form-control" placeholder="Price" name="productPrice" required>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-2 label-control">Basic Price?</label>
                <div class="col-md-10">
                  <fieldset id="radioButtons">
                    <div class="d-inline-block custom-control custom-radio mr-1">
                      <input type="radio" class="custom-control-input" name="basicPrice" id="basic-price-yes" value="yes">
                      <label class="custom-control-label" for="basic-price-yes">Yes</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                      <input type="radio" class="custom-control-input" name="basicPrice" id="basic-price-no" value="no" checked>
                      <label class="custom-control-label" for="basic-price-no">No</label>
                    </div>
                  </fieldset>
                </div>
              </div>
              <div class="form-group">
                <label class="label-control" for="e-mail">Description</label>
                <textarea name="productDescription" id="product-description" rows="5"></textarea>
              </div>
              <div class="row">
                <div class="col-12 d-flex justify-content-center justify-content-md-end">
                  <button type="button" class="btn btn-info" id="add-product-addition" data-toggle="modal" data-target="#addition-modal">Add Product Addition</button>
                </div>
              </div>


              <!-- Addition Modal -->
              <div class="modal fade text-left" id="addition-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header bg-info white">
                      <h4 class="modal-title" id="myModalLabel1">Add Product Addition</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="form-group">
                        <label for="addition-title">Title</label>
                        <input type="text" class="form-control" id="addition-title">
                      </div>
                      <div class="form-group">
                        <div class="custom-control custom-check">
                          <input type="checkbox" class="custom-control-input" id="addition-required">
                          <label for="addition-required" class="custom-control-label">Required Addition</label>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="custom-control custom-check">
                          <input type="checkbox" class="custom-control-input disabled" id="multiple-prod-opt">
                          <label for="multiple-prod-opt" class="custom-control-label">Allow Multiple Choices</label>
                        </div>
                      </div>

                      <div class="form-group">
                        <button type="button" class="btn btn-info" id="add-option">Add Option</button>
                      </div>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn grey btn-outline-secondary" id="modal-cancel" data-dismiss="modal">Cancel</button>
                      <button type="button" class="btn btn-outline-info" id="another-addition">Add Another Addition</button>
                      <button type="button" class="btn btn-outline-primary" id="save-additions" data-dismiss="modal">Save changes</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Addition Modal -->

            </div>
          </div>

          <!-- Additions (in shape of cards) -->
          <div class="row added-options-wrapper"></div>


        </div>
        <div class="row">
          <div class="col-12">
            <div class="form-actions">
              <button type="button" class="btn btn-warning mr-1">
                <i class="ft-x"></i> Cancel
              </button>
              <button type="submit" class="btn btn-primary" id="submit-product">
                <i class="la la-check-square-o"></i> Save
              </button>
            </div>
          </div>
        </div>
    </div>
  </div>
  </form>
</div>
@endsection

@section('scripts')
<script>
  let id = window.location.search.split('id=')[1]
</script>
@endsection