@extends('layout.layout')

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Overview</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">

      <!-- CONTENT GOES HERE -->
      <section id="validation">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"></h4>
                <a class="heading-elements-toggle">
                  <i class="la la-ellipsis-h font-medium-3"></i>
                </a>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <form action="upload.php" enctype="multipart/form-data" method="POST" class="steps-validation wizard-circle">

                    <!-- Step 1 -->
                    <h6>Step 1</h6>
                    <fieldset>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-name">
                              Name:
                              <span class="danger">*</span>
                            </label>
                            <input type="text" class="form-control required" id="provider-name" name="providerName">
                          </div>
                          <div class="form-group">
                            <label for="provider-email">
                              Email Address :
                            </label>
                            <input type="email" class="form-control" id="provider-email" name="providerEmail">
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="lastName3">
                              Short Description:
                              <span class="danger">*</span>
                            </label>
                            <textarea name="providerDescription" class="form-control required" id="provider-description" cols="30" rows="6"></textarea>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-phone">
                              Phone Number :
                            </label>
                            <input type="tel" class="form-control" id="provider-phone" name="providerPhone">
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-type">
                              Type :
                              <span class="danger">*</span>
                            </label>
                            <select class="custom-select form-control required" id="provider-type" name="providerType">
                              <option value="Restaurant">Restaurant</option>
                              <option value="Grocery">Grocery</option>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="row">

                        <div class="col-md-6">
                          <label>Main Branch Images</label>
                          <div class="col-12 col-md-6 d-flex justify-content-end">
                            <div id="edit-category-img" class="edit-img-wrapper">
                              <img src="" alt="">
                              <div class="edit-image-overlay">
                                <button class="btn btn-cyan edit-image">Change Image</button>
                                <button class="btn btn-danger delete-image">Delete Image</button>
                              </div>
                            </div>
                            <div id="edit-img-dropzone"></div>
                          </div>
                        </div>
                    </fieldset>

                    <!-- Step 2 -->
                    <h6>Step 2</h6>
                    <fieldset>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group position-relative">
                            <label for="provider-taxes">
                              Taxes :
                              <span class="danger">*</span>
                            </label>
                            <input type="number" class="form-control required" id="provider-taxes" name="providerTaxes">
                            <span class="percentage">%</span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group position-relative">
                            <label for="provider-service">
                              Service :
                              <span class="danger">*</span>
                            </label>
                            <input type="number" class="form-control required" id="provider-service" name="providerService">
                            <span class="percentage">%</span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-categories">
                              Categories :
                              <span class="danger">*</span>
                            </label>
                            <select name="providercategories" id="provider-categories" multiple="multiple" style="width:100%">
                              <option value="">lorem</option>
                              <option value="">lorem</option>
                              <option value="">lorem</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group position-relative">
                            <label for="provider-preparation-time">
                              Preparation Time :
                              <span class="danger">*</span>
                            </label>
                            <input type="number" class="form-control required" id="provider-preparation-time" name="providerPreparationTime">
                            <span class="percentage">min.</span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-price-range">
                              Price Range :
                            </label>
                            <select class="custom-select form-control" id="provider-price-range" name="providerPriceRang">
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-cuisines">
                              Cuisines :
                              <span class="danger">*</span>
                            </label>
                            <select name="providercuisines" id="provider-cuisines" multiple="multiple" style="width:100%">
                              <option value="">lorem</option>
                              <option value="">lorem</option>
                              <option value="">lorem</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </fieldset>

                    <!-- Step 3 -->
                    <h6>Step 3</h6>
                    <fieldset>
                      <div class="row">

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-countries">
                              Countries :
                              <span class="danger">*</span>
                            </label>
                            <select name="providerCountries" id="provider-countries" style="width:100%">
                              <option value="">lorem</option>
                              <option value="">lorem</option>
                              <option value="">lorem</option>
                            </select>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-city">
                              City :
                              <span class="danger">*</span>
                            </label>
                            <select name="providercity" id="provider-city" style="width:100%">
                              <option value="">lorem</option>
                              <option value="">lorem</option>
                              <option value="">lorem</option>
                            </select>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-area">
                              Country :
                              <span class="danger">*</span>
                            </label>
                            <select name="providerArea" id="provider-area" style="width:100%">
                              <option value="">lorem</option>
                              <option value="">lorem</option>
                              <option value="">lorem</option>
                            </select>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="provider-address">
                              Address :
                              <span class="danger">*</span>
                            </label>
                            <input type="text" class="form-control required" id="provider-address" name="providerAddress">
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <div class="row">
                              <div class="col-12 col-md-6">
                                <label for="eventName3">
                                  Latitude :
                                  <span class="danger">*</span>
                                </label>
                                <input type="text" class="form-control required" id="eventName3" name="eventName">
                              </div>
                              <div class="col-12 col-md-6">
                                <label for="eventName3">
                                  To :
                                  <span class="danger">*</span>
                                </label>
                                <input type="text" class="form-control required" id="eventName3" name="eventName">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </section>
      <!-- Form wizard with step validation section end -->

      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script>
  console.log($('#provider-price-range').val())
  let id = window.location.search.split("id=")[1];
  let categories = [];
  let weight = 1;
  let priceRange = 1;
  let cuisines = [];

  $.ajax({
    type: 'GET',
    url: `http://spoon.api.myspoon.me/api/admin/v1/providerById/${id}`,
    headers: {
      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE"
    },
    success: ({ data }) => {
      $('#provider-name').val(data.name);
      $('#provider-description').val(data.description);
      $('#provider-type').val(data.type == 1 ? "Restaurant" : "Grocery");
      $('#provider-phone').val(data.contact_mobile);
      $('#provider-email').val(data.contact_email);
      $('#provider-taxes').val(data.taxes);
      $('#provider-service').val(data.service);
      $('#provider-preparation-time').val(parseInt(data.branch.restaurant.preparation_time))
      $('#provider-price-range').val(parseInt(data.branch.restaurant.price_range))
    }
  })
</script>
@endsection