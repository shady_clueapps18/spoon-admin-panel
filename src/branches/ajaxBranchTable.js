export const ajaxBranchTable = () => {
  const id = window.location.search.split('id=')[1];
  console.log(id);
  $('#branches-table').DataTable({
    processing: true,
    ajax: {
      type: 'GET',
      url: `http://spoon.api.myspoon.me/api/admin/v1/branchesByProviderId/1`,
      dataType: 'json',
      dataSrc: 'data',
      headers: {
        Authorization:
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
      }
    },
    columns: [
      { data: data => data.branch.id },
      { data: 'name' },
      {
        data: data => data.branch.type // == 1 ? "Restaurant" : "Grocery"
      },
      {
        data: data => data.branch.area // + ' - ' + data.branch.city
      },
      {
        data: data => `<a href="orders">${data.branch.num_order}</a>`
      },

      // TODO: Ask Mario to return "total payment"
      { data: 'service' },
      { data: 'favorite' },
      { data: 'taxes' },
      {
        mRender: function(data, type, row, meta) {
          return `
              <button class="btn btn-sm btn-cyan view-branch"> View</button>
              <button class="btn btn-sm btn-info edit-branch">Edit</button>
              <button class="btn btn-sm btn-danger delete-branch">Delete</button>
              `;
        },
        className: 'table-actions'
      }
    ]
  });
};
