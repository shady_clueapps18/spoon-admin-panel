export const ajaxBranchProfile = () => {
  const id = window.location.search.split('id=')[1];
  let $id = $('.branch-id > span');
  let name = $('.branch-name > span');
  let bundle = $('.branch-bundle > span');
  let logo = $('.branch-img-wrapper img');
  let mainBranch = $('.branch-main-branch > span');
  let type = $('.branch-type > span');
  let createdAt = $('.branch-created-at > span');
  let updatedAt = $('.branch-updated-at > span');
  let email = $('.branch-email > span');
  let phone = $('.branch-phone > span');
  let descripiton = $('.branch-description p');
  let categories = $('.branch-categories');
  let features = $('.branch-features');
  let services = $('.branch-services');
  let flags = $('.branch-flags > span');
  let weight = $('.branch-weight > span');
  let servicesPercentage = $('.branch-service-percent > span');
  let taxes = $('.branch-taxes > span');
  let orders = $('.branch-orders h3');
  let branches = $('.branch-branches h3');
  let favs = $('.branch-favs h3');
  let reviews = $('.branch-reviews h3');
  let pickups = $('.branch-pickups h3');
  let dineIns = $('.branch-dineins h3');
  let bookings = $('.branch-bookings h3');
  // let preparationTime = $('.branch-res-prep').find('span');
  // let cuisines = $('.branch-res-cuisines');
  // let $priceRange = $('.branch-res-price').find('span');

  $.ajax({
    type: 'GET',
    url: `http://spoon.api.myspoon.me/api/admin/v1/providerById/${id}`,
    success: ({ data }) => {
      $($id).text(data.id);
      $(name).text(data.name);
      $(logo).attr('src', data.logo);

      //FIXME: Return the main branch, created at, updated at!
      $(mainBranch).text(`${data.branch.area} - ${data.branch.city}`);
      $(createdAt).text(data.created_at);
      $(updatedAt).text(data.updated_at);

      $(descripiton).text(data.description);
      $(type).text(data.grocery == null ? 'Resturant' : 'Grocery');
      $(email).text(
        data.contact_email == null ? 'Not Provided' : data.contact_email
      );
      $(phone).text(
        data.contact_phone == null ? 'Not Provided' : data.contact_phone
      );
      $(services).text(data.service);
      $(taxes).text(data.taxes);
      $(orders).text(data.branch.order_count);
      $(favs).text(data.num_favorite);
      $(branches).text(data.branch_count);
      $(reviews).text(data.num_review);
      if (data.grocery == null) {
        $(preparationTime).text(data.branch.restaurant.preparation_time);

        switch (data.branch.restaurant.price_range) {
          case 1:
            $($priceRange).text('$');
            break;

          case 2:
            $($priceRange).text('$$');
            break;

          case 3:
            $($priceRange).text('$$$');
            break;

          default:
            break;
        }

        if (!data.branch.restaurant.cuisines.length) {
          $(cuisines).append(`<span>No Cuisines Found!</span>`);
        } else {
          $(cuisines).append(
            data.branch.restaurant.cuisines.map(cuisine => {
              return `
          <div class="badge badge-primary">${cuisine}</div>
            `;
            })
          );
        }
      }

      $(categories).append(
        data.categories.map(cat => {
          return `
          <div class="badge badge-primary">${cat}</div>
        `;
        })
      );

      if (data.grocery != null) {
        $('.add-rest-info, .rest-menu-sec').addClass('d-none');
      }
    },
    error: err => console.log(err),
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    }
  });
};
