let id = window.location.search.split('id=')[1];

$.ajax({
  type: 'GET',
  url: `http://spoon.api.myspoon.me/api/admin/v1/providerById/${id}`,
  headers: {
    Authorization:
      'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
  },
  success: ({ data }) => {
    $('#provider-name').val(data.name);
    $('#provider-description').val(data.description);
    $('#provider-type').val(data.type == 1 ? 'Restaurant' : 'Grocery');
    $('#provider-phone').val(data.contact_mobile);
    $('#provider-email').val(data.contact_email);
    $('#provider-taxes').val(data.taxes);
    $('#provider-service').val(data.service);
    $('#provider-preparation-time').val(
      parseInt(data.branch.restaurant.preparation_time)
    );
    $('#provider-price-range').val(
      parseInt(data.branch.restaurant.price_range)
    );
  }
});
