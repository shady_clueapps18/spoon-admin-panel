let $id = $('.provider-id > span');
let name = $('.provider-name > span');
let logo = $('.provider-img-wrapper img');
let mainBranch = $('.provider-main-branch > span');
let type = $('.provider-type > span');
let createdAt = $('.provider-created-at > span');
let updatedAt = $('.provider-updated-at > span');
let email = $('.provider-email > span');
let phone = $('.provider-phone > span');
let descripiton = $('.provider-description p');
let categories = $('.provider-categories');
let orders = $('.provider-orders h3');
let favs = $('.provider-favs h3');
let branches = $('.provider-branches h3');
let reviews = $('.provider-reviews h3');
let services = $('.provider-service-percent > span');
let taxes = $('.provider-taxes > span');
let preparationTime = $('.provider-res-prep').find('span');
let cuisines = $('.provider-res-cuisines');
let $priceRange = $('.provider-res-price').find('span');
const tableMarkup = `
    <table class="table table-striped table-bordered" id="provider-menu-section">
      <thead>
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>No. of Products</th>
          <th>Orders</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  `;

$.ajax({
  type: 'GET',
  url: `http://spoon.api.myspoon.me/api/admin/v1/menuProvider/${id}`,
  headers: {
    Authorization:
      'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
  },
  success: ({ data }) => {
    if (data.length == 0) {
      $('#menu-table-wrapper').append(
        `<a href="./create-section.html?id=${id}" class="btn btn-primary">Add Menu</a>`
      );
    } else {
      $('#menu-table-wrapper').append(tableMarkup);

      $('#provider-menu-section').DataTable({
        processing: true,
        ajax: {
          type: 'GET',
          url: `http://spoon.api.myspoon.me/api/admin/v1/menuProvider/${id}`,
          dataSrc: 'data',
          headers: {
            Authorization:
              'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
          }
        },
        columns: [
          { data: 'id' },
          { data: 'name' },
          {
            data: data =>
              `<a href="" class="provider-products">${data.product_count}</a>`
          },
          { data: 'order' },
          {
            mRender: function(data, type, row) {
              return `
                <a href="" class="btn btn-sm btn-cyan add-product">Add Product</a>
                <a href="" class="btn btn-sm btn-secondary menu-edit">Edit</a>
                <button class="btn btn-sm btn-danger menu-delete">Delete</button>
              `;
            },
            className: 'table-actions'
          }
        ]
      });
    }
  }
});

$.ajax({
  type: 'GET',
  url: `http://spoon.api.myspoon.me/api/admin/v1/providerById/${id}`,
  success: ({ data }) => {
    $($id).text(data.id);
    $(name).text(data.name);
    $(logo).attr('src', data.logo);
    $(mainBranch).text(`${data.branch.area} - ${data.branch.city}`);
    $(createdAt).text(data.created_at);
    $(updatedAt).text(data.updated_at);
    $(descripiton).text(data.description);
    $(type).text(data.grocery == null ? 'Resturant' : 'Grocery');
    $(createdAt).text(data.created_at);
    $(updatedAt).text(data.updated_at);
    $(email).text(
      data.contact_email == null ? 'Not Provided' : data.contact_email
    );
    $(phone).text(
      data.contact_phone == null ? 'Not Provided' : data.contact_phone
    );
    $(services).text(data.service);
    $(taxes).text(data.taxes);
    $(orders).text(data.order_count);
    $(favs).text(data.num_favorite);
    $(branches).text(data.branch_count);
    $(reviews).text(data.num_review);
    if (data.grocery == null) {
      $(preparationTime).text(data.branch.restaurant.preparation_time);

      switch (data.branch.restaurant.price_range) {
        case 1:
          $($priceRange).text('$');
          break;

        case 2:
          $($priceRange).text('$$');
          break;

        case 3:
          $($priceRange).text('$$$');
          break;

        default:
          break;
      }

      if (!data.branch.restaurant.cuisines.length) {
        $(cuisines).append(`<span>No Cuisines Found!</span>`);
      } else {
        $(cuisines).append(
          data.branch.restaurant.cuisines.map(cuisine => {
            return `
        <div class="badge badge-primary">${cuisine}</div>
          `;
          })
        );
      }
    }

    $(categories).append(
      data.categories.map(cat => {
        return `
        <div class="badge badge-primary">${cat}</div>
      `;
      })
    );

    if (data.grocery != null) {
      $('.add-rest-info, .rest-menu-sec').addClass('d-none');
    }
  },
  error: err => console.log(err),
  headers: {
    Authorization:
      'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
  }
});

$('.provider-orders a').attr('href', `../orders/orders-table.html?id=${id}`);
$('.provider-branches a').attr('href', `../branches/branch-table.html?=${id}`);
$('.provider-favs a').attr('href', `../user/user-table.html?id=${id}`);
