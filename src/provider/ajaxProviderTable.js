// export const ajaxProviderTable = () => {
const providersTable = $('#providers-table').DataTable({
  processing: true,
  ajax: {
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/admin/v1/providers?',
    dataSrc: 'data',
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    }
  },
  columns: [
    { data: 'id' },
    {
      data: data =>
        `<a href='./provider-profile.html?id=${data.id}'>${data.name}</a>`
    },
    {
      data: data => (data.branch.grocery == null ? 'Restaurant' : 'Grocery')
    },
    {
      data: data =>
        `<a href="../branches/branch-table.html?id=${data.id}">${
          data.branch_count
        }</a>`
    },
    {
      data: data => `<a href="" class="provider-fav">${data.num_favorite}</a>`
    },
    { data: 'order_count' },
    { data: 'num_review' },
    {
      mRender: function(data, type, row, meta) {
        return `
                <button class="btn btn-sm btn-cyan provider-view">View</button>
                <button class="btn btn-sm btn-info provider-edit">Edit</button>
                <button class="btn btn-sm btn-danger provider-delete">Delete</button>
              `;
      },
      className: 'table-actions'
    }
  ]
});

// Case redirected from other table (filtering based on parameter)!
if (document.URL.includes('providers=')) {
  const searchKey = decodeURI(
    window.location.search.split('providers=')[1].replace(/\,/g, '|')
  );
  providersTable
    .column(1)
    .search(searchKey, true, false)
    .draw();
}

$.ajax({
  type: 'GET',
  url: 'http://spoon.api.myspoon.me/api/admin/v1/providerFavoriteWithUser',
  headers: {
    Authorization:
      'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
  },
  success: ({ data }) => {
    data.forEach(elm => {
      user.id = elm.id;
      user.user = elm.users_make_provider_favorite;
      users.push(user);
      user = {};
    });
  },
  error: err => console.log(err)
});
// };
