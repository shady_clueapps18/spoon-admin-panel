const id = window.location.search.split('id=')[1];

const productsTable = $('#products-table').DataTable({
  processing: true,
  ajax: {
    type: 'GET',
    url: `http://spoon.api.myspoon.me/api/admin/v1/menuSectionProduct/${id}`,
    dataSrc: 'data',
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    },
    columns: [
      { data: 'id' },
      { data: 'name' },
      { data: data => `<img src="${data.image}" />` },
      { data: 'price' },
      {
        data: data =>
          `<a href="" class="prod-additions">${
            data.product_additions.length
          }</a>`
      },
      { data: 'order' },
      {
        mRender: function(data, type, row) {
          return `
                <a href="" class="btn btn-sm btn-cyan view-product">View</a>
                <a href="" class="btn btn-sm btn-secondary edit-product">Edit</a>
              `;
        },
        className: 'table-actions'
      }
    ]
  }
});
