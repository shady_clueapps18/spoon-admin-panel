import { userInfo } from 'os';

/**
|--------------------------------------------------
| Utility Functions
|--------------------------------------------------
*/
/**
 * These are the functions will be used through the whole app
 */

// addButton function adds new button on the top of datatable in order to create new module for this table
// (Ex: Add Branch will be on the top of all branches table to redirect to Create Branch Page)
// ==== Parameters ====
// elm => selected element to prepend the button
// href => the redirect url
// title => text on the button
export const addButton = (elm, href, title) => {
  $(elm).prepend(`<a href=${href} class="btn btn-primary">${title}</a>`);
};

// limitChars function limits the characters of a long sentence to specific amount of characters
// ==== parameters ====
// input => input you want to limit its characters
// maxChars => maximum number of characters
export const limitChars = (input, maxChars) => {
  $(input).on('keyup', function() {
    $(this).val(
      $(this)
        .val()
        .substring(0, maxChars)
    );
  });
};

// createDropZone function creates new dropzone view every time it gets executed by adding .dropzone to the element then apply the function
// This is because the simple implementation of the function conflicts with the theme configuration of the function
// ==== parameters ====
// element => the div will initialize the dropzone
// maxFiles => maximum files to be uploaded
// msg => message appears after initialization of the plugin (ex: Upload up to five images)
// url => this is cuz the dropzone can't get initialized without url and it's no impact on the place the images will be sent to
export const createDropZone = (
  element,
  maxFiles,
  msg = 'Upload up to 5 images for your branch',
  url
) => {
  $(element).addClass('dropzone');
  $(element).dropzone({
    url: url,
    autoProcessQueue: false,
    dictDefaultMessage: msg,
    uploadMultiple: true,
    maxFilesize: 2,
    maxFiles: maxFiles,
    acceptedFiles: 'image/*',
    addRemoveLinks: true,
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    }
  });
};

// deleteAPI function is a function used in all delete apis for all modules (ex: delete userInfo, branch, provider, ...etc)
// ==== parameters ====
// elm => the element when gets clicked executes the action
// apiURL => the url we send the request
// successMessage => the message appears on the sweetAlert2
export const deleteAPI = (elm, apiURL, successMessage) => {
  $(document).on('click', elm, function() {
    const $id = $(this)
      .parents('tr')
      .find('td:first-of-type')
      .text();
    $.ajax({
      type: 'DELETE',
      url: `${apiURL}/${$id}`,
      headers: {
        Authorization:
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
      },
      data: {
        id: $id
      },
      success: data => {
        swal('Success!', successMessage, 'success');
      },
      error: err => console.log(err)
    });
  });
};

// editAPI function is a function used in all edit actions to redirect to the edit view
// ==== parameters ====
// elm => the element when gets clicked executes the action
// url => the url of the view we redirect to
export const editAPI = (elm, url) => {
  $(document).on('click', elm, function() {
    const $id = $(this)
      .parents('tr')
      .find('td:first-of-type')
      .text();
    $(this).attr('href', `${url}?id=${$id}`);
  });
};

// countryChanged function is executed when the user change country so this function is responsible to show the cities under this country
// ==== parameters ====
// countryElm => the dom element holding the country
// cityElm => the dom element holding the cities
export const countryChanged = (countryElm, cityElm) => {
  $(countryElm).on('change', function() {
    cities = [];
    $.ajax({
      type: 'GET',
      url: `http://spoon.api.myspoon.me/api/v1/cities?countryId=${$(
        this
      ).val()}`,
      headers: {
        Authorization:
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
      },
      success: ({ data }) => {
        $(cityElm)
          .find('option')
          .each(function() {
            $(this).remove();
          });
        data.forEach(i => cities.push({ id: i.id, name: i.name }));
        console.log(cities);
        cities.forEach(city =>
          $(cityElm).append(`<option value=${city.id}>${city.name}</option>`)
        );
      }
    });
  });
};

// cityChanged function is executed when the user change city so this function is responsible to show the areas under this country
// ==== parameters ====
// cityElm => the dom element holding the city
// areaElm => the dom element holding the areas
export const cityChanged = (cityElm, areaElm) => {
  $(cityElm).on('change', function() {
    areas = [];
    $.ajax({
      type: 'GET',
      url: `http://spoon.api.myspoon.me/api/v1/areas?cityId=${$(this).val()}`,
      headers: {
        Authorization:
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
      },
      success: ({ data }) => {
        $(areaElm)
          .find('option')
          .each(function() {
            $(this).remove();
          });
        data.forEach(i => areas.push({ id: i.id, name: i.name }));
        console.log(areas);
        areas.forEach(area =>
          $(areaElm).append(`<option value=${area.id}>${area.name}</option>`)
        );
      }
    });
  });
};
