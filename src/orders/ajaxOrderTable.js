const id = window.location.search.split('id=')[1];
const ordersTable = $('#orders-table').DataTable({
  processing: true,
  ajax: {
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/admin/v1/orders?',
    dataSrc: 'data',
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    }
  },
  columns: [
    { data: 'id' },
    {
      data: data =>
        `<a href='../providers/provider-profile.html?id=${data.provider.id}'>${
          data.provider.name
        }</a>`
    },
    {
      data: data =>
        `<a href="../user/user-profile.html?id=${data.user_id}">${
          data.user_name
        }</a>`
    },
    {
      data: data => {
        switch (data.type) {
          case 'RESTAURANT_PICKUP':
            return `<a href="">Restaurant + Restaurant Pickup</a>`;
            break;

          case 'CAR_PICKUP':
            return `<a href="">Restaurant + Car Pickup</a>`;
            break;

          case 'BOOKING':
            return `<a href="">Booking</a>`;
            break;

          case 'DINE_IN':
            return `<a href="">Restaurant + Dine-in</a>`;
            break;

          case 'PICKUP':
            return `<a href="">Grocery Pickup</a>`;
            break;

          default:
            break;
        }
      }
    },
    {
      data: data => {
        switch (data.status) {
          case 'PENDING':
            return `<a href="#" data-toggle="modal" class="change-status" data-target="#change-order-status">Pending</a>`;
            break;

          case 'IN_PROGRESS':
            return `<a href="#" data-toggle="modal" class="change-status" data-target="#change-order-status">In Progress</a>`;
            break;

          case 'APPROVED':
            return `<a href="#" data-toggle="modal" class="change-status" data-target="#change-order-status">Approved</a>`;
            break;

          case 'FINISHED':
            return `<a href="#" data-toggle="modal" class="change-status" data-target="#change-order-status">Finished</a>`;
            break;

          case 'READY':
            return `<a href="#" data-toggle="modal" class="change-status" data-target="#change-order-status">Ready</a>`;
            break;

          case 'CANCELED_BY_CLIENT':
            return `<a href="#" data-toggle="modal" class="change-status" data-target="#change-order-status">Canceled By Client</a>`;
            break;

          case 'CANCELED_BY_ADMIN':
            return `<a href="#" data-toggle="modal" class="change-status" data-target="#change-order-status">Canceled By Restaurant</a>`;
            break;

          default:
            break;
        }
      }
    },
    {
      data: data => {
        return `<a href="${data.id}">${data.order_products.reduce(
          (acc, curr) => acc + curr.quantity,
          0
        )}</a>`;
      }
    },
    { data: data => `${data.total_price} EGP` },
    { data: 'rate' },
    { data: 'created_at' },
    { data: 'finished_at' },
    {
      mRender: function(data, type, row, meta) {
        return `
                <a href="./order-view.html?id=" class="btn btn-sm btn-cyan view-order">View</a>
                <a href="" class="btn btn-sm btn-info track-order" data-toggle="modal" data-target="#track-order-modal">Track</a>
                <a href="./" class="btn btn-sm btn-danger edit-order">Edit</a>
                <button class="btn btn-sm btn-danger delete-order">Delete</button>
              `;
      },
      className: 'table-actions'
    }
  ]
});
