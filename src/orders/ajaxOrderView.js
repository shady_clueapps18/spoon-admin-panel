const id = window.location.search.split('id=')[1];
let $id = $('.order-id > span');
let type = $('.order-type > span');
let user = $('.order-user > a');
let provider = $('.order-provider > a');
let createdAt = $('.order-created-at > span');
let finishedAt = $('.order-finished-at > span');
let subTotal = $('.order-sub-total > span');
let promo = $('.order-promo > span');
let walletDiscount = $('.order-wallet-discount > span');
let discountedSubtotal = $('.order-discounted-sub > span');
let total = $('.order-total > span');
let $status = $('.order-status > div');
let $rate = $('.order-rate > .rate-wrapper');
let review = $('.order-review > review-text');

let readStatus = data => {
  switch (data.status) {
    case 'PENDING':
      return $($status).append(`<div class="badge badge-danger">Pending</div>`);
      break;

    case 'IN_PROGRESS':
      return $($status).append(
        `<div class="badge badge-warning">In Progress</div>`
      );
      break;

    case 'APPROVED':
      return $($status).append(
        `<div class="badge badge-danger">Approved</div>`
      );
      break;

    case 'FINISHED':
      return $($status).append(
        `<div class="badge badge-danger">Finished</div>`
      );
      break;

    case 'READY':
      return $($status).append(`<div class="badge badge-danger">Ready</div>`);
      break;

    case 'CANCELED_BY_CLIENT':
      return $($status).append(
        `<div class="badge badge-danger">Canceled By Client</div>`
      );
      break;

    case 'CANCELED_BY_ADMIN':
      return $($status).append(
        `<div class="badge badge-danger">Canceled By Restaurant</div>`
      );
      break;

    default:
      break;
  }
};
let readType = data => {
  switch (data.type) {
    case 'RESTAURANT_PICKUP':
      return $(type).text(`Restaurant + Restaurant Pickup`);
      break;

    case 'CAR_PICKUP':
      return $(type).text(`Restaurant + Car Pickup`);
      break;

    case 'BOOKING':
      return $(type).text(`Booking`);
      break;

    case 'DINE_IN':
      return $(type).text(`Restaurant + Dine-in`);
      break;

    case 'PICKUP':
      return $(type).text(`Grocery Pickup`);
      break;

    default:
      break;
  }
};

$.ajax({
  type: 'GET',
  url: `http://spoon.api.myspoon.me/api/admin/v1/orders/${id}`,
  success: ({ data }) => {
    console.log(data);
    $($id).text(data.id);
    $(user)
      .text(data.user_name)
      .attr('href', `../user/user-profile?id=${data.user_id}`);
    $(provider)
      .text(data.provider.name)
      .attr(
        'href',
        `../providers/provider-profile.html?id=${data.provider.id}`
      );
    $(createdAt).text(data.created_at);
    $(finishedAt).text(data.finished_at);
    $(subTotal).text(`${data.subtotal_price} EGP`);
    //TODO: Fill the right data for "PromoCode"
    $(promo).text(data.taxes);
    $(walletDiscount).text(`${data.discount_amount} EGP`);
    $(discountedSubtotal).text(
      `${data.subtotal_price - data.discount_amount} EGP`
    );
    $(total).text(`${data.total_price} EGP`);
    $(review).text(data.review);

    // Display Rating
    $($rate).raty({
      path: '../../app-assets/images/raty',
      halfShow: true,
      readOnly: true,
      score: data.rate
    });

    readStatus(data);
    readType(data);
  },
  error: err => console.log(err),
  headers: {
    Authorization:
      'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
  }
});

$('#order-products-table').DataTable({
  processing: true,
  ajax: {
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/admin/v1/providers?',
    dataSrc: 'data',
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    }
  },
  columns: [
    { data: 'id' },
    {
      data: data =>
        `<a href='./provider-profile.html?id=${data.id}'>${data.name}</a>`
    },
    {
      data: data => (data.branch.grocery == null ? 'Restaurant' : 'Grocery')
    },
    {
      data: data =>
        `<a href="../branches/branch-table.html?id=${data.id}">${
          data.branch_count
        }</a>`
    },
    { data: 'num_favorite' },
    { data: 'order_count' },
    { data: 'taxes' },
    {
      mRender: function(data, type, row, meta) {
        return `
              <button class="btn btn-sm btn-cyan btn-view">View</button>
              <button class="btn btn-sm btn-info btn-edit">Edit</button>
              <button class="btn btn-sm btn-danger" id="delete">Delete</button>
            `;
      },
      className: 'table-actions'
    }
  ]
});
