// getCountries function is a function retrieves the countries from its API and display them as a select2 plugin
// ==== parameters ====
// elm => the dom will hold the countries
// countries => the array will receive the countries
export const getCountries = (elm, countries = []) => {
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/v1/countries',
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    },
    success: ({ data }) => {
      data.forEach(i => countries.push({ id: i.id, name: i.name }));
      countries.forEach(country =>
        $(elm).append(`<option value=${country.id}>${country.name}</option>`)
      );
      $(elm).select2();
    }
  });
};

// getCities function is a function retrieves the cities from its API and display them as a select2 plugin
// ==== parameters ====
// elm => the dom will hold the cities
// cities => the array will receive the cities
export const getCities = (elm, cities) => {
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/v1/cities?countryId=1',
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    },
    success: ({ data }) => {
      data.forEach(i => cities.push({ id: i.id, name: i.name }));
      cities.forEach(city =>
        $(elm).append(`<option value=${city.id}>${city.name}</option>`)
      );
      $(elm).select2();
    }
  });
};

// getAreas function is a function retrieves the areas from its API and display them as a select2 plugin
// ==== parameters ====
// elm => the dom will hold the areas
// areas => the array will receive the areas
export const getAreas = (elm, areas = []) => {
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/v1/areas?cityId=1',
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    },
    success: ({ data }) => {
      data.forEach(i => areas.push({ id: i.id, name: i.name }));
      areas.forEach(area =>
        $(elm).append(`<option value=${area.id}>${area.name}</option>`)
      );
      $(elm).select2();
    }
  });
};

// getCategories function is a function retrieves the categories from its API and display them as a select2 plugin
// ==== parameters ====
// elm => the dom will hold the cuisines
// categories => the array will receive the categories
export const getCategories = () => {
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/v1/home/categories',
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    },
    success: ({ data }) => {
      data.forEach(i => categories.push({ id: i.id, name: i.name }));
      categories.forEach(category =>
        $('#provider-categories').append(
          `<option value=${category.id}>${category.name}</option>`
        )
      );
      $('#provider-categories').select2({
        placeholder: 'Categories'
      });
    }
  });
};

// getCuisines function is a function retrieves the cuisines from its API and display them as a select2 plugin
// ==== parameters ====
// elm => the dom will hold the cuisines
// cuisines => the array will receive the cuisines
export const getCuisines = (elm, cuisines = []) => {
  $.ajax({
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/v1/search/cuisines',
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    },
    success: ({ data }) => {
      data.forEach(i => cuisines.push({ id: i.id, name: i.name }));
      cuisines.forEach(cuisine =>
        $(elm).append(`<option value=${cuisine.id}>${cuisine.name}</option>`)
      );
      $(elm).select2({
        placeholder: 'Cuisines'
      });
    }
  });
};
