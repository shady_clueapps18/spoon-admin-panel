const id = window.location.search.split('id=')[1];
const areasTable = $('#areas-table').DataTable({
  processing: true,
  ajax: {
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/admin/v1/areas?',
    dataSrc: 'data',
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    }
  },
  columns: [
    { data: 'id' },
    { data: 'city_name' },
    { data: 'name' },
    { data: 'order' },
    { data: data => (data.active === 1 ? 'Active' : 'Inactive') },
    {
      mRender: function(data, type, row, meta) {
        return `
                  <a href="" class="btn btn-sm btn-info area-edit">Edit</a>
                  <button class="btn btn-sm btn-danger area-delete">Delete</button>
                `;
      },
      className: 'table-actions'
    }
  ]
});

if (document.URL.includes('areas=')) {
  const searchKey = decodeURI(
    window.location.search.split('areas=')[1].replace(/\,/g, '|')
  );
  console.log(searchKey);
  areasTable
    .column(2)
    .search(searchKey, true, false)
    .draw();
}
