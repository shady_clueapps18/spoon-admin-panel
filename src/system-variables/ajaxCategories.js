const id = window.location.search.split('id=')[1];
$('#categories-table').DataTable({
  processing: true,
  ajax: {
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/admin/v1/categories',
    dataSrc: 'data',
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    }
  },
  columns: [
    { data: 'id' },
    { data: 'name' },
    {
      data: data => `<div style="max-width: 150px; max-height: 150px">
                          <img style="width: 100%; height: 100%" src="${
                            data.image
                          }" />
                         </div>`
    },
    { data: 'order' },
    {
      data: data =>
        data.provider_count === 0
          ? 0
          : `<a href="../../providers/provider-table.html?providers=" class="provider-count">${
              data.provider_count
            }</a>`
    },
    // { data: "taxes" },
    {
      mRender: function(data, type, row, meta) {
        return `
              <a href="" class="btn btn-sm btn-info category-edit">Edit</a>
              <button class="btn btn-sm btn-danger category-delete">Delete</button>
            `;
      },
      className: 'table-actions'
    }
  ]
});
