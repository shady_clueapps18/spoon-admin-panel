const notificationTable = $('#notification-table').DataTable({
  processing: true,
  ajax: {
    type: 'GET',
    url: 'http://spoon.api.myspoon.me/api/admin/v1/notifications',
    dataSrc: 'data',
    headers: {
      Authorization:
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'
    }
  },
  columns: [
    { data: data => `<p id="notificationId">${data.id}</a>` },
    { data: 'title' },
    { data: data => `<a href="#" id="content">${data.content}</a>` },
    {
      data: data => `<div style="max-width: 150px; max-height: 150px">
                          <img style="width: 100%; height: 100%" src="${
                            data.image
                          }" />
                         </div>`
    },
    { data: 'platform' },
    { data: data => `<a href="" id="sendTo">${data.send_to}</a>` },
    { data: 'send_to' },
    { data: 'created_at' },
    {
      mRender: function(data, type, row) {
        return `<button class="btn btn-sm btn-cyan" id="resend-action">Resend</button>`;
      },
      className: 'table-actions'
    }
  ]
});
