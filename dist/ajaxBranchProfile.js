/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/branches/ajaxBranchProfile.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/branches/ajaxBranchProfile.js":
/*!*******************************************!*\
  !*** ./src/branches/ajaxBranchProfile.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nvar ajaxBranchProfile = exports.ajaxBranchProfile = function ajaxBranchProfile() {\n  var id = window.location.search.split('id=')[1];\n  var $id = $('.branch-id > span');\n  var name = $('.branch-name > span');\n  var bundle = $('.branch-bundle > span');\n  var logo = $('.branch-img-wrapper img');\n  var mainBranch = $('.branch-main-branch > span');\n  var type = $('.branch-type > span');\n  var createdAt = $('.branch-created-at > span');\n  var updatedAt = $('.branch-updated-at > span');\n  var email = $('.branch-email > span');\n  var phone = $('.branch-phone > span');\n  var descripiton = $('.branch-description p');\n  var categories = $('.branch-categories');\n  var features = $('.branch-features');\n  var services = $('.branch-services');\n  var flags = $('.branch-flags > span');\n  var weight = $('.branch-weight > span');\n  var servicesPercentage = $('.branch-service-percent > span');\n  var taxes = $('.branch-taxes > span');\n  var orders = $('.branch-orders h3');\n  var branches = $('.branch-branches h3');\n  var favs = $('.branch-favs h3');\n  var reviews = $('.branch-reviews h3');\n  var pickups = $('.branch-pickups h3');\n  var dineIns = $('.branch-dineins h3');\n  var bookings = $('.branch-bookings h3');\n  // let preparationTime = $('.branch-res-prep').find('span');\n  // let cuisines = $('.branch-res-cuisines');\n  // let $priceRange = $('.branch-res-price').find('span');\n\n  $.ajax({\n    type: 'GET',\n    url: 'http://spoon.api.myspoon.me/api/admin/v1/providerById/' + id,\n    success: function success(_ref) {\n      var data = _ref.data;\n\n      $($id).text(data.id);\n      $(name).text(data.name);\n      $(logo).attr('src', data.logo);\n\n      //FIXME: Return the main branch, created at, updated at!\n      $(mainBranch).text(data.branch.area + ' - ' + data.branch.city);\n      $(createdAt).text(data.created_at);\n      $(updatedAt).text(data.updated_at);\n\n      $(descripiton).text(data.description);\n      $(type).text(data.grocery == null ? 'Resturant' : 'Grocery');\n      $(email).text(data.contact_email == null ? 'Not Provided' : data.contact_email);\n      $(phone).text(data.contact_phone == null ? 'Not Provided' : data.contact_phone);\n      $(services).text(data.service);\n      $(taxes).text(data.taxes);\n      $(orders).text(data.branch.order_count);\n      $(favs).text(data.num_favorite);\n      $(branches).text(data.branch_count);\n      $(reviews).text(data.num_review);\n      if (data.grocery == null) {\n        $(preparationTime).text(data.branch.restaurant.preparation_time);\n\n        switch (data.branch.restaurant.price_range) {\n          case 1:\n            $($priceRange).text('$');\n            break;\n\n          case 2:\n            $($priceRange).text('$$');\n            break;\n\n          case 3:\n            $($priceRange).text('$$$');\n            break;\n\n          default:\n            break;\n        }\n\n        if (!data.branch.restaurant.cuisines.length) {\n          $(cuisines).append('<span>No Cuisines Found!</span>');\n        } else {\n          $(cuisines).append(data.branch.restaurant.cuisines.map(function (cuisine) {\n            return '\\n          <div class=\"badge badge-primary\">' + cuisine + '</div>\\n            ';\n          }));\n        }\n      }\n\n      $(categories).append(data.categories.map(function (cat) {\n        return '\\n          <div class=\"badge badge-primary\">' + cat + '</div>\\n        ';\n      }));\n\n      if (data.grocery != null) {\n        $('.add-rest-info, .rest-menu-sec').addClass('d-none');\n      }\n    },\n    error: function error(err) {\n      return console.log(err);\n    },\n    headers: {\n      Authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'\n    }\n  });\n};\n\n//# sourceURL=webpack:///./src/branches/ajaxBranchProfile.js?");

/***/ })

/******/ });