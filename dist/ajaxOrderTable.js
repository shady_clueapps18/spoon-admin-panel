/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/orders/ajaxOrderTable.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/orders/ajaxOrderTable.js":
/*!**************************************!*\
  !*** ./src/orders/ajaxOrderTable.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar id = window.location.search.split('id=')[1];\nvar ordersTable = $('#orders-table').DataTable({\n  processing: true,\n  ajax: {\n    type: 'GET',\n    url: 'http://spoon.api.myspoon.me/api/admin/v1/orders?',\n    dataSrc: 'data',\n    headers: {\n      Authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'\n    }\n  },\n  columns: [{ data: 'id' }, {\n    data: function data(_data) {\n      return '<a href=\\'../providers/provider-profile.html?id=' + _data.provider.id + '\\'>' + _data.provider.name + '</a>';\n    }\n  }, {\n    data: function data(_data2) {\n      return '<a href=\"../user/user-profile.html?id=' + _data2.user_id + '\">' + _data2.user_name + '</a>';\n    }\n  }, {\n    data: function data(_data3) {\n      switch (_data3.type) {\n        case 'RESTAURANT_PICKUP':\n          return '<a href=\"\">Restaurant + Restaurant Pickup</a>';\n          break;\n\n        case 'CAR_PICKUP':\n          return '<a href=\"\">Restaurant + Car Pickup</a>';\n          break;\n\n        case 'BOOKING':\n          return '<a href=\"\">Booking</a>';\n          break;\n\n        case 'DINE_IN':\n          return '<a href=\"\">Restaurant + Dine-in</a>';\n          break;\n\n        case 'PICKUP':\n          return '<a href=\"\">Grocery Pickup</a>';\n          break;\n\n        default:\n          break;\n      }\n    }\n  }, {\n    data: function data(_data4) {\n      switch (_data4.status) {\n        case 'PENDING':\n          return '<a href=\"#\" data-toggle=\"modal\" class=\"change-status\" data-target=\"#change-order-status\">Pending</a>';\n          break;\n\n        case 'IN_PROGRESS':\n          return '<a href=\"#\" data-toggle=\"modal\" class=\"change-status\" data-target=\"#change-order-status\">In Progress</a>';\n          break;\n\n        case 'APPROVED':\n          return '<a href=\"#\" data-toggle=\"modal\" class=\"change-status\" data-target=\"#change-order-status\">Approved</a>';\n          break;\n\n        case 'FINISHED':\n          return '<a href=\"#\" data-toggle=\"modal\" class=\"change-status\" data-target=\"#change-order-status\">Finished</a>';\n          break;\n\n        case 'READY':\n          return '<a href=\"#\" data-toggle=\"modal\" class=\"change-status\" data-target=\"#change-order-status\">Ready</a>';\n          break;\n\n        case 'CANCELED_BY_CLIENT':\n          return '<a href=\"#\" data-toggle=\"modal\" class=\"change-status\" data-target=\"#change-order-status\">Canceled By Client</a>';\n          break;\n\n        case 'CANCELED_BY_ADMIN':\n          return '<a href=\"#\" data-toggle=\"modal\" class=\"change-status\" data-target=\"#change-order-status\">Canceled By Restaurant</a>';\n          break;\n\n        default:\n          break;\n      }\n    }\n  }, {\n    data: function data(_data5) {\n      return '<a href=\"' + _data5.id + '\">' + _data5.order_products.reduce(function (acc, curr) {\n        return acc + curr.quantity;\n      }, 0) + '</a>';\n    }\n  }, { data: function data(_data6) {\n      return _data6.total_price + ' EGP';\n    } }, { data: 'rate' }, { data: 'created_at' }, { data: 'finished_at' }, {\n    mRender: function mRender(data, type, row, meta) {\n      return '\\n                <a href=\"./order-view.html?id=\" class=\"btn btn-sm btn-cyan view-order\">View</a>\\n                <a href=\"\" class=\"btn btn-sm btn-info track-order\" data-toggle=\"modal\" data-target=\"#track-order-modal\">Track</a>\\n                <a href=\"./\" class=\"btn btn-sm btn-danger edit-order\">Edit</a>\\n                <button class=\"btn btn-sm btn-danger delete-order\">Delete</button>\\n              ';\n    },\n    className: 'table-actions'\n  }]\n});\n\n//# sourceURL=webpack:///./src/orders/ajaxOrderTable.js?");

/***/ })

/******/ });