/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/provider/ajaxProviderProfile.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/provider/ajaxProviderProfile.js":
/*!*********************************************!*\
  !*** ./src/provider/ajaxProviderProfile.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar $id = $('.provider-id > span');\nvar name = $('.provider-name > span');\nvar logo = $('.provider-img-wrapper img');\nvar mainBranch = $('.provider-main-branch > span');\nvar type = $('.provider-type > span');\nvar createdAt = $('.provider-created-at > span');\nvar updatedAt = $('.provider-updated-at > span');\nvar email = $('.provider-email > span');\nvar phone = $('.provider-phone > span');\nvar descripiton = $('.provider-description p');\nvar categories = $('.provider-categories');\nvar orders = $('.provider-orders h3');\nvar favs = $('.provider-favs h3');\nvar branches = $('.provider-branches h3');\nvar reviews = $('.provider-reviews h3');\nvar services = $('.provider-service-percent > span');\nvar taxes = $('.provider-taxes > span');\nvar preparationTime = $('.provider-res-prep').find('span');\nvar cuisines = $('.provider-res-cuisines');\nvar $priceRange = $('.provider-res-price').find('span');\nvar tableMarkup = '\\n    <table class=\"table table-striped table-bordered\" id=\"provider-menu-section\">\\n      <thead>\\n        <tr>\\n          <th>Id</th>\\n          <th>Name</th>\\n          <th>No. of Products</th>\\n          <th>Orders</th>\\n          <th>Actions</th>\\n        </tr>\\n      </thead>\\n      <tbody>\\n      </tbody>\\n    </table>\\n  ';\n\n$.ajax({\n  type: 'GET',\n  url: 'http://spoon.api.myspoon.me/api/admin/v1/menuProvider/' + id,\n  headers: {\n    Authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'\n  },\n  success: function success(_ref) {\n    var data = _ref.data;\n\n    if (data.length == 0) {\n      $('#menu-table-wrapper').append('<a href=\"./create-section.html?id=' + id + '\" class=\"btn btn-primary\">Add Menu</a>');\n    } else {\n      $('#menu-table-wrapper').append(tableMarkup);\n\n      $('#provider-menu-section').DataTable({\n        processing: true,\n        ajax: {\n          type: 'GET',\n          url: 'http://spoon.api.myspoon.me/api/admin/v1/menuProvider/' + id,\n          dataSrc: 'data',\n          headers: {\n            Authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'\n          }\n        },\n        columns: [{ data: 'id' }, { data: 'name' }, {\n          data: function data(_data) {\n            return '<a href=\"\" class=\"provider-products\">' + _data.product_count + '</a>';\n          }\n        }, { data: 'order' }, {\n          mRender: function mRender(data, type, row) {\n            return '\\n                <a href=\"\" class=\"btn btn-sm btn-cyan add-product\">Add Product</a>\\n                <a href=\"\" class=\"btn btn-sm btn-secondary menu-edit\">Edit</a>\\n                <button class=\"btn btn-sm btn-danger menu-delete\">Delete</button>\\n              ';\n          },\n          className: 'table-actions'\n        }]\n      });\n    }\n  }\n});\n\n$.ajax({\n  type: 'GET',\n  url: 'http://spoon.api.myspoon.me/api/admin/v1/providerById/' + id,\n  success: function success(_ref2) {\n    var data = _ref2.data;\n\n    $($id).text(data.id);\n    $(name).text(data.name);\n    $(logo).attr('src', data.logo);\n    $(mainBranch).text(data.branch.area + ' - ' + data.branch.city);\n    $(createdAt).text(data.created_at);\n    $(updatedAt).text(data.updated_at);\n    $(descripiton).text(data.description);\n    $(type).text(data.grocery == null ? 'Resturant' : 'Grocery');\n    $(createdAt).text(data.created_at);\n    $(updatedAt).text(data.updated_at);\n    $(email).text(data.contact_email == null ? 'Not Provided' : data.contact_email);\n    $(phone).text(data.contact_phone == null ? 'Not Provided' : data.contact_phone);\n    $(services).text(data.service);\n    $(taxes).text(data.taxes);\n    $(orders).text(data.order_count);\n    $(favs).text(data.num_favorite);\n    $(branches).text(data.branch_count);\n    $(reviews).text(data.num_review);\n    if (data.grocery == null) {\n      $(preparationTime).text(data.branch.restaurant.preparation_time);\n\n      switch (data.branch.restaurant.price_range) {\n        case 1:\n          $($priceRange).text('$');\n          break;\n\n        case 2:\n          $($priceRange).text('$$');\n          break;\n\n        case 3:\n          $($priceRange).text('$$$');\n          break;\n\n        default:\n          break;\n      }\n\n      if (!data.branch.restaurant.cuisines.length) {\n        $(cuisines).append('<span>No Cuisines Found!</span>');\n      } else {\n        $(cuisines).append(data.branch.restaurant.cuisines.map(function (cuisine) {\n          return '\\n        <div class=\"badge badge-primary\">' + cuisine + '</div>\\n          ';\n        }));\n      }\n    }\n\n    $(categories).append(data.categories.map(function (cat) {\n      return '\\n        <div class=\"badge badge-primary\">' + cat + '</div>\\n      ';\n    }));\n\n    if (data.grocery != null) {\n      $('.add-rest-info, .rest-menu-sec').addClass('d-none');\n    }\n  },\n  error: function error(err) {\n    return console.log(err);\n  },\n  headers: {\n    Authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'\n  }\n});\n\n$('.provider-orders a').attr('href', '../orders/orders-table.html?id=' + id);\n$('.provider-branches a').attr('href', '../branches/branch-table.html?=' + id);\n$('.provider-favs a').attr('href', '../user/user-table.html?id=' + id);\n\n//# sourceURL=webpack:///./src/provider/ajaxProviderProfile.js?");

/***/ })

/******/ });