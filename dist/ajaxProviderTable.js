/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/provider/ajaxProviderTable.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/provider/ajaxProviderTable.js":
/*!*******************************************!*\
  !*** ./src/provider/ajaxProviderTable.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n// export const ajaxProviderTable = () => {\nvar providersTable = $('#providers-table').DataTable({\n  processing: true,\n  ajax: {\n    type: 'GET',\n    url: 'http://spoon.api.myspoon.me/api/admin/v1/providers?',\n    dataSrc: 'data',\n    headers: {\n      Authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'\n    }\n  },\n  columns: [{ data: 'id' }, {\n    data: function data(_data) {\n      return '<a href=\\'./provider-profile.html?id=' + _data.id + '\\'>' + _data.name + '</a>';\n    }\n  }, {\n    data: function data(_data2) {\n      return _data2.branch.grocery == null ? 'Restaurant' : 'Grocery';\n    }\n  }, {\n    data: function data(_data3) {\n      return '<a href=\"../branches/branch-table.html?id=' + _data3.id + '\">' + _data3.branch_count + '</a>';\n    }\n  }, {\n    data: function data(_data4) {\n      return '<a href=\"\" class=\"provider-fav\">' + _data4.num_favorite + '</a>';\n    }\n  }, { data: 'order_count' }, { data: 'num_review' }, {\n    mRender: function mRender(data, type, row, meta) {\n      return '\\n                <button class=\"btn btn-sm btn-cyan provider-view\">View</button>\\n                <button class=\"btn btn-sm btn-info provider-edit\">Edit</button>\\n                <button class=\"btn btn-sm btn-danger provider-delete\">Delete</button>\\n              ';\n    },\n    className: 'table-actions'\n  }]\n});\n\n// Case redirected from other table (filtering based on parameter)!\nif (document.URL.includes('providers=')) {\n  var searchKey = decodeURI(window.location.search.split('providers=')[1].replace(/\\,/g, '|'));\n  providersTable.column(1).search(searchKey, true, false).draw();\n}\n\n$.ajax({\n  type: 'GET',\n  url: 'http://spoon.api.myspoon.me/api/admin/v1/providerFavoriteWithUser',\n  headers: {\n    Authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'\n  },\n  success: function success(_ref) {\n    var data = _ref.data;\n\n    data.forEach(function (elm) {\n      user.id = elm.id;\n      user.user = elm.users_make_provider_favorite;\n      users.push(user);\n      user = {};\n    });\n  },\n  error: function error(err) {\n    return console.log(err);\n  }\n});\n// };\n\n//# sourceURL=webpack:///./src/provider/ajaxProviderTable.js?");

/***/ })

/******/ });