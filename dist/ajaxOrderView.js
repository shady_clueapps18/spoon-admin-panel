/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/orders/ajaxOrderView.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/orders/ajaxOrderView.js":
/*!*************************************!*\
  !*** ./src/orders/ajaxOrderView.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar id = window.location.search.split('id=')[1];\nvar $id = $('.order-id > span');\nvar type = $('.order-type > span');\nvar user = $('.order-user > a');\nvar provider = $('.order-provider > a');\nvar createdAt = $('.order-created-at > span');\nvar finishedAt = $('.order-finished-at > span');\nvar subTotal = $('.order-sub-total > span');\nvar promo = $('.order-promo > span');\nvar walletDiscount = $('.order-wallet-discount > span');\nvar discountedSubtotal = $('.order-discounted-sub > span');\nvar total = $('.order-total > span');\nvar $status = $('.order-status > div');\nvar $rate = $('.order-rate > .rate-wrapper');\nvar review = $('.order-review > review-text');\n\nvar readStatus = function readStatus(data) {\n  switch (data.status) {\n    case 'PENDING':\n      return $($status).append('<div class=\"badge badge-danger\">Pending</div>');\n      break;\n\n    case 'IN_PROGRESS':\n      return $($status).append('<div class=\"badge badge-warning\">In Progress</div>');\n      break;\n\n    case 'APPROVED':\n      return $($status).append('<div class=\"badge badge-danger\">Approved</div>');\n      break;\n\n    case 'FINISHED':\n      return $($status).append('<div class=\"badge badge-danger\">Finished</div>');\n      break;\n\n    case 'READY':\n      return $($status).append('<div class=\"badge badge-danger\">Ready</div>');\n      break;\n\n    case 'CANCELED_BY_CLIENT':\n      return $($status).append('<div class=\"badge badge-danger\">Canceled By Client</div>');\n      break;\n\n    case 'CANCELED_BY_ADMIN':\n      return $($status).append('<div class=\"badge badge-danger\">Canceled By Restaurant</div>');\n      break;\n\n    default:\n      break;\n  }\n};\nvar readType = function readType(data) {\n  switch (data.type) {\n    case 'RESTAURANT_PICKUP':\n      return $(type).text('Restaurant + Restaurant Pickup');\n      break;\n\n    case 'CAR_PICKUP':\n      return $(type).text('Restaurant + Car Pickup');\n      break;\n\n    case 'BOOKING':\n      return $(type).text('Booking');\n      break;\n\n    case 'DINE_IN':\n      return $(type).text('Restaurant + Dine-in');\n      break;\n\n    case 'PICKUP':\n      return $(type).text('Grocery Pickup');\n      break;\n\n    default:\n      break;\n  }\n};\n\n$.ajax({\n  type: 'GET',\n  url: 'http://spoon.api.myspoon.me/api/admin/v1/orders/' + id,\n  success: function success(_ref) {\n    var data = _ref.data;\n\n    console.log(data);\n    $($id).text(data.id);\n    $(user).text(data.user_name).attr('href', '../user/user-profile?id=' + data.user_id);\n    $(provider).text(data.provider.name).attr('href', '../providers/provider-profile.html?id=' + data.provider.id);\n    $(createdAt).text(data.created_at);\n    $(finishedAt).text(data.finished_at);\n    $(subTotal).text(data.subtotal_price + ' EGP');\n    //TODO: Fill the right data for \"PromoCode\"\n    $(promo).text(data.taxes);\n    $(walletDiscount).text(data.discount_amount + ' EGP');\n    $(discountedSubtotal).text(data.subtotal_price - data.discount_amount + ' EGP');\n    $(total).text(data.total_price + ' EGP');\n    $(review).text(data.review);\n\n    // Display Rating\n    $($rate).raty({\n      path: '../../app-assets/images/raty',\n      halfShow: true,\n      readOnly: true,\n      score: data.rate\n    });\n\n    readStatus(data);\n    readType(data);\n  },\n  error: function error(err) {\n    return console.log(err);\n  },\n  headers: {\n    Authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'\n  }\n});\n\n$('#order-products-table').DataTable({\n  processing: true,\n  ajax: {\n    type: 'GET',\n    url: 'http://spoon.api.myspoon.me/api/admin/v1/providers?',\n    dataSrc: 'data',\n    headers: {\n      Authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zcG9vbi5hcGkubXlzcG9vbi5tZVwvYXBpXC9hZG1pblwvdjFcL3VzZXJzIiwiaWF0IjoxNTMxNjU5NDQzLCJleHAiOjE1Mzk0MzU0NDMsIm5iZiI6MTUzMTY1OTQ0MywianRpIjoiaGVwZDdUYmlNZ0JUQ1J5TSIsInN1YiI6MTQ3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.fpFidjY3TOKwtJ803Nqj20kcowYZ30KtuJxvBVXRRdE'\n    }\n  },\n  columns: [{ data: 'id' }, {\n    data: function data(_data) {\n      return '<a href=\\'./provider-profile.html?id=' + _data.id + '\\'>' + _data.name + '</a>';\n    }\n  }, {\n    data: function data(_data2) {\n      return _data2.branch.grocery == null ? 'Restaurant' : 'Grocery';\n    }\n  }, {\n    data: function data(_data3) {\n      return '<a href=\"../branches/branch-table.html?id=' + _data3.id + '\">' + _data3.branch_count + '</a>';\n    }\n  }, { data: 'num_favorite' }, { data: 'order_count' }, { data: 'taxes' }, {\n    mRender: function mRender(data, type, row, meta) {\n      return '\\n              <button class=\"btn btn-sm btn-cyan btn-view\">View</button>\\n              <button class=\"btn btn-sm btn-info btn-edit\">Edit</button>\\n              <button class=\"btn btn-sm btn-danger\" id=\"delete\">Delete</button>\\n            ';\n    },\n    className: 'table-actions'\n  }]\n});\n\n//# sourceURL=webpack:///./src/orders/ajaxOrderView.js?");

/***/ })

/******/ });